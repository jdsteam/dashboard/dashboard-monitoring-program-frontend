/*
  Copyright 2019 Esri
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { loadModules } from 'esri-loader';
import esri = __esri;


@Component({
  selector: 'app-esri-map',
  templateUrl: './esri-map.component.html',
  styleUrls: ['./esri-map.component.css']
})
export class EsriMapComponent implements OnInit {
  @ViewChild('mapViewNode') private mapViewEl: ElementRef;
  @Output() mapLoadedEvent = new EventEmitter<boolean>();  
  @ViewChild('infoDiv') private infoDiv: ElementRef;

  /**
   * _zoom sets map zoom
   * _center sets map center
   * _basemap sets type of map
   * _loaded provides map loaded status
   */
  private paramZoom = 13;
  private paramCenter: Array<number> = [-6.882691, 107.610764];
  private paramBasemap = 'national-geographic';
  private paramGround = 'world-elevation';
  public paramData = {};
  public paramIsloaded = false;

  private EsriMap: any;
  private EsriMapView: any;
  private EsriSceneView: any;
  private EsriLocator: any;
  private EsriExpand: any;
  private EsriBasemapToggle: any;
  private EsriBasemapGallery: any;
  private EsriFeatureLayer: any;
  private EsriNavigationToggle: any;
  private EsriPoint: any;
  private EsriPolygon: any;
  private EsriLegend: any;
  private EsriConfig: any;
  private EsriRequest: any;
  private EsriGraphicsLayer: any;
  private EsriGraphic: any;

  public maps: any;
  public mapView: any;
  private locatorTask: any;
  private layer: any;
  private legend: any;

  private graphicsLayer: any;

  get mapLoaded(): boolean {
    return this.paramIsloaded;
  }

  @Input()
  set zoom(zoom: number) {
    this.paramZoom = zoom;
  }

  get zoom(): number {
    return this.paramZoom;
  }

  @Input()
  set center(center: Array<number>) {
    this.paramCenter = center;
  }

  get center(): Array<number> {
    return this.paramCenter;
  }

  @Input()
  set basemap(basemap: string) {
    this.paramBasemap = basemap;
  }

  get basemap(): string {
    return this.paramBasemap;
  }

  @Input()
  set ground(ground: string) {
    this.paramGround = ground;
  }

  get ground(): string {
    return this.paramGround;
  } 
  
  @Input()
  set data(data: any) {
    this.paramData = data;
  }

  get data(): any {
    return this.paramData;
  }
  
  constructor() {
  }

  errback(error) {
    console.error('Creating legend failed. ', error);
  }

  ngOnInit() {
    // Initialize MapView and return an instance of MapView
    this.initializeMap().then((result) => {
      this.houseKeeping(result);
    });
  }
 
  async initializeMap() {
    try {

      // 3D
      const [EsriMap, EsriMapView, EsriSceneView, EsriLocator, EsriExpand, EsriBasemapToggle,
        EsriBasemapGallery, EsriFeatureLayer, EsriNavigationToggle,
        EsriPoint, EsriPolygon, EsriLegend, EsriConfig, EsriRequest,
        EsriGraphicsLayer, EsriGraphic] = await loadModules([
        'esri/Map',
        'esri/views/MapView',
        'esri/views/SceneView',
        'esri/tasks/Locator',
        'esri/widgets/Expand',
        'esri/widgets/BasemapToggle',
        'esri/widgets/BasemapGallery',
        'esri/layers/FeatureLayer',
        'esri/widgets/NavigationToggle',
        'esri/geometry/Point',
        'esri/geometry/Polygon',
        'esri/widgets/Legend',
        'esri/config',
        'esri/request',
        'esri/layers/GraphicsLayer',
        'esri/Graphic'
      ]);

      this.EsriMap = EsriMap;
      this.EsriMapView = EsriMapView;
      this.EsriSceneView = EsriSceneView;
      this.EsriLocator = EsriLocator;
      this.EsriExpand = EsriExpand;
      this.EsriBasemapToggle = EsriBasemapToggle;
      this.EsriBasemapGallery = EsriBasemapGallery;
      this.EsriFeatureLayer = EsriFeatureLayer;
      this.EsriNavigationToggle = EsriNavigationToggle;
      this.EsriPoint = EsriPoint;
      this.EsriPolygon = EsriPolygon;
      this.EsriLegend = EsriLegend;
      this.EsriConfig = EsriConfig;
      this.EsriRequest = EsriRequest;
      this.EsriGraphicsLayer = EsriGraphicsLayer;
      this.EsriGraphic = EsriGraphic;

      // Configure the Map
      const mapProperties: esri.MapProperties = {
        basemap: this.paramBasemap,
        ground: this.paramGround
      };

      this.maps = new this.EsriMap(mapProperties);

      // Initialize the MapView
      const mapViewProperties: esri.MapViewProperties = {
        container: this.mapViewEl.nativeElement,
        center: this.paramCenter,
        zoom: this.paramZoom,
        map: this.maps,
        constraints: {
          snapToZoom: true
        },
        ui: {
          padding: {
            bottom: 15,
            right: 0
          }
        }
      };

      // 2D
      // this.mapView = new this.EsriMapView(mapViewProperties);

      // 3D
      this.mapView = new this.EsriSceneView(mapViewProperties);

      // locatortask
      this.locatorTask = new this.EsriLocator({
        url: 'https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer'
      });

      // Create the widget
      // const basemapToggle = new EsriBasemapToggle({
      //   view: mapView, // view that provides access to the map's 'topo' basemap
      //   nextBasemap: 'satellite' // allows for toggling to the 'hybrid' basemap
      // });

      const basemapGallery = new this.EsriBasemapGallery({
        view: this.mapView,
        source: {
          portal: {
            url: 'http://www.arcgis.com',
            useVectorBasemaps: false  // Load vector tile basemaps
          }
        }
      });

      const bgExpand = new this.EsriExpand({
        expandIconClass: 'esri-icon-collection',
        expandTooltip: 'Basemaps',
        view: this.mapView,
        content: basemapGallery
      });

      // close the expand whenever a basemap is selected
      basemapGallery.watch('activeBasemap', () => {
        const mobileSize = this.mapView.heightBreakpoint === 'xsmall' || this.mapView.widthBreakpoint === 'xsmall';
        if (mobileSize) {
          bgExpand.collapse();
        }
      });

      this.mapView.ui.add(bgExpand, 'top-right');

      this.mapView.ui.move([ 'zoom', 'navigation-toggle', 'compass' ], 'top-right');

      return this.mapView;

    } catch (error) {
      console.log('EsriLoader: ', error);
    }

  }
 
  
  // Finalize a few things once the MapView has been loaded
  houseKeeping(mapView) {
    this.mapView.when(() => {
      console.log('mapView ready: ', mapView.ready);
      this.paramIsloaded = mapView.ready;
      this.mapLoadedEvent.emit(true); 
      // this.basePolygon();  
      this.createGraphics();
    });


  }


  basePolygon() { 

    const graphicsLayer = new this.EsriGraphicsLayer();
    const url = '../../assets/data/kokab_v3.geojson';
    // this.EsriConfig.request.corsEnabledServers.push(url);
    this.EsriRequest(url, {responseType: 'json'}).then((geoJson) => {

      geoJson.data.features.map((feature, i) => {

        let varRings = [];
        if (feature.geometry.coordinates.length === 1) {
          varRings = feature.geometry.coordinates[0];
        } else {
          for (let j = 0; j < feature.geometry.coordinates.length; j++) {
            varRings[j] = feature.geometry.coordinates[j][0];
          }
        }

        const polygon = new this.EsriPolygon({
          hasM: true,
          rings: varRings,
          spatialReference: {wkid : 4326}
        }); 

        const fillSymbol = {
          type: 'simple-fill', // autocasts as new SimpleFillSymbol()
          color: [131,192,239, 0.4],
          outline: { // autocasts as new SimpleLineSymbol()
            color: [255, 255, 255],
            width: 1
          }
        };

        const attribute = {
          ObjectID: i,
          id: feature.properties.id,
          kemendagri_kode: feature.properties.kemendagri_kode,
          kemendagri_nama: feature.properties.kemendagri_nama,
          bps_kode: feature.properties.bps_kode,
          bps_nama: feature.properties.bps_nama
        };

        const popupTemplates = {
          type: 'PopupTemplate',
          title: 'Kabupaten / Kota',
          content: [{
            type: 'fields',
            fieldInfos: [{
              fieldName: 'id',
              label: 'id',
              visible: true
            }, {
              fieldName: 'kemendagri_kode',
              label: 'Kemendagri Kode',
              visible: true
            }, {
              fieldName: 'kemendagri_nama',
              label: 'Kemendagri Nama',
              visible: true
            }, {
              fieldName: 'bps_kode',
              label: 'BPS Kode',
              visible: true
            }, {
              fieldName: 'bps_nama',
              label: 'bps_nama',
              visible: true
            }]
          }]
        };

        const polygonGraphic = {
          type: 'Graphic',
          geometry: polygon,
          symbol: fillSymbol,
          attributes: attribute,
          popupTemplate: popupTemplates
        };

        graphicsLayer.add(polygonGraphic);
      });

      this.maps.add(graphicsLayer); 
    });
  }

  createGraphics() {
    this.graphicsLayer = new this.EsriGraphicsLayer(); 

    this.loadData();
    
    
  }

  loadData(){
    let response = this.data;
    // console.log(this.data);
    // console.log(this.paramData);
    // console.log(response.features);
    if(response.features !== ""){
      
      response.features.map((feature, i) => {
    
        const point = {
          type: 'point', // autocasts as new Point()
          x: feature.geometry.coordinates[0],
          y: feature.geometry.coordinates[1]
        };

        const marker = {
          type: 'simple-marker', // autocasts as new SimpleMarkerSymbol()
          color: [65, 113, 134],
          outline: { // autocasts as new SimpleLineSymbol()
            color: [255, 255, 255],
            width: 1
          }
        };

        const attributes = {
          ObjectID: i,
          id: feature.properties.id,
          program_nama: feature.properties.program_nama,
          indikator_nama: feature.properties.indikator_nama,
          indikator_target: feature.properties.indikator_target,
          periode_tahun: feature.properties.periode_tahun,
          periode_target: feature.properties.periode_target,
          progress_pelaksana: feature.properties.progress_pelaksana,
          progress_kuantitas: feature.properties.progress_kuantitas,
          progress_persen: feature.properties.progress_persen,
          progress_waktu_pelaporan: feature.properties.progress_waktu_pelaporan,
          progress_keterangan: feature.properties.progress_keterangan,
          nama_kota: feature.properties.nama_kota,
          nama_kecamatan: feature.properties.nama_kecamatan,
          nama_kelurahan: feature.properties.nama_kelurahan,
        }
  
        const template = {
          title: '{program_nama}',
          content: [{
            type: 'fields',
            fieldInfos: [{
              fieldName: 'id',
              label: 'id',
              visible: true
            }, {
              fieldName: 'indikator_nama',
              label: 'indikator_nama',
              visible: true
            }, {
              fieldName: 'indikator_target',
              label: 'indikator_target',
              visible: true
            }, {
              fieldName: 'periode_tahun',
              label: 'periode_tahun',
              visible: true
            }, {
              fieldName: 'periode_target',
              label: 'periode_target',
              visible: true
            }, {
              fieldName: 'progress_pelaksana',
              label: 'progress_pelaksana',
              visible: true
            }, {
              fieldName: 'progress_kuantitas',
              label: 'progress_kuantitas',
              visible: true 
            }, {
              fieldName: 'progress_persen',
              label: 'progress_persen',
              visible: true
            }, {
              fieldName: 'progress_waktu_pelaporan',
              label: 'progress_waktu_pelaporan',
              visible: true,
              format: {
                dateFormat: 'short-date-short-time'
              }
            }, {
              fieldName: 'progress_keterangan',
              label: 'progress_keterangan',
              visible: true
            }, {
              fieldName: 'nama_kota',
              label: 'nama_kota',
              visible: true
            }, {
              fieldName: 'nama_kecamatan',
              label: 'nama_kecamatan',
              visible: true
            }, {
              fieldName: 'nama_kelurahan',
              label: 'nama_kelurahan',
              visible: true
            }]
          }]
        };
        
        
        const pointGraphic = {
          type: 'Graphic',
          geometry: point,
          symbol: marker, 
          attributes: attributes,
          popupTemplate: template
        };

        this.graphicsLayer.add(pointGraphic); 
      });
    
      this.maps.add(this.graphicsLayer);
 
    }

    
  }
 
 
} 