import { Component } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { SessionService } from './shared/service/session.service';
import { GlobalsService } from './shared/service/globals.service'

@Component({
  selector: 'app-root',
  template: '<router-outlet><spinner></spinner></router-outlet>'
})
export class AppComponent {
  constructor( 
    private _constant: GlobalsService,
    private router: Router, 
    private session: SessionService,
    ) {

    this._constant.changeMessage();
    
    router.events.forEach((event: NavigationEvent) => {

      // After Navigation
      // if (event instanceof NavigationEnd) {

      // Berfore Navigation
      if (event instanceof NavigationStart) {
        const url = event.url;
        const routing = url.split('/');
        const role = this.session.getRole();

        // console.log('call every change route');
        // console.log('url : ' + url);
        // console.log('routing : ' + routing);
        // console.log('islogin : ' + this.session.isLoggedIn());
        // console.log('tokenExpired : ' + this.session.getTokenExpired());
        // console.log('check access : ' + this.session.checkAccess(url));

        window.scrollTo(0, 0)

        if ((routing[1] === 'auth') && (routing[2] === 'logout')) {
          console.log('logouting');

        } else if (routing[1] === 'error') {
          console.log('error page');

        } else {
          // console.log('masuk route normal');

          if (this.session.isLoggedIn()) {
            // console.log('loggin true');
            if(routing[1] === ''){
              routing[1] = 'dashboard';
            }

            // check role access by menu
            let can_menu = this.session.checkMenu(routing[1], routing[2]);
            // console.log(can_menu);

            // check role access by session
            // let can_access = this.session.checkAccess(routing[1], routing[2]);
            // console.log(can_access);

            if(!can_menu){
              this.router.navigate(['/error/403']);
            }
            else{
              
            }

          } else {
            // console.log('loggin false');
            if (routing[1] !== 'auth') {
              console.log('session false, redirect login');
              this.router.navigate(['/auth/login']);
            }

          }
        }

      }
    });
  }
}
