import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { SessionService } from '../../shared/service/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { MenuItems } from '../../shared/menu-items/menu-items';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public loading = false;
  public APIEndpoint = environment.APIEndpoint;
  private url_login = this.APIEndpoint + '/api/auth/local';
  private url_role = this.APIEndpoint + '/api/users-permissions/roles/';

  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private httpRequest: HttpRequestService,
    private errorMessage: ErrorMessageService,
    private session: SessionService,
    private router: Router,
    public menuItems: MenuItems) {

  }

  ngOnInit() {
    const txt_identifier = new FormControl('', [
      Validators.required
    ]);
    const txt_password = new FormControl('', 
      Validators.required
    );

    this.myForm = new FormGroup({
      identifier: txt_identifier,
      password: txt_password
    });
  }

  onSubmit() {
    this.submitted = true;
    const val_login = this.myForm.value;
    const json_login = JSON.stringify(val_login);
    // console.log(json_login);

    this.loading = true;
    this.httpRequest.httpLogin(this.url_login, json_login).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          let role = result_msg['user']['role']['type'];
          let role_id = result_msg['user']['role']['id'];
          role = role.toString().toLowerCase();
          role = role.replace('_', '-');
          // console.log(result_msg);
          // console.log(result_msg['user']);
          // console.log(role);

          this.session.logIn(result_msg['jwt'], JSON.stringify(result_msg['user']), JSON.stringify({}));
          this.loading = false;
          this.router.navigate(['/dashboard']);

          // this.httpRequest.httpRole(this.url_role + role_id, {}).subscribe(
          //   result2 => {
          //     try {
          //       const result_msg2 = JSON.parse(result2._body);
          //       // console.log(result_msg2);
          //       result_msg['user']['role'] = result_msg2['role'];
          //       // console.log(result_msg);

          //       this.session.logIn(result_msg['jwt'], JSON.stringify(result_msg['user']), JSON.stringify({}));
          //       this.loading = false;
          //       this.router.navigate(['/dashboard']);

          //     } catch (error) {
          //       this.loading = false;
          //       this.errorMessage.openErrorSwal('Something wrong.');
          //     }
          //   },
          //   error => {
          //     console.log(error);
          //     this.loading = false;
          //   }
          // );

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

}
