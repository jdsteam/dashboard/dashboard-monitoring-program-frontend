import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {LoginComponent} from "./login.component";
import {SharedModule} from "../../shared/shared.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const LoginRoutes: Routes = [
    {
        path: '',
        component: LoginComponent,
        data: {
        breadcrumb: 'Login'
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginRoutes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [LoginComponent],
    bootstrap: [LoginComponent]
})

export class LoginModule {}
