import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {CommonModule} from "@angular/common";
import {LogoutComponent} from "./logout.component";
import {SharedModule} from "../../shared/shared.module";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';

export const LogoutRoutes: Routes = [
    {
        path: '',
        component: LogoutComponent,
        data: {
        breadcrumb: 'Logout'
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LogoutRoutes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [LogoutComponent]
})

export class LogoutModule {}
