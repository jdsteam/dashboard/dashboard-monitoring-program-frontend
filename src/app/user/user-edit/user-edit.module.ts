import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {UserEditComponent} from './user-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const UserEditRoutes: Routes = [{
    path: '',
    component: UserEditComponent,
    data: {
        breadcrumb: "Edit User"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserEditRoutes),
        SharedModule,
        ShowHidePasswordModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [UserEditComponent]
})

export class UserEditModule {}
