import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: [ './user-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class UserDetailComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users';
  private url_dinas = '/api/dinas?is_deleted=false';
  private url_role = '/api/users-permissions/roles';
  disableTextbox:Boolean = true;
  dataUser = [];
  dataDinas = [];
  dataRole = [];
  
  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
  }

  ngOnInit() {
    if(!this.session.checkAccess('user','detail')){
      this.router.navigate(['/error/403']);
    }
    let id = this.route.snapshot.paramMap.get('id');
    this.getDataUser(id);
    this.getDataDinas();
    this.getDataRole();
 
    this.myForm = new FormGroup({
      id: new FormControl({value: '', disabled: true}, []), 
      username: new FormControl({value: '', disabled: true}, []), 
      nama: new FormControl({value: '', disabled: true}, []),  
      no_telp: new FormControl({value: '', disabled: true}, []), 
      alamat: new FormControl({value: '', disabled: true}, []),  
      dinas: new FormControl({value: '', disabled: true}, []), 
      role: new FormControl({value: '', disabled: true}, []), 
      password: new FormControl({value: '', disabled: true}, []), 
      password_repeat: new FormControl({value: '', disabled: true}, []), 
      email: new FormControl(),
      provider: new FormControl(),
      is_deleted: new FormControl(),
      confirmed: new FormControl(),
    });
  }
  
  
  getDataUser(id) {

    const json_user = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_user + '/'+ id, json_user).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataUser = result_msg;
          // console.log(this.dataUser);
          // set data
          this.setForm(this.dataUser);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataUser = null; 
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataUser = null;
        return null;
      }
    );
  }

  getDataDinas() {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '&_sort=kode_skpd:ASC', json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataDinas = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }

  getDataRole() {

    const json_role = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_role, json_role).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataRole = result_msg;
          // console.log(this.dataRole);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataRole = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataRole = null;
        return null;
      }
    );
  }

  setForm(data){
    this.myForm.patchValue({
      id: data.id,
      username: data.username,
      nama: data.nama,
      no_telp: data.no_telp,
      alamat: data.alamat,
      role: data.role.id,
      dinas: data.dinas.id,
      password: '**********',
      password_repeat: '**********'
    });
  }
  
  showList(){
    this.router.navigate(['/user/list'])
  }

  showAdd(){
    this.router.navigate(['/user/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/user/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/user/edit', row.id])
  }
  
  
  showBack(){
    this._location.back();
  }
 
}
