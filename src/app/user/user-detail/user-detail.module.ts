import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {UserDetailComponent} from './user-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const UserDetailRoutes: Routes = [{
    path: '',
    component: UserDetailComponent,
    data: {
        breadcrumb: "Detail User"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserDetailRoutes),
        SharedModule,
        ShowHidePasswordModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [UserDetailComponent]
})

export class UserDetailModule {}
