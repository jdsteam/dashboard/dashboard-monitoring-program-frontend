import {Routes} from '@angular/router';

export const UserRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'User',
      status: true
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './user-list/user-list.module#UserListModule'
      },
      {
        path: 'add',
        loadChildren: './user-add/user-add.module#UserAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './user-detail/user-detail.module#UserDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './user-edit/user-edit.module#UserEditModule'
      },
    ]
  }
];


