import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {UserAddComponent} from './user-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const UserAddRoutes: Routes = [{
    path: '',
    component: UserAddComponent,
    data: {
        breadcrumb: "Tambah User"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UserAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [UserAddComponent]
})

export class UserAddModule {}
