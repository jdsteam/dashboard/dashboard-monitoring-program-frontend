import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: [ './user-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class UserAddComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users';
  private url_dinas = '/api/dinas?is_deleted=false';
  private url_role = '/api/users-permissions/roles';
  disableTextbox:Boolean = true;
  dataUser = [];
  dataDinas = [];
  dataRole = [];
  
  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
  }

  ngOnInit() {
    if(!this.session.checkAccess('user','add')){
      this.router.navigate(['/error/403']);
    }
    this.getDataDinas();
    this.getDataRole();
 
    this.myForm = new FormGroup({
      username: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(6)]), 
      nama: new FormControl({value: '', disabled: false}, [Validators.required]),  
      no_telp: new FormControl({value: '', disabled: false}, [Validators.required, Validators.pattern("^[0-9]*$")]), 
      alamat: new FormControl({value: '', disabled: false}, [Validators.required]),  
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]), 
      role: new FormControl({value: '', disabled: false}, [Validators.required]), 
      password: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(6)]), 
      password_repeat: new FormControl({value: '', disabled: false}, [Validators.required, Validators.minLength(6)]), 
      email: new FormControl(),
      provider: new FormControl(),
      is_deleted: new FormControl(),
      confirmed: new FormControl(),
    },
      this.passwordMatchValidator
    );
  }

  passwordMatchValidator(control: FormGroup) {
      return control.get('password').value === control.get('password_repeat').value
        ? null : {'mismatch': true};
  }

  noWhitespaceValidator(control: FormControl) {
      const isWhitespace = (control.value || '').trim().length === 0;
      const isValid = !isWhitespace;
      return isValid ? null : { 'whitespace': true };
  }

  getDataDinas() {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '&_sort=kode_skpd:ASC', json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataDinas = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }

  getDataRole() {

    const json_role = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_role, json_role).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataRole = result_msg;
          // console.log(this.dataRole);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataRole = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataRole = null;
        return null;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_user = this.myForm.value;
    
    this.myForm.patchValue({
      email: val_user.username + '@email.com',
      provider: 'local',
      is_deleted: false,
      confirmed: true
    });

    val_user = this.myForm.value; 
    let json_user = JSON.stringify(val_user);
    // console.log(json_user);

    this.loading = true;
    this.httpRequest.httpPost(this.url_user, json_user).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.showSuccess(result_msg['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  showList(){
    this.router.navigate(['/user/list'])
  }

  showAdd(){
    this.router.navigate(['/user/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/user/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/user/edit', row.id])
  }

  
  showBack(){
    this._location.back();
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: 'User ' + nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/user/list'])
    });
    
  }


}
