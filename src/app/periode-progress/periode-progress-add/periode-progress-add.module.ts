import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeProgressAddComponent} from './periode-progress-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const PeriodeProgressAddRoutes: Routes = [{
    path: '',
    component: PeriodeProgressAddComponent,
    data: {
        breadcrumb: "Add Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeProgressAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [PeriodeProgressAddComponent]
})

export class PeriodeProgressAddModule {}
