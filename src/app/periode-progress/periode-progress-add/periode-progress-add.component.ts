import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-periode-progress-add',
  templateUrl: './periode-progress-add.component.html',
  styleUrls: [ './periode-progress-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class PeriodeProgressAddComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = false; 
 
  private url_periode = '/api/periodes'; 
  private url_periodeprogress = '/api/periodeprogresses';  
  private url_indikator = '/api/indikators';  
 
  dataPeriode = []; 
  dataPeriodeProgress = [];  
  dataIndikator = [];  
 
  myForm: FormGroup;   

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';
 
  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {

    if(!this.session.checkAccess('periodeprogress','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4]; 
    this.periode_id = '0'; 
    this.periodeprogress_id = id; 

    this.getDataPeriode();
    this.getDataIndikator(this.indikator_id);
 
    this.myForm = new FormGroup({ 
      program: new FormControl(), 
      indikator: new FormControl(),  
      periode: new FormControl({value: '', disabled: false}, [Validators.required]),  
      target: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.pattern('[1-9][0-9]*|0'), Validators.min(0)]), 
      satuan: new FormControl({value: '', disabled: false}, [Validators.required]), 
      keterangan: new FormControl({value: '', disabled: false}, []), 
      is_deleted: new FormControl(),    
    });
 
  }
 
  getDataPeriode() {

    const json_periode = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periode + '?is_deleted=false&_sort=id:asc', json_periode).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriode = result_msg;
          // console.log(this.dataPeriode);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriode = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriode = null;
        return null;
      }
    );
  }

  getDataIndikator(id) {

    const json_indikator = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_indikator + '/' + id, json_indikator).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIndikator = result_msg;
          console.log(this.dataIndikator);
          this.myForm.patchValue({
            satuan: result_msg['target_satuan']
          });

          this.myForm.controls.target.setValidators([Validators.required, 
            Validators.pattern('[1-9][0-9]*|0'), Validators.min(0),
            Validators.max(result_msg['target_kuantitatif'])]); 
          this.myForm.controls.target.updateValueAndValidity();

          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIndikator = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIndikator = null;
        return null;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_periodeprogress = this.myForm.value;  
  
    val_periodeprogress['program'] = this.program_id; 
    val_periodeprogress['indikator'] = this.indikator_id; 
    val_periodeprogress['is_deleted'] = false; 
   
    console.log(val_periodeprogress);

    this.loading = true;
    let json_periodeprogress = JSON.stringify(val_periodeprogress); 
    this.httpRequest.httpPost(this.url_periodeprogress, json_periodeprogress).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['id']); 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
   

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Periode Progress berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      const url = this.router.url;
      const routing = url.split('/'); 
      this.router.navigate(['/program/', routing[2], 'indikator', routing[4]])
    });

  }
 
    
}
