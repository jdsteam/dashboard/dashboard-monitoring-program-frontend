import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-periode-progress-detail',
  templateUrl: './periode-progress-detail.component.html',
  styleUrls: [ './periode-progress-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class PeriodeProgressDetailComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = false; 
 
  private url_program = '/api/programs';   
  private url_periodeprogress = '/api/periodeprogresses'; 
  private url_lapprogress = '/api/lapprogresses'; 

  dataProgram = [];   
  dataIndikator = [];   
  dataPeriode = [];   
  dataPeriodeProgress = []; 
  dataLapProgress = []; 

  
  pencapaianIndikatorKuantitas = 0;
  pencapaianIndikatorPersen = 0;
  pencapaianPeriodeKuantitas = 0;
  pencapaianPeriodePersen = 0;

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 
  is_verifikasi = false; 

  myForm: FormGroup;   

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';

  pencapaian_indikator = 0; 
  pencapaian_periode = 0; 


  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {

    if(!this.session.checkAccess('periodeprogress','detail')){
      this.router.navigate(['/error/403']);
    }
 
    this.is_detail = this.session.checkAccess('lapprogress','detail');
    this.is_add = this.session.checkAccess('lapprogress','add');
    this.is_edit = this.session.checkAccess('lapprogress','edit');
    this.is_delete = this.session.checkAccess('lapprogress','delete'); 
    this.is_verifikasi = this.session.checkAccess('lapprogress','verifikasi'); 
    
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4]; 
    this.periode_id = '0'; 
    this.periodeprogress_id = id; 

    this.getDataPeriodeProgress(this.periodeprogress_id);
    this.countPencapaianIndikator(this.indikator_id);
    this.countPencapaianPeriode(this.periodeprogress_id);
    
    this.myForm = new FormGroup({ });
 
  }

  getDataPeriodeProgress(id) {

    const json_periodeprogress = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeprogress + '/' + id, json_periodeprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false; 
          this.dataPeriodeProgress = result_msg; 
          this.dataIndikator = result_msg['indikator']; 
          this.dataPeriode = result_msg['periode']; 
          this.getDataProgram(result_msg['program']['id'], id);   
          // console.log('dataPeriodeProgress'); 
          // console.log(this.dataPeriodeProgress); 
          // console.log('dataIndikator'); 
          // console.log(this.dataIndikator); 
          // console.log('dataPeriode');  
          // console.log(this.dataPeriode);  
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataPeriodeProgress = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataPeriodeProgress = null;
        return null;
      }
    );
  }

  getDataProgram(program_id, periodeprogress_id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/' + program_id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;  
          this.dataProgram = result_msg; 
          this.getDataLapProgress(periodeprogress_id); 
          // console.log('dataProgram'); 
          // console.log(this.dataProgram); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataProgram = null;
        return null;
      }
    );
  }
 
  getDataLapProgress(id) {

    // console.log(id);
    const json_lapprogress = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_lapprogress + '?periodeprogress=' + id + '&is_deleted=false', json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false; 
          this.finish_loading = true;
          this.dataLapProgress = result_msg; 
          // console.log('dataLapProgress'); 
          // console.log(this.dataLapProgress); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataLapProgress = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataLapProgress = null;
        return null;
      }
    );
  }

  countPencapaianPeriode(id){
    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/?periodeprogress=' + id + '&is_deleted=false' , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + element.kuantitas;
            target = element.periodeprogress.target;
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianIndikator(id){
    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/?indikator=' + id + '&is_deleted=false' , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + element.kuantitas;
            target = element.indikator.target_kuantitatif;
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianIndikatorKuantitas = kuantitas;
          this.pencapaianIndikatorPersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onDeleteLapProgress(row) {
 
    // let json_indikator = row;
    let json_lapprogress = {};
    json_lapprogress['is_deleted'] = true;
    // console.log(json_indikator);

    this.loading = true;
    this.httpRequest.httpPut(this.url_lapprogress + '/' + row['id'], json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          this.loading = false;
          this.showSuccessLapProgress();

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 

   

  showLapProgressAdd(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id, 'progress', 'add'])
  }
  showLapProgressDetail(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id, 'progress', row['id']])
  }
  showLapProgressEdit(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id, 'progress', row['id'], 'edit'])
  }
  showLapProgressVerifikasi(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id, 'progress', row['id'], 'verifikasi'])
  }

  showLapProgressDelete(event, row){

    console.log(row);
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteLapProgress(row);
      }
    });
    
  }

  showBack(){
    this._location.back();
  }
 
  
  showSuccessLapProgress(){
    swal({
      title: 'Success',
      text: 'Laporan Progress berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataLapProgress= [];
      this.getDataLapProgress(this.periodeprogress_id); 
    });
    
  }

}
