import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeProgressDetailComponent} from './periode-progress-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const PeriodeProgressDetailRoutes: Routes = [{
    path: '',
    component: PeriodeProgressDetailComponent,
    data: {
        breadcrumb: "Detail Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeProgressDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [PeriodeProgressDetailComponent]
})

export class PeriodeProgressDetailModule {}
