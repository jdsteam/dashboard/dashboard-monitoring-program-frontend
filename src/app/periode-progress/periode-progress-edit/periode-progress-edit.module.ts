import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeProgressEditComponent} from './periode-progress-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const PeriodeProgressEditRoutes: Routes = [{
    path: '',
    component: PeriodeProgressEditComponent,
    data: {
        breadcrumb: "Edit Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeProgressEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [PeriodeProgressEditComponent]
})

export class PeriodeProgressEditModule {}
