import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {KegiatanEditComponent} from './kegiatan-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanEditRoutes: Routes = [{
    path: '',
    component: KegiatanEditComponent,
    data: {
        breadcrumb: "Edit Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(KegiatanEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [KegiatanEditComponent]
})

export class KegiatanEditModule {}
