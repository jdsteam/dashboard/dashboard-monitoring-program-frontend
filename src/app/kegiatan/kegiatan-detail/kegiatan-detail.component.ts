import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ModalBasicComponent } from '../../shared/modal-basic/modal-basic.component';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { round } from 'd3';


@Component({
  selector: 'app-kegiatan-detail',
  templateUrl: './kegiatan-detail.component.html',
  styleUrls: [ './kegiatan-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class KegiatanDetailComponent implements OnInit {

  public loading = false; 
  public finish_loading = false; 
  private url_program = '/api/programs'; 
  private url_kegiatan = '/api/kegiatans';  
  private url_periodeanggaran = '/api/periodeanggarans';  
  private url_lapanggaran = '/api/lapanggarans';  
  private url_lapanggaranplan = '/api/lapanggaranplans';  

  disableTextbox:Boolean = false; 
  dataProgram = [];
  dataKegiatan = [];  
  dataPeriodeAnggaran = [];  
  dataLapAnggaran = [];  
  dataLapAnggaranPlan = [];  
 
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 
  is_verifikasi = false; 

  myForm: FormGroup;  
  myFormLapAnggaranPlan: FormGroup;   
  myFormLapAnggaran: FormGroup;   
  periodeanggaran: FormArray; 

  pencapaianPeriodeKuantitas:number = 0;
  pencapaianPeriodePersen:number = 0;
  pencapaianKegiatanKuantitas:number = 0;
  pencapaianKegiatanPersen:number = 0;

  program_id : string = '0';
  kegiatan_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  lapanggaran_id : string = '0';

  submitted: boolean; 

  @ViewChild('showPanelLapAnggaranEdit')
  modalEdit: ModalBasicComponent;

  @ViewChild('showPanelLapAnggaranPlanEdit')
  modalPlanEdit: ModalBasicComponent;

  @ViewChild('showPanelLapAnggaranVerifikasi')
  modalVerifikasi: ModalBasicComponent;

  @ViewChild('showPanelLapAnggaranPlanVerifikasi')
  modalPlanVerifikasi: ModalBasicComponent;


  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('kegiatan','detail')){
      this.router.navigate(['/error/403']);
    }

    this.is_detail = this.session.checkAccess('lapanggaran','detail');
    this.is_add = this.session.checkAccess('lapanggaran','add');
    this.is_edit = this.session.checkAccess('lapanggaran','edit');
    this.is_delete = this.session.checkAccess('lapanggaran','delete'); 
    this.is_verifikasi = this.session.checkAccess('lapanggaran','verifikasi'); 

    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.periodeanggaran_id = routing[4];
    this.kegiatan_id = routing[6];

    this.getDataProgram(this.program_id); 
    this.getDataLapAnggaran(this.kegiatan_id); 
    this.getDataLapAnggaranPlan(this.kegiatan_id); 

    this.countPencapaianKegiatan(this.kegiatan_id);
    this.countPencapaianPeriode(this.periodeanggaran_id);

    this.myForm = new FormGroup({      
    });

    

    this.myFormLapAnggaranPlan = new FormGroup({   
      bulan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      serapan: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),   
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),  
      keterangan: new FormControl({value: '', disabled: false}, []),    
      keterangan_verifikasi: new FormControl({value: '', disabled: false}, []),    
      is_verified: new FormControl(), 
      is_deleted: new FormControl(), 
      id: new FormControl()
    });

    this.myFormLapAnggaran = new FormGroup({   
      bulan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      serapan: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),   
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),  
      keterangan: new FormControl({value: '', disabled: false}, []),    
      keterangan_verifikasi: new FormControl({value: '', disabled: false}, []),    
      is_verified: new FormControl(), 
      is_deleted: new FormControl(), 
      id: new FormControl() 
    });
  }
 
  checkPersen(value){ 
    // console.log(event);
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataKegiatan['anggaran']);
    let persen = (value / this.dataKegiatan['anggaran']) * 100;
    // console.log(persen)
    this.myFormLapAnggaran.patchValue({
      persen: persen.toFixed(2)
    });
  }
 
  checkPersenPlan(value){ 
    // console.log(event);
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataKegiatan['anggaran']);
    let persen = (value / this.dataKegiatan['anggaran']) * 100;
    // console.log(persen)
    this.myFormLapAnggaranPlan.patchValue({
      persen: persen.toFixed(2)
    });
  }


  getDataProgram(id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/' + id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataProgram = result_msg;
          console.log(result_msg); 
          this.getDataPeriodeAnggaran(this.periodeanggaran_id);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram = null;
        return null;
      }
    );
  } 

  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeanggaran + '/' + id , json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriodeAnggaran = result_msg; 
          this.periode_id = result_msg['periode']['id'];
          this.dataPeriodeAnggaran['total_anggaran'] = Number(result_msg['apbd']) + Number(result_msg['nonapbd']);
          console.log(result_msg);
          this.getDataKegiatan(this.kegiatan_id)
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriodeAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriodeAnggaran = null;
        return null;
      }
    );
  }
  
  getDataKegiatan(id) {

    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + '/' + id, json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.finish_loading = true; 
          this.dataKegiatan = result_msg; 
          console.log(result_msg);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan = null;
        return null;
      }
    );
  }

  getDataLapAnggaran(id) {

    const json_lapanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_lapanggaran + '?kegiatan=' + id + '&is_deleted=false', json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataLapAnggaran = result_msg; 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataLapAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataLapAnggaran = null;
        return null;
      }
    );
  }

  getDataLapAnggaranPlan(id) {

    const json_lapanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_lapanggaranplan + '?kegiatan=' + id + '&is_deleted=false', json_lapanggaran).subscribe(
      result => {
        try {
          console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataLapAnggaranPlan = result_msg; 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataLapAnggaranPlan = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataLapAnggaranPlan = null;
        return null;
      }
    );
  }
  
  onDeleteLapAnggaran(row) {
 
    // let json_kegiatan = row;
    let json_lapanggaran = {};
    json_lapanggaran['is_deleted'] = true;
    // console.log(json_kegiatan);

    this.loading = true;
    this.httpRequest.httpPut(this.url_lapanggaran + '/' + row['id'], json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          this.loading = false;
          this.showSuccessLapAnggaran('dihapus');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onDeleteLapAnggaranPlan(row) {
 
    // let json_kegiatan = row;
    let json_lapanggaran = {};
    json_lapanggaran['is_deleted'] = true;
    // console.log(json_kegiatan);

    this.loading = true;
    this.httpRequest.httpPut(this.url_lapanggaranplan + '/' + row['id'], json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          this.loading = false;
          this.showSuccessLapAnggaranPlan('dihapus');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitAdd() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaran.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = false;
    delete val_lapanggaran.id;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPost(this.url_lapanggaran, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaran('disimpan'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  onSubmitAddPlan() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaranPlan.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = false;
    delete val_lapanggaran.id;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPost(this.url_lapanggaranplan, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaranPlan('disimpan'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitEdit() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaran.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = false; 
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPut(this.url_lapanggaran + '/' + val_lapanggaran.id , json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaran('diupdate'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  onSubmitEditPlan() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaranPlan.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = false;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPut(this.url_lapanggaranplan + '/' + val_lapanggaran.id, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaranPlan('diupdate'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitVerifikasi() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaran.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = true; 
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPut(this.url_lapanggaran + '/' + val_lapanggaran.id , json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaran('diverifikasi'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  onSubmitVerifikasiPlan() {
    this.submitted = true;
    let val_lapanggaran = this.myFormLapAnggaranPlan.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = true;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPut(this.url_lapanggaranplan + '/' + val_lapanggaran.id, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccessLapAnggaranPlan('diverifikasi'); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  
  countPencapaianPeriode(id){
    const json_lapanggaran= {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?periodeanggaran=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.periodeanggaran.apbd) + parseInt(element.periodeanggaran.nonapbd);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianKegiatan(id){
    const json_lapanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?kegiatan=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.kegiatan.anggaran);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianKegiatanKuantitas = kuantitas;
          this.pencapaianKegiatanPersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
   
  showBack(){
    this._location.back();
  }  
   
  // showLapAnggaranAdd(row){ 
  //   this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', 'add'])
  // }
  // showLapAnggaranPlanAdd(row){ 
  //   this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', 'add'])
  // }
  // showLapAnggaranDetail(row){ 
  //   this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'detail'])
  // }
  // showLapAnggaranPlanDetail(row){ 
  //   this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'detail'])
  // }
  showLapAnggaranEdit(row){ 
    // this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'edit'])
    this.modalEdit.show();
    this.myFormLapAnggaran.patchValue({ 
      bulan: row.bulan,  
      serapan: row.serapan,   
      persen: row.persen,  
      keterangan: row.keterangan,    
      is_verified: row.is_verified, 
      is_deleted: row.is_deleted, 
      id: row.id, 
    });
  } 
  showLapAnggaranPlanEdit(row){ 
    // this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'edit'])
    this.modalPlanEdit.show();
    this.myFormLapAnggaranPlan.patchValue({ 
      bulan: row.bulan,  
      serapan: row.serapan,   
      persen: row.persen,  
      keterangan: row.keterangan,    
      is_verified: row.is_verified, 
      is_deleted: row.is_deleted, 
      id: row.id, 
    });
  } 
  showLapAnggaranVerifikasi(row){ 
    // this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'verifikasi'])
    this.modalVerifikasi.show();
    this.myFormLapAnggaran.patchValue({ 
      bulan: row.bulan,  
      serapan: row.serapan,   
      persen: row.persen,  
      keterangan: row.keterangan,    
      is_verified: row.is_verified, 
      is_deleted: row.is_deleted, 
      id: row.id, 
    });
  } 
  showLapAnggaranPlanVerifikasi(row){ 
    // this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id, 'serapan', row['id'], 'verifikasi'])
    this.modalPlanVerifikasi.show();
    this.myFormLapAnggaranPlan.patchValue({ 
      bulan: row.bulan,  
      serapan: row.serapan,   
      persen: row.persen,  
      keterangan: row.keterangan,    
      is_verified: row.is_verified, 
      is_deleted: row.is_deleted, 
      id: row.id, 
    });
  } 
  showLapAnggaranDelete(event, row){
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteLapAnggaran(row);
      }
    });
  }

  showLapAnggaranPlanDelete(event, row){
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteLapAnggaranPlan(row);
      }
    });
  }

  showSuccessLapAnggaran(text){
    swal({
      title: 'Success',
      text: 'Laporan Anggaran berhasil ' + text + '.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataLapAnggaran= []; 
      this.getDataLapAnggaran(this.kegiatan_id); 
    });
    
  }

  showSuccessLapAnggaranPlan(text){
    swal({
      title: 'Success',
      text: 'Laporan Anggaran berhasil ' + text + '.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataLapAnggaranPlan= []; 
      this.getDataLapAnggaranPlan(this.kegiatan_id); 
    });
    
  }


}
