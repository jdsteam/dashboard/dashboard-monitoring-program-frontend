import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {KegiatanDetailComponent} from './kegiatan-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanDetailRoutes: Routes = [{
    path: '',
    component: KegiatanDetailComponent,
    data: {
        breadcrumb: "Detail Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(KegiatanDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [KegiatanDetailComponent]
})

export class KegiatanDetailModule {}
