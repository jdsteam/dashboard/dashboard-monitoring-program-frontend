import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-kegiatan-add',
  templateUrl: './kegiatan-add.component.html',
  styleUrls: [ './kegiatan-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class KegiatanAddComponent implements OnInit {

  public loading = false; 
  private url_kegiatan = '/api/kegiatans';   
  private url_periodeanggaran = '/api/periodeanggarans';   

  disableTextbox:Boolean = true; 
  dataKegiatan = []; 
  dataPeriodeAnggaran = []; 
  
  myForm: FormGroup;   

  program_id : string = '0';
  kegiatan_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  lapanggaran_id : string = '0';
  
  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('kegiatan','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/'); 
    this.program_id = routing[2];
    this.periodeanggaran_id = routing[4];

    this.getDataPeriodeAnggaran(this.periodeanggaran_id);

    this.myForm = new FormGroup({ 
      kegiatan: new FormControl({value: '', disabled: false}, [Validators.required]), 
      kode_rekening: new FormControl({value: '', disabled: false}, []), 
      anggaran: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),  
      keterangan: new FormControl({value: '', disabled: false}, []),  
      is_deleted: new FormControl(),    
    });
 
  }
 

  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeanggaran  + '/' + id, json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriodeAnggaran = result_msg;
          console.log(result_msg);
   
          
          this.loading = true;
          this.httpRequest.httpGet(this.url_kegiatan + '?periodeanggaran=' + id, json_periodeanggaran).subscribe(
            result => {
              try {
                // console.log(result);
                const result_msg = JSON.parse(result._body);
                this.loading = false;
                let total_anggaran_sebelumnya = 0;
                result_msg.forEach(element => {
                  total_anggaran_sebelumnya = total_anggaran_sebelumnya + Number(element.anggaran);
                }); 
                console.log(total_anggaran_sebelumnya);
 
                let total_anggaran = Number(this.dataPeriodeAnggaran['apbd']) + Number(this.dataPeriodeAnggaran['nonapbd']) - Number(total_anggaran_sebelumnya);
                console.log(total_anggaran);

                this.myForm.controls.anggaran.setValidators([Validators.required, 
                  Validators.pattern('[1-9][0-9]*|0'), Validators.min(0), 
                  Validators.max(total_anggaran)]); 
                this.myForm.controls.anggaran.updateValueAndValidity();
      
              } catch (error) {}
            } 
          ); 


          return result_msg;


        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriodeAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriodeAnggaran = null;
        return null;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_kegiatan = this.myForm.value;  
  
    val_kegiatan['program'] = this.program_id; 
    val_kegiatan['periode'] = this.dataPeriodeAnggaran['periode']['id']; 
    val_kegiatan['periodeanggaran'] = this.dataPeriodeAnggaran['id']; 
    val_kegiatan['is_deleted'] = false; 
  
    console.log(val_kegiatan);
     
    this.loading = true;
    let json_kegiatan = JSON.stringify(val_kegiatan); 
    this.httpRequest.httpPost(this.url_kegiatan, json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['id']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  showBack(){
    this._location.back();
  }
  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Kegiatan berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id])
    });
    
  }
}
