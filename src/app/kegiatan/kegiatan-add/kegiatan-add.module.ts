import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {KegiatanAddComponent} from './kegiatan-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const KegiatanAddRoutes: Routes = [{
    path: '',
    component: KegiatanAddComponent,
    data: {
        breadcrumb: "Tambah Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(KegiatanAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [KegiatanAddComponent]
})

export class KegiatanAddModule {}
