import {Routes} from '@angular/router';

export const DinasRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'OPD',
      status: true
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './dinas-list/dinas-list.module#DinasListModule'
      },
      {
        path: 'add',
        loadChildren: './dinas-add/dinas-add.module#DinasAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './dinas-detail/dinas-detail.module#DinasDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './dinas-edit/dinas-edit.module#DinasEditModule'
      },
    ]
  }
];


