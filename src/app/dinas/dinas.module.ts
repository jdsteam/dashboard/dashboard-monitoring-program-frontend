import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DinasRoutes } from './dinas.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DinasRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class DinasModule {}
