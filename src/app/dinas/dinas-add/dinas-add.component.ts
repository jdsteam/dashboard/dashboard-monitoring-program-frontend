import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-dinas-add',
  templateUrl: './dinas-add.component.html',
  styleUrls: [ './dinas-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class DinasAddComponent implements OnInit {

  public loading = false; 
  private url_dinas = '/api/dinas'; 
  disableTextbox:Boolean = true;
  dataUser = [];
  dataDinas = [];
  dataRole = [];
  
  myForm: FormGroup;
  submitted: boolean;
   
  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
  }

  ngOnInit() {
    if(!this.session.checkAccess('dinas','add')){
      this.router.navigate(['/error/403']);
    }
    this.myForm = new FormGroup({ 
      jenis: new FormControl({value: '', disabled: false}, []), 
      nama: new FormControl({value: '', disabled: false}, []),  
      no_telp: new FormControl({value: '', disabled: false}, []), 
      alamat: new FormControl({value: '', disabled: false}, []),  
      web: new FormControl({value: '', disabled: false}, []),   
      is_deleted: new FormControl(), 
    });
  }

  onSubmit() {
    this.submitted = true;
    let val_dinas = this.myForm.value;
    
    this.myForm.patchValue({ 
      is_deleted: false, 
    });

    val_dinas = this.myForm.value; 
    let json_dinas = JSON.stringify(val_dinas);
    // console.log(json_dinas);

    this.loading = true;
    this.httpRequest.httpPost(this.url_dinas, json_dinas).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.showSuccess(result_msg['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  showList(){
    this.router.navigate(['/dinas/list'])
  }

  showAdd(){
    this.router.navigate(['/dinas/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/dinas/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/dinas/edit', row.id])
  }

  
  showBack(){
    this._location.back();
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/dinas/list'])
    });
    
  }



}
