import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {DinasAddComponent} from './dinas-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const DinasAddRoutes: Routes = [{
    path: '',
    component: DinasAddComponent,
    data: {
        breadcrumb: "Tambah OPD"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DinasAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [DinasAddComponent]
})

export class DinasAddModule {}
