import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import swal from 'sweetalert2';


@Component({
  selector: 'app-dinas-list',
  templateUrl: './dinas-list.component.html',
  styleUrls: [ './dinas-list.component.css',],
  animations: [fadeInOutTranslate]
})

export class DinasListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_dinas = '/api/dinas';
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }
  
  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];
  
  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _location: Location) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
    if(!this.session.checkAccess('dinas','list')){
      this.router.navigate(['/error/403']);
    }
 
    this.is_list = this.session.checkAccess('dinas','list');
    this.is_detail = this.session.checkAccess('dinas','detail');
    this.is_add = this.session.checkAccess('dinas','add');
    this.is_edit = this.session.checkAccess('dinas','edit');
    this.is_delete = this.session.checkAccess('dinas','delete'); 
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        // TODO[Dmitry Teplov] test with server-side paging.
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  getData(cb) {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?is_deleted=false&_sort=kode_skpd:ASC', json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.nama.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  onDelete(row) {
 
    // let json_dinas = row;
    let json_dinas = {};
    json_dinas['is_deleted'] = true;
    // console.log(json_dinas);

    this.loading = true;
    this.httpRequest.httpPut(this.url_dinas + '/' + row['id'], json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.showSuccess(row['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  showList(){
    this.router.navigate(['/dinas/list'])
  }

  showAdd(){
    this.router.navigate(['/dinas/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/dinas/detail', row.id])
  }

  showEdit(row){
    // console.log(row);
    this.router.navigate(['/dinas/edit', row.id])
  }

  
  showBack(){
    this._location.back();
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }
 
  
  showSuccess(nama){
    swal({
      title: 'Informasi',
      html: 'OPD ' + nama + ' berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.rowsFilter = [];
      this.tempFilter = [];
      this.getData((data) => {
        this.tempFilter = [...data];
        this.rowsFilter = data;
      });
    });
    
  }


}
