import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {DinasListComponent} from './dinas-list.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasListRoutes: Routes = [{
    path: '',
    component: DinasListComponent,
    data: {
        breadcrumb: "List OPD",
        status: true
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DinasListRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [DinasListComponent]
})

export class DinasListModule {}
