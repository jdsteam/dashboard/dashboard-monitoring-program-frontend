import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {DinasEditComponent} from './dinas-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const DinasEditRoutes: Routes = [{
    path: '',
    component: DinasEditComponent,
    data: {
        breadcrumb: "Update OPD"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DinasEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [DinasEditComponent]
})

export class DinasEditModule {}
