import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-dinas-detail',
  templateUrl: './dinas-detail.component.html',
  styleUrls: [ './dinas-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class DinasDetailComponent implements OnInit {

  public loading = false;
  private url_dinas = '/api/dinas';
  disableTextbox:Boolean = true;
  dataDinas = [];
  
  myForm: FormGroup;
  submitted: boolean;

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
  }

  ngOnInit() { 
    if(!this.session.checkAccess('dinas','detail')){
      this.router.navigate(['/error/403']);
    }
    let id = this.route.snapshot.paramMap.get('id'); 
    this.getDataDinas(id);

    this.myForm = new FormGroup({
      id: new FormControl({value: '', disabled: true}, [Validators.required]), 
      jenis: new FormControl({value: '', disabled: true}, [Validators.required]), 
      nama: new FormControl({value: '', disabled: true}, [Validators.required]),  
      no_telp: new FormControl({value: '', disabled: true}, [Validators.required]), 
      alamat: new FormControl({value: '', disabled: true}, [Validators.required]),  
      web: new FormControl({value: '', disabled: true}, [Validators.required]),  
    });
  }

  getDataDinas(id) {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '/'+ id, json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          // console.log(this.dataUser);
          // set data
          this.setForm(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataDinas = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }

  setForm(data){ 
    this.myForm.patchValue({
      id: data.id,
      jenis: data.jenis,
      nama: data.nama,
      no_telp: data.no_telp,
      alamat: data.alamat,
      web: data.web,
    });
  }

  showList(){
    this.router.navigate(['/dinas/list'])
  }

  showAdd(){
    this.router.navigate(['/dinas/add'])
  }
  
  showDetail(row){
    // console.log(row);
    this.router.navigate(['/dinas/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/dinas/edit', row.id])
  }
  
  showBack(){
    this._location.back();
  }
 


}
