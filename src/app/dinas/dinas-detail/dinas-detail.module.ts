import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {DinasDetailComponent} from './dinas-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const DinasDetailRoutes: Routes = [{
    path: '',
    component: DinasDetailComponent,
    data: {
        breadcrumb: "Detail OPD"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(DinasDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [DinasDetailComponent]
})

export class DinasDetailModule {}
