import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeAnggaranAddComponent} from './periode-anggaran-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import {MatInputModule, MatSelectModule, MatFormFieldModule} from '@angular/material';
import { Select2Module } from 'ng2-select2';

export const PeriodeAnggaranAddRoutes: Routes = [{
    path: '',
    component: PeriodeAnggaranAddComponent,
    data: {
        breadcrumb: "Add Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeAnggaranAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        Select2Module
    ],
    declarations: [PeriodeAnggaranAddComponent]
})

export class PeriodeAnggaranAddModule {}
