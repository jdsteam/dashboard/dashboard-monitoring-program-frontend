import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx'; 

import { IOption } from "ng-select";


@Component({
  selector: 'app-periode-anggaran-add',
  templateUrl: './periode-anggaran-add.component.html',
  styleUrls: [ './periode-anggaran-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class PeriodeAnggaranAddComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = false; 
 
  private url_periode = '/api/periodes'; 
  private url_periodeanggaran = '/api/periodeanggarans';  
  private url_apbd_skpd = '/apbd/anggaran_apbd/skpd';  
  private url_apbd_skpdsub = '/apbd/anggaran_apbd/skpdsub';  
  private url_apbd_program= '/apbd/anggaran_apbd/program';  
  private url_apbd_kegiatan= '/apbd/anggaran_apbd/kegiatan';  
  private url_rencana_serapan= '/apbd/rencana_serapan/detail';  
  private url_realisasi_serapan= '/apbd/realisasi_serapan/detail';  
  private url_kegiatan = '/api/kegiatans';   
  private url_lapanggaran = '/api/lapanggarans';  
  private url_lapanggaranplan = '/api/lapanggaranplans';  
 
  dataPeriode = []; 
  dataPeriodeAnggaran = [];  
  // dataSKPD = [];  
  dataSKPD: Array<IOption> = [];
  dataSKPDSub: any[] = [];  
  dataProgram = [];  
  dataKegiatan = [];  
 
  myForm: FormGroup;   
  apbd_detail: FormArray;   
  nonapbd_detail: FormArray;   

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  lapanggaran_id : string = '0'; 
 
  submitted: boolean; 

  show_apbd: boolean = false;
  show_nonapbd: boolean = false; 

  selectedPeriode = 0;

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('periodeanggaran','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2]; 
    this.periode_id = '0'; 
    this.periodeanggaran_id = id; 

    this.getDataPeriode(); 
 
    this.myForm = new FormGroup({ 
      program: new FormControl(),  
      periode: new FormControl({value: '', disabled: false}, [Validators.required]),  
      apbd: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]), 
      nonapbd: new FormControl({value: '0', disabled: false}, [Validators.min(0), Validators.pattern("^[0-9]*$")]), 
      sumber: new FormControl({value: '', disabled: false}, []), 
      keterangan: new FormControl({value: '', disabled: false}, []), 
      is_deleted: new FormControl(),    
      apbd_detail: new FormArray([this.createAPBDDetail()]),    
      nonapbd_detail: new FormArray([this.createNonAPBDDetail()]),    
    });
 
    this.addFieldValueAPBDDetail();
    this.removeFieldValueAPBDDetail(0);
    this.addFieldValueNonAPBDDetail();
    this.removeFieldValueNonAPBDDetail(0);
 
  }
 
  
  sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  createAPBDDetail(): FormGroup {
    return new FormGroup({
      no: new FormControl({value: '', disabled: false}), 
      kodeskpd: new FormControl({value: '', disabled: false},), 
      kodeskpdsub: new FormControl({value: '', disabled: false},), 
      kodeprogram: new FormControl({value: '', disabled: false},), 
      kodekegiatan: new FormControl({value: '', disabled: false},), 
      namaskpd: new FormControl({value: '', disabled: false},), 
      namaskpdsub: new FormControl({value: '', disabled: false},), 
      namaprogram: new FormControl({value: '', disabled: false},), 
      namakegiatan: new FormControl({value: '', disabled: false},), 
      pagu: new FormControl({value: '', disabled: false},), 
      idbl: new FormControl({value: '', disabled: false},), 
      rencana: new FormControl(),   
      realisasi: new FormControl(),   
    });
  }
  
  createNonAPBDDetail(): FormGroup {
    return new FormGroup({
      no: new FormControl({value: '', disabled: false}), 
      kegiatan: new FormControl({value: '', disabled: false},), 
      anggaran: new FormControl({value: '', disabled: false},), 
      sumber: new FormControl({value: '', disabled: false},),   
    });
  }
  
  createRencana(): FormGroup {
    return new FormGroup({
      idbl: new FormControl({value: '', disabled: false}), 
      bulan1: new FormControl({value: '', disabled: false}),   
      bulan2: new FormControl({value: '', disabled: false}),   
      bulan3: new FormControl({value: '', disabled: false}),   
      bulan4: new FormControl({value: '', disabled: false}),   
      bulan5: new FormControl({value: '', disabled: false}),   
      bulan6: new FormControl({value: '', disabled: false}),   
      bulan7: new FormControl({value: '', disabled: false}),   
      bulan8: new FormControl({value: '', disabled: false}),   
      bulan9: new FormControl({value: '', disabled: false}),   
      bulan10: new FormControl({value: '', disabled: false}),   
      bulan11: new FormControl({value: '', disabled: false}),   
      bulan12: new FormControl({value: '', disabled: false}),   
    });
  }

  createRealisasi(): FormGroup {
    return new FormGroup({
      kodegiat: new FormControl({value: '', disabled: false}), 
      bulan1: new FormControl({value: '', disabled: false}),   
      bulan2: new FormControl({value: '', disabled: false}),   
      bulan3: new FormControl({value: '', disabled: false}),   
      bulan4: new FormControl({value: '', disabled: false}),   
      bulan5: new FormControl({value: '', disabled: false}),   
      bulan6: new FormControl({value: '', disabled: false}),   
      bulan7: new FormControl({value: '', disabled: false}),   
      bulan8: new FormControl({value: '', disabled: false}),   
      bulan9: new FormControl({value: '', disabled: false}),   
      bulan10: new FormControl({value: '', disabled: false}),   
      bulan11: new FormControl({value: '', disabled: false}),   
      bulan12: new FormControl({value: '', disabled: false}),   
    });
  }
  
  addFieldValueAPBDDetail() {
    this.apbd_detail = this.myForm.get('apbd_detail') as FormArray;
    this.apbd_detail.push(this.createAPBDDetail());
    return false;
  }

  addFieldValueNonAPBDDetail() {
    this.nonapbd_detail = this.myForm.get('nonapbd_detail') as FormArray;
    this.nonapbd_detail.push(this.createNonAPBDDetail());
    return false;
  }

  removeFieldValueAPBDDetail(i) {
    if(this.apbd_detail){
      this.apbd_detail.removeAt(i);
    }
    this.dataSKPDSub.splice(i, 1);
    this.dataProgram.splice(i, 1);
    this.dataKegiatan.splice(i, 1);
    return false;
  }
  removeFieldValueNonAPBDDetail(i) {
    if(this.nonapbd_detail){
      this.nonapbd_detail.removeAt(i);
    }
    return false;
  }

  getDataPeriode() {

    const json_periode = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periode + '?is_deleted=false&_sort=id:asc', json_periode).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriode = result_msg;
          // console.log(this.dataPeriode);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriode = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriode = null;
        return null;
      }
    );
  }

  
  getDataSKPD() {

    const json_skpd = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_skpd + "?where={'tahun':'"+this.selectedPeriode+"'}", json_skpd).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataSKPD = result_msg['data'];
          // this.dataSKPD.push({
          //   "kodeskpd": "",
          //   "namaskpd": "-- Pilih OPD --"
          // })
          // result_msg['data'].forEach(element => {
          //   this.dataSKPD.push(element);
          // });

          result_msg['data'].forEach(element => {
            this.dataSKPD.push({ 
              value: element.kodeskpd,
              label: element.namaskpd
            });
          });

          // console.log(this.dataSKPD);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataSKPD = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataSKPD = null;
        return null;
      }
    );
  }

  getDataSKPDSub(kodeskpd, i) {

    const json_skpdsub = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_skpdsub + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeskpd':'"+kodeskpd+"'}", json_skpdsub).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataSKPDSub[i] = result_msg['data'];
          // this.dataSKPDSub[i] = [];
          // this.dataSKPDSub[i].push({
          //   "kodeskpdsub": "",
          //   "namaskpdsub": "-- Pilih Sub OPD --"
          // })
          // result_msg['data'].forEach(element => {
          //   this.dataSKPDSub[i].push(element)
          // });

          this.dataSKPDSub[i] = [];
          result_msg['data'].forEach(element => {
            this.dataSKPDSub[i].push({ 
              value: element.kodeskpdsub,
              label: element.namaskpdsub
            });
          });

          // console.log(this.dataSKPDSub);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataSKPDSub[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataSKPDSub[i] = null;
        return null;
      }
    );
  }

  getDataProgram(kodeskpdsub, i) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_program + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeskpdsub':'"+kodeskpdsub+"'}", json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataProgram[i] = result_msg['data'];
          // this.dataProgram[i] = [];
          // this.dataProgram[i].push({
          //   "kodeprogram": "",
          //   "namaprogram": "-- Pilih Kegiatan --"
          // })
          // result_msg['data'].forEach(element => {
          //   this.dataProgram[i].push(element);
          // });

          this.dataProgram[i] = [];
          result_msg['data'].forEach(element => {
            this.dataProgram[i].push({ 
              value: element.kodeprogram,
              label: element.namaprogram
            });
          });

          // console.log(this.dataProgram);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram[i] = null;
        return null;
      }
    );
  }

  getDataKegiatan(kodeprogram, i) {

    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_kegiatan + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeprogram':'"+kodeprogram+"'}", json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataKegiatan[i] = result_msg['data'];
          // this.dataKegiatan[i] = [];
          // this.dataKegiatan[i].push({
          //   "kodegiat": "",
          //   "namagiat": "-- Pilih Kegiatan --",
          //   "pagu": "",
          //   "idbl": ""
          // })
          // result_msg['data'].forEach(element => {
          //   this.dataKegiatan[i].push(element)
          // });

          this.dataKegiatan[i] = [];
          result_msg['data'].forEach(element => {
            this.dataKegiatan[i].push({ 
              value: element.kodegiat,
              label: element.namagiat,
              pagu: element.pagu,
              idbl: element.idbl
            });
          });

          // console.log(this.dataKegiatan);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }
  
  
  getDataRencana(idbl, i) {

    const json_rencana = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_rencana_serapan + "?where={'tahun':'"+this.selectedPeriode+"', 'idbl':'"+idbl+"'}", json_rencana).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // console.log(result_msg['data'])

          this.createRencana(); 
          this.apbd_detail.at(i).patchValue({ 
            rencana: result_msg['data'],   
          }); 

          // console.log(this.apbd_detail.value)
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }

  getDataRealisasi(kodeskpd, kodeprogram, kodekegiatan, i) {

    const json_realisasi = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_realisasi_serapan + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeskpd':'"+kodeskpd+"', 'kodeprogram':'"+kodeprogram+"', 'kodegiat':'"+kodekegiatan+"'}", json_realisasi).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // console.log(result_msg['data'])

          this.createRealisasi(); 
          this.apbd_detail.at(i).patchValue({ 
            realisasi: result_msg['data'],   
          }); 

          // console.log(this.apbd_detail.value)
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }

  onChangePeriode(eventPeriode) {
    // console.log(eventPeriode);

    let value = eventPeriode.target.value;  
    let text =  eventPeriode.target.options[eventPeriode.target.options.selectedIndex].text;
    // console.log(value)
    // console.log(text)

    this.selectedPeriode = text;
    
    this.getDataSKPD(); 
  }
  
  onChangeSKPD(eventSKPD, i) {
    // console.log(eventSKPD);

    // let target = eventSKPD.source.selected._element.nativeElement;  
    // let text = target.innerText.trim(); 
    let text = eventSKPD.label; 
    let value = eventSKPD.value;  
    // console.log(text);
    // console.log(value); 
    this.apbd_detail.at(i).patchValue({ 
      kodeskpd: value,  
      namaskpd: text,  
    }); 

    this.getDataSKPDSub(value, i); 
  }
  
  onChangeSKPDSub(eventSKPDSub, i) {
    // console.log(eventSKPDSub);

    // let target = eventSKPDSub.source.selected._element.nativeElement;  
    // let text = target.innerText.trim(); 
    let text = eventSKPDSub.label; 
    let value = eventSKPDSub.value;  
    // console.log(text);
    // console.log(value);
    this.apbd_detail.at(i).patchValue({ 
      kodeskpdsub: value,  
      namaskpdsub: text,  
    }); 

    this.getDataProgram(value, i); 
  }
  
  onChangeProgram(eventProgram, i) {
    // console.log(eventProgram);

    // let target = eventProgram.source.selected._element.nativeElement;  
    // let text = target.innerText.trim(); 
    let text = eventProgram.label; 
    let value = eventProgram.value;  
    // console.log(text);
    // console.log(value);
    this.apbd_detail.at(i).patchValue({ 
      kodeprogram: value,  
      namaprogram: text,  
    }); 

    this.getDataKegiatan(value, i); 
  }

  onChangeKegiatan(eventKegiatan, i) {
    // console.log(eventKegiatan);

    // let target = eventKegiatan.source.selected._element.nativeElement;
    // let split = target.innerText.trim().split('\n');
    let value = eventKegiatan.value;
    let text1 = eventKegiatan.label;
    let text2 = eventKegiatan.pagu;
    let text3 = eventKegiatan.idbl;  
    // console.log(text1);
    // console.log(text2);
    // console.log(text3);
    // console.log(value);
    let pagu = text2;
    pagu = pagu.replace('Rp. ', '');
    pagu = pagu.split('.').join('') 
    this.apbd_detail.at(i).patchValue({ 
      no: i,  
      kodekegiatan: value,  
      namakegiatan: text1,  
      pagu: pagu,  
      idbl: text3,  
    });  

    this.sleep(500).then(() => {
      this.getDataRencana(text3, i);
    })

    this.sleep(1000).then(() => {
      // console.log(this.apbd_detail.at(i).get)
      let kodeskpd = this.apbd_detail.controls[i].get('kodeskpd').value;
      let kodeskpdsub = this.apbd_detail.controls[i].get('kodeskpdsub').value;
      let kodeprogram = this.apbd_detail.controls[i].get('kodeprogram').value;
      let kodekegiatan = this.apbd_detail.controls[i].get('kodekegiatan').value;
      this.getDataRealisasi(kodeskpd, kodeprogram, kodekegiatan, i);
    })

    let sum = 0;
    this.apbd_detail.controls.forEach(element => {   
      let anggaran = element['controls']['pagu']['value']; 
      // console.log(anggaran) 
      anggaran = anggaran.replace('Rp. ', '');
      anggaran = anggaran.split('.').join('') 
      // console.log(anggaran)
      sum = sum + Number(anggaran); 
    });  

    this.myForm.patchValue({
      apbd: sum
    })

  } 

  countNonAPBD(event, i){  
    let j = 0;
    let sum = 0;
    this.nonapbd_detail.controls.forEach(element => {  
      if(j === i){
        sum = sum + event;
      }else { 
        sum = sum + element['controls']['anggaran']['value'];
      } 
      j = j + 1;
    });  

    
    this.nonapbd_detail.at(i).patchValue({ 
      no: i,   
    });  

    this.myForm.patchValue({
      nonapbd: sum
    })
 
  }

  onSubmit() {
    this.submitted = true;
    let val_periodeanggaran = this.myForm.value;  

    val_periodeanggaran['apbd_detail'] = {
      'arr' : val_periodeanggaran['apbd_detail'] 
    };
    val_periodeanggaran['nonapbd_detail'] = {
      'arr' : val_periodeanggaran['nonapbd_detail'] 
    };
  
    val_periodeanggaran['program'] = this.program_id;  
    val_periodeanggaran['is_deleted'] = false; 
   
    // console.log(val_periodeanggaran);

    this.loading = true;
    let json_periodeanggaran = JSON.stringify(val_periodeanggaran); 
    this.httpRequest.httpPost(this.url_periodeanggaran, json_periodeanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          // console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['id']); 
          
          if(val_periodeanggaran['apbd'] !== '0'){ 
            let i = 0;
            val_periodeanggaran['apbd_detail']['arr'].forEach(element => { 
              element['program'] = this.program_id; 
              element['periode'] = result_msg['periode']['id'];
              element['periodeanggaran'] = result_msg['id'];
              element['anggaran'] = element['pagu'];
              element['kegiatan'] = element['namakegiatan'];
              element['kategori'] = 'apbd'; 
              delete element.realisasi;
              delete element.rencana;
              this.onSubmitKegiatan(element, i, 'apbd'); 
              i++;
            });
          }

          if(val_periodeanggaran['nonapbd'] !== '0'){ 
            let j = 0;
            val_periodeanggaran['nonapbd_detail']['arr'].forEach(element => { 
              element['program'] = this.program_id; 
              element['periode'] = result_msg['periode']['id'];
              element['periodeanggaran'] = result_msg['id'];
              element['anggaran'] = element['anggaran'];
              element['kegiatan'] = element['kegiatan'];
              element['kategori'] = 'nonapbd';
              element['non_namakegiatan'] = element['kegiatan'];
              element['non_pagu'] = element['anggaran'];
              element['non_sumber'] = element['sumber'];
              delete element.sumber; 
              this.onSubmitKegiatan(element, j, 'nonapbd'); 
              j++;
            });
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitKegiatan(val_kegiatan, i, type) { 
  
    val_kegiatan['is_deleted'] = false;   
    // console.log(val_kegiatan);
     
    this.loading = true;
    let json_kegiatan = JSON.stringify(val_kegiatan); 
    this.httpRequest.httpPost(this.url_kegiatan, json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          // console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

          if(type === 'apbd'){
            
            let val_lapanggaran = {};
            val_lapanggaran['program'] = result_msg['program']['id'];
            val_lapanggaran['periodeanggaran'] = result_msg['periodeanggaran']['id'];
            val_lapanggaran['periode'] = result_msg['periode']['id'];
            val_lapanggaran['kegiatan'] = result_msg['id'];
            
            let realisasi = this.apbd_detail.controls[i].get('realisasi').value;
            realisasi.forEach(element => { 
              
              for(let j=1; j<=12; j++){ 
                let element2 = val_lapanggaran;
                if(j== 1){
                  element2['bulan'] = 'Januari';
                }else if(j== 2){
                  element2['bulan'] = 'Februari';
                }else if(j== 3){
                  element2['bulan'] = 'Maret';
                }else if(j== 4){
                  element2['bulan'] = 'April';
                }else if(j== 5){
                  element2['bulan'] = 'Mei';
                }else if(j== 6){
                  element2['bulan'] = 'Juni';
                }else if(j== 7){
                  element2['bulan'] = 'Juli';
                }else if(j== 8){
                  element2['bulan'] = 'Agustus';
                }else if(j== 9){
                  element2['bulan'] = 'September';
                }else if(j== 10){
                  element2['bulan'] = 'Oktober';
                }else if(j== 11){
                  element2['bulan'] = 'November';
                }else if(j== 12){
                  element2['bulan'] = 'Desember';
                }
                element2['serapan'] = element['bulan'+j];
                element2['persen'] = (Number(element['bulan'+j]) / Number(this.apbd_detail.controls[i].get('pagu').value)) * 100;
                this.onSubmitLapAnggaran(element2); 
              }
              
            });

            let rencana = this.apbd_detail.controls[i].get('rencana').value;
            rencana.forEach(element => { 
              
              for(let j=1; j<=12; j++){ 
                let element2 = val_lapanggaran;
                if(j== 1){
                  element2['bulan'] = 'Januari';
                }else if(j== 2){
                  element2['bulan'] = 'Februari';
                }else if(j== 3){
                  element2['bulan'] = 'Maret';
                }else if(j== 4){
                  element2['bulan'] = 'April';
                }else if(j== 5){
                  element2['bulan'] = 'Mei';
                }else if(j== 6){
                  element2['bulan'] = 'Juni';
                }else if(j== 7){
                  element2['bulan'] = 'Juli';
                }else if(j== 8){
                  element2['bulan'] = 'Agustus';
                }else if(j== 9){
                  element2['bulan'] = 'September';
                }else if(j== 10){
                  element2['bulan'] = 'Oktober';
                }else if(j== 11){
                  element2['bulan'] = 'November';
                }else if(j== 12){
                  element2['bulan'] = 'Desember';
                }
                element2['serapan'] = element['bulan'+j];
                element2['persen'] = (Number(element['bulan'+j]) / Number(this.apbd_detail.controls[i].get('pagu').value)) * 100;
                this.onSubmitLapAnggaranPlan(element2); 
              }
              
            }); 
  
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  onSubmitLapAnggaran(val_lapanggaran) {

    val_lapanggaran['is_deleted'] = false;  
    val_lapanggaran['is_verified'] = true;  
    val_lapanggaran['keterangan_verifikasi'] = 'by system apbd';  
    // console.log(val_lapanggaran);

    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPost(this.url_lapanggaran, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitLapAnggaranPlan(val_lapanggaranplan) {

    val_lapanggaranplan['is_deleted'] = false;  
    val_lapanggaranplan['is_verified'] = true;  
    val_lapanggaranplan['keterangan_verifikasi'] = 'by system apbd';  
    // console.log(val_lapanggaranplan);

    this.loading = true;
    let json_lapanggaranplan = JSON.stringify(val_lapanggaranplan); 
    this.httpRequest.httpPost(this.url_lapanggaranplan, json_lapanggaranplan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
   

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Periode Anggaran berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      const url = this.router.url;
      const routing = url.split('/'); 
      this.router.navigate(['/program/', routing[2]])
    });

  }
 
    
}
