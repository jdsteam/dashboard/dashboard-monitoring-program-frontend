import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeAnggaranDetailComponent} from './periode-anggaran-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const PeriodeAnggaranDetailRoutes: Routes = [{
    path: '',
    component: PeriodeAnggaranDetailComponent,
    data: {
        breadcrumb: "Detail Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeAnggaranDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [PeriodeAnggaranDetailComponent]
})

export class PeriodeAnggaranDetailModule {}
