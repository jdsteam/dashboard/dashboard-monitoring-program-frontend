import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { round } from 'd3';


@Component({
  selector: 'app-periode-anggaran-detail',
  templateUrl: './periode-anggaran-detail.component.html',
  styleUrls: [ './periode-anggaran-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class PeriodeAnggaranDetailComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = false; 
 
  private url_program = '/api/programs';   
  private url_periodeanggaran = '/api/periodeanggarans'; 
  private url_kegiatan = '/api/kegiatans'; 
  private url_lapanggaran = '/api/lapanggarans'; 

  dataProgram = [];   
  dataPeriode = [];   
  dataPeriodeAnggaran = []; 
  dataKegiatan = [];    

  pencapaianPeriodeKuantitas:number = 0;
  pencapaianPeriodePersen:number = 0;
  pencapaianKegiatanKuantitas:number = 0;
  pencapaianKegiatanPersen:number = 0;
 
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  myForm: FormGroup;   

  program_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  kegiatan_id : string = '0';
  lapanggaran_id : string = '0';


  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {

    if(!this.session.checkAccess('periodeanggaran','detail')){
      this.router.navigate(['/error/403']);
    }
    
    this.is_detail = this.session.checkAccess('kegiatan','detail');
    this.is_add = this.session.checkAccess('kegiatan','add');
    this.is_edit = this.session.checkAccess('kegiatan','edit');
    this.is_delete = this.session.checkAccess('kegiatan','delete'); 

    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.periodeanggaran_id = routing[4]; 
    this.periode_id = '0';  

    this.getDataPeriodeAnggaran(this.periodeanggaran_id);
    this.countPencapaianPeriode(this.periodeanggaran_id);
    
    this.myForm = new FormGroup({ });
 
  }

  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeanggaran + '/' + id, json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false; 
          this.dataPeriodeAnggaran = result_msg;  
          this.dataPeriodeAnggaran['total_anggaran'] = Number(result_msg['apbd']) + Number(result_msg['nonapbd']);  
          this.dataPeriode = result_msg['periode']; 
          this.getDataProgram(result_msg['program']['id'], id);   
          // console.log('dataPeriodeAnggaran'); 
          // console.log(this.dataPeriodeAnggaran); 
          // console.log('dataIndikator'); 
          // console.log(this.dataIndikator); 
          // console.log('dataPeriode');  
          // console.log(this.dataPeriode);  
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataPeriodeAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataPeriodeAnggaran = null;
        return null;
      }
    );
  }

  getDataProgram(program_id, periodeanggaran_id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/' + program_id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;  
          this.dataProgram = result_msg; 
          this.getDataKegiatan(periodeanggaran_id); 
          // console.log('dataProgram'); 
          // console.log(this.dataProgram); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataProgram = null;
        return null;
      }
    );
  }
 
  getDataKegiatan(id) {

    // console.log(id);
    const json_kegiatan = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + '?periodeanggaran=' + id + '&is_deleted=false', json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false; 
          this.finish_loading = true;
          this.dataKegiatan = result_msg; 
          this.countPencapaianKegiatan(result_msg);
          // console.log('dataLapAnggaran'); 
          // console.log(this.dataLapAnggaran); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.finish_loading = true;
          this.dataKegiatan = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.finish_loading = true;
        this.dataKegiatan = null;
        return null;
      }
    );
  }

  onDeleteKegiatan(row) {
 
    // let json_indikator = row;
    let json_kegiatan = {};
    json_kegiatan['is_deleted'] = true;
    // console.log(json_indikator);

    this.loading = true;
    this.httpRequest.httpPut(this.url_kegiatan + '/' + row['id'], json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);
 
          // get lap anggaran by kegiatan
          this.httpRequest.httpGet(this.url_lapanggaran + '?kegiatan=' + row['id'], json_kegiatan).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body);
                result_msg.forEach(element => {

                  // delete lap anggaran
                  this.httpRequest.httpPut(this.url_lapanggaran + '/' + element['id'], json_kegiatan).subscribe();
                  
                });
              } catch (error) { 
              }
            }
          );
          
          this.loading = false;
          this.showSuccessKegiatan();
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 

  countPencapaianPeriode(id){
    const json_lapanggaran= {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?periodeanggaran=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.periodeanggaran.apbd) + parseInt(element.periodeanggaran.nonapbd);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = pencapaian * 1;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianKegiatan(row){

    let i = 0;
    console.log(row);
    row.forEach(element => {
       
      const json_lapanggaran = {};
      this.loading = true;  

      this.httpRequest.httpGet(this.url_lapanggaran+ '/?kegiatan=' + element.id + '&is_deleted=false' , json_lapanggaran).subscribe(
        result => {
          try {
            // console.log(result);
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;
            temp.forEach(element => {
              // console.log(element); 
              kuantitas = kuantitas + parseInt(element.serapan);
              target = parseInt(element.kegiatan.anggaran);
            }); 
            pencapaian = (kuantitas / target) * 100; 
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            // console.log('-----------');
            // console.log(i);
            // console.log(element.id);
            // console.log(row[i]);
            // console.log(kuantitas);
            // console.log(target);
            // console.log(pencapaian)
            // console.log('-----------'); 
            let index = this.dataKegiatan.indexOf(element);
            this.dataKegiatan[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataKegiatan[index]['pencapaian_persen'] = pencapaian;
            this.loading = false; 
            i = i + 1;

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
    });
  }
   

  showKegiatanAdd(row){ 
    this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', 'add'])
  }
  showKegiatanDetail(row){ 
    this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', row['id']])
  }
  showKegiatanEdit(row){ 
    this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', row['id'], 'edit'])
  } 

  showKegiatanDelete(event, row){

    console.log(row);
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteKegiatan(row);
      }
    });
    
  }

  showBack(){
    this._location.back();
  }
 
  
  showSuccessKegiatan(){
    swal({
      title: 'Success',
      text: 'Kegiatan berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataKegiatan= [];
      this.getDataKegiatan(this.periodeanggaran_id); 
    });
    
  }

}
