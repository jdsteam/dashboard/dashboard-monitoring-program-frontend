import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-periode-anggaran-edit',
  templateUrl: './periode-anggaran-edit.component.html',
  styleUrls: [ './periode-anggaran-edit.component.css',],
  animations: [fadeInOutTranslate]
})
export class PeriodeAnggaranEditComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = false; 
 
  private url_periode = '/api/periodes'; 
  private url_periodeanggaran = '/api/periodeanggarans';   
  private url_apbd_skpd = '/apbd/anggaran_apbd/skpd';  
  private url_apbd_skpdsub = '/apbd/anggaran_apbd/skpdsub';  
  private url_apbd_program= '/apbd/anggaran_apbd/program';  
  private url_apbd_kegiatan= '/apbd/anggaran_apbd/kegiatan';  
  private url_rencana_serapan= '/apbd/rencana_serapan/detail';  
  private url_realisasi_serapan= '/apbd/realisasi_serapan/detail';  
  private url_kegiatan = '/api/kegiatans';   
  private url_lapanggaran = '/api/lapanggarans';  
  private url_lapanggaranplan = '/api/lapanggaranplans';  
 
  dataPeriode = []; 
  dataPeriodeAnggaran = [];  
  dataKegiatanSave = [];  
  dataSKPD = [];  
  dataSKPDSub = [];  
  dataProgram = [];  
  dataKegiatan = [];  

  myForm: FormGroup;   
  apbd_detail: FormArray;   
  nonapbd_detail: FormArray;   

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  lapanggaran_id : string = '0';
 
  selectedPeriode = '0';

  submitted: boolean; 

  show_apbd: boolean = true;
  show_nonapbd: boolean = true; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {

    if(!this.session.checkAccess('periodeanggaran','edit')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2]; 
    this.periode_id = '0'; 
    this.periodeanggaran_id = id; 

    this.getDataPeriode();
    this.getDataPeriodeAnggaran(this.periodeanggaran_id); 
 
    this.myForm = new FormGroup({ 
      id: new FormControl(), 
      program: new FormControl(),  
      periode: new FormControl({value: '', disabled: false}, [Validators.required]),  
      apbd: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]), 
      nonapbd: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]), 
      sumber: new FormControl({value: '', disabled: false}, []), 
      keterangan: new FormControl({value: '', disabled: false}, []), 
      is_deleted: new FormControl(),    
      apbd_detail: new FormArray([this.createAPBDDetail()]),    
      nonapbd_detail: new FormArray([this.createNonAPBDDetail()]),      
    });
 
    this.addFieldValueAPBDDetail();
    this.removeFieldValueAPBDDetail(0);
    this.addFieldValueNonAPBDDetail();
    this.removeFieldValueNonAPBDDetail(0);
 
  }
 
  createAPBDDetail(): FormGroup {
    return new FormGroup({
      id: new FormControl(),  
      program: new FormControl(),  
      periodeanggaran: new FormControl(),  
      periode: new FormControl(),  
      no: new FormControl({value: '', disabled: false}), 
      kodeskpd: new FormControl({value: '', disabled: false}, [Validators.required]), 
      kodeskpdsub: new FormControl({value: '', disabled: false}, [Validators.required]), 
      kodeprogram: new FormControl({value: '', disabled: false}, [Validators.required]), 
      kodekegiatan: new FormControl({value: '', disabled: false}, [Validators.required]), 
      namaskpd: new FormControl({value: '', disabled: false}, [Validators.required]), 
      namaskpdsub: new FormControl({value: '', disabled: false}, [Validators.required]), 
      namaprogram: new FormControl({value: '', disabled: false}, [Validators.required]), 
      namakegiatan: new FormControl({value: '', disabled: false}, [Validators.required]), 
      pagu: new FormControl({value: '', disabled: false}, [Validators.required]), 
      idbl: new FormControl({value: '', disabled: false}, [Validators.required]), 
      rencana: new FormControl(),   
      realisasi: new FormControl(),    
      readonly: new FormControl({value: false}),    
    });
  }
  
  createNonAPBDDetail(): FormGroup {
    return new FormGroup({
      id: new FormControl(),  
      program: new FormControl(),  
      periodeanggaran: new FormControl(),  
      periode: new FormControl(),  
      no: new FormControl({value: '', disabled: false}), 
      kegiatan: new FormControl({value: '', disabled: false}, [Validators.required]), 
      anggaran: new FormControl({value: '', disabled: false}, [Validators.required]), 
      sumber: new FormControl({value: '', disabled: false}, [Validators.required]),    
      readonly: new FormControl({value: false}),  
    });
  }
  
  createRencana(): FormGroup {
    return new FormGroup({
      idbl: new FormControl({value: '', disabled: false}), 
      bulan1: new FormControl({value: '', disabled: false}),   
      bulan2: new FormControl({value: '', disabled: false}),   
      bulan3: new FormControl({value: '', disabled: false}),   
      bulan4: new FormControl({value: '', disabled: false}),   
      bulan5: new FormControl({value: '', disabled: false}),   
      bulan6: new FormControl({value: '', disabled: false}),   
      bulan7: new FormControl({value: '', disabled: false}),   
      bulan8: new FormControl({value: '', disabled: false}),   
      bulan9: new FormControl({value: '', disabled: false}),   
      bulan10: new FormControl({value: '', disabled: false}),   
      bulan11: new FormControl({value: '', disabled: false}),   
      bulan12: new FormControl({value: '', disabled: false}),   
    });
  }

  createRealisasi(): FormGroup {
    return new FormGroup({
      kodegiat: new FormControl({value: '', disabled: false}), 
      bulan1: new FormControl({value: '', disabled: false}),   
      bulan2: new FormControl({value: '', disabled: false}),   
      bulan3: new FormControl({value: '', disabled: false}),   
      bulan4: new FormControl({value: '', disabled: false}),   
      bulan5: new FormControl({value: '', disabled: false}),   
      bulan6: new FormControl({value: '', disabled: false}),   
      bulan7: new FormControl({value: '', disabled: false}),   
      bulan8: new FormControl({value: '', disabled: false}),   
      bulan9: new FormControl({value: '', disabled: false}),   
      bulan10: new FormControl({value: '', disabled: false}),   
      bulan11: new FormControl({value: '', disabled: false}),   
      bulan12: new FormControl({value: '', disabled: false}),   
    });
  }

  addFieldValueAPBDDetail() {
    this.apbd_detail = this.myForm.get('apbd_detail') as FormArray;
    this.apbd_detail.push(this.createAPBDDetail()); 
    this.editFieldValueAPBDDetail(this.apbd_detail.length-1)
    return false;
  }

  addFieldValueNonAPBDDetail() {
    this.nonapbd_detail = this.myForm.get('nonapbd_detail') as FormArray;
    this.nonapbd_detail.push(this.createNonAPBDDetail());
    this.editFieldValueNonAPBDDetail(this.nonapbd_detail.length-1)
    return false;
  }

  removeFieldValueAPBDDetail(i) {
    // console.log(this.apbd_detail.at(i).value);
    if(this.apbd_detail){
      this.apbd_detail.removeAt(i);
    }
    return false;
  }
  removeFieldValueNonAPBDDetail(i) {
    // console.log(this.nonapbd_detail.at(i).value);
    if(this.nonapbd_detail){
      this.nonapbd_detail.removeAt(i);
    }
    return false;
  } 
  
  editFieldValueAPBDDetail(i) { 
    this.apbd_detail.at(i).patchValue({ 
      readonly: false,   
    }); 
    return false;
  }
  editFieldValueNonAPBDDetail(i) {
    this.nonapbd_detail.at(i).patchValue({ 
      readonly: false,   
    }); 
    return false;
  }
  
  saveFieldValueAPBDDetail(i) { 
    console.log(this.apbd_detail.at(i).value);
    this.apbd_detail.at(i).patchValue({ 
      readonly: true,   
    }); 
    let values = {};
    values['id'] = this.apbd_detail.at(i).value['id'];
    values['no'] = i;
    values['program'] = this.program_id;
    values['periode'] = this.periode_id;
    values['periodeanggaran'] = this.periodeanggaran_id;
    values['kegiatan'] = this.apbd_detail.at(i).value['namakegiatan'];
    values['anggaran'] = Number(this.apbd_detail.at(i).value['pagu']);
    values['kategori'] = 'apbd';
    values['kodeskpd'] = this.apbd_detail.at(i).value['kodeskpd'];
    values['kodeskpdsub'] = this.apbd_detail.at(i).value['kodeskpdsub'];
    values['kodeprogram'] = this.apbd_detail.at(i).value['kodeprogram'];
    values['kodeprogram'] = this.apbd_detail.at(i).value['kodeprogram'];
    values['kodekegiatan'] = this.apbd_detail.at(i).value['kodekegiatan'];
    values['namaskpd'] = this.apbd_detail.at(i).value['namaskpd'];
    values['namaskpdsub'] = this.apbd_detail.at(i).value['namaskpdsub'];
    values['namaprogram'] = this.apbd_detail.at(i).value['namaprogram'];
    values['namaprogram'] = this.apbd_detail.at(i).value['namaprogram'];
    values['namakegiatan'] = this.apbd_detail.at(i).value['namakegiatan'];
    values['idbl'] = this.apbd_detail.at(i).value['idbl'];
    values['pagu'] = this.apbd_detail.at(i).value['pagu'];

    console.log(values);
    if(values['id']){
      console.log('update')
      this.onUpdateKegiatan(values);
    }else{
      console.log('insert')
      delete values['id'];
      this.onSubmitKegiatan(values, i, 'apbd');
    }

    this.onUpdate()
    
    return false;
  }

  saveFieldValueNonAPBDDetail(i) {
    console.log(this.nonapbd_detail.at(i).value);
    this.nonapbd_detail.at(i).patchValue({ 
      readonly: true,   
    }); 
    let values = {};
    values['id'] = this.nonapbd_detail.at(i).value['id'];
    values['no'] = i;
    values['program'] = this.program_id;
    values['periode'] = this.periode_id;
    values['periodeanggaran'] = this.periodeanggaran_id;
    values['kegiatan'] = this.nonapbd_detail.at(i).value['kegiatan'];
    values['anggaran'] = Number(this.nonapbd_detail.at(i).value['anggaran']);
    values['kategori'] = 'nonapbd';
    values['non_namakegiatan'] = this.nonapbd_detail.at(i).value['kegiatan'];
    values['non_pagu'] = this.nonapbd_detail.at(i).value['anggaran'];
    values['non_sumber'] = this.nonapbd_detail.at(i).value['sumber'];

    console.log(values);
    if(values['id']){
      console.log('update')
      this.onUpdateKegiatan(values);
    }else{
      console.log('insert')
      delete values['id'];
      this.onSubmitKegiatan(values, i, 'nonapbd');
    }

    this.countALL("", 0);
    this.onUpdate()
    
    return false;
  }

  getDataPeriode() {

    const json_periode = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periode + '?is_deleted=false&_sort=id:asc', json_periode).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriode = result_msg;
          // console.log(this.dataPeriode);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriode = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriode = null;
        return null;
      }
    );
  }

  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeanggaran + '/' + id, json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriodeAnggaran = result_msg;
          this.periode_id = result_msg['periode']['id'];
          this.getDataKegiatanSave(this.periodeanggaran_id);
          // console.log(this.dataPeriodeAnggaran);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriodeAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriodeAnggaran = null;
        return null;
      }
    );
  }
 
  getDataKegiatanSave(id) {

    const json_periodeanggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_kegiatan + '?periodeanggaran=' + id + '&is_deleted=false', json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataKegiatanSave = result_msg;
  
          this.dataPeriodeAnggaran['apbd_detail']['arr'] = [];
          this.dataPeriodeAnggaran['nonapbd_detail']['arr'] = [];
          this.dataKegiatanSave.forEach(element => {
            if(element['kategori'] === 'apbd'){   
              this.dataPeriodeAnggaran['apbd_detail']['arr'].push(element)
            }else if(element['kategori'] === 'nonapbd'){  
              this.dataPeriodeAnggaran['nonapbd_detail']['arr'].push(element)
            }  
          });
          
          this.setForm(this.dataPeriodeAnggaran, this.dataKegiatanSave);

          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatanSave = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatanSave = null;
        return null;
      }
    );
  }

  getDataSKPD() {

    const json_skpd = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_skpd + "?where={'tahun':'"+this.selectedPeriode+"'}", json_skpd).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataSKPD = result_msg['data'];
          this.dataSKPD.push({
            "kodeskpd": "",
            "namaskpd": "-- Pilih OPD --"
          })
          result_msg['data'].forEach(element => {
            this.dataSKPD.push(element);
          });
          // console.log(this.dataSKPD);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataSKPD = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataSKPD = null;
        return null;
      }
    );
  }

  getDataSKPDSub(kodeskpd, i) {

    const json_skpdsub = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_skpdsub + "?where={'tahun':'"+this.selectedPeriode+"' , 'kodeskpd':'"+kodeskpd+"'}", json_skpdsub).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataSKPDSub[i] = result_msg['data'];
          this.dataSKPDSub[i] = [];
          this.dataSKPDSub[i].push({
            "kodeskpdsub": "",
            "namaskpdsub": "-- Pilih Sub OPD --"
          })
          result_msg['data'].forEach(element => {
            this.dataSKPDSub[i].push(element)
          });
          // console.log(this.dataSKPDSub);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataSKPDSub[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataSKPDSub[i] = null;
        return null;
      }
    );
  }

  getDataProgram(kodeskpdsub, i) {

    const json_program = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_program + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeskpdsub':'"+kodeskpdsub+"'}", json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataProgram[i] = result_msg['data'];
          this.dataProgram[i] = [];
          this.dataProgram[i].push({
            "kodeprogram": "",
            "namaprogram": "-- Pilih Kegiatan --"
          })
          result_msg['data'].forEach(element => {
            this.dataProgram[i].push(element);
          });
          // console.log(this.dataProgram);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram[i] = null;
        return null;
      }
    );
  }

  getDataKegiatan(kodeprogram, i) {

    const json_kegiatan = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_apbd_kegiatan + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeprogram':'"+kodeprogram+"'}", json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // this.dataKegiatan[i] = result_msg['data'];
          this.dataKegiatan[i] = [];
          this.dataKegiatan[i].push({
            "kodegiat": "",
            "namagiat": "-- Pilih Kegiatan --",
            "pagu": "",
            "idbl": ""
          })
          result_msg['data'].forEach(element => {
            this.dataKegiatan[i].push(element)
          });
          // console.log(this.dataKegiatan);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }
 
  getDataRencana(idbl, i) {

    const json_rencana = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_rencana_serapan + "?where={'tahun':'"+this.selectedPeriode+"', 'idbl':'"+idbl+"'}", json_rencana).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // console.log(result_msg['data'])

          this.createRencana(); 
          this.apbd_detail.at(i).patchValue({ 
            rencana: result_msg['data'],   
          }); 

          // console.log(this.apbd_detail.value)
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }

  getDataRealisasi(kodeskpd, kodeprogram, kodekegiatan, i) {

    const json_realisasi = {};
    // this.loading = true;
    this.httpRequest.httpGet(this.url_realisasi_serapan + "?where={'tahun':'"+this.selectedPeriode+"', 'kodeskpd':'"+kodeskpd+"', 'kodeprogram':'"+kodeprogram+"', 'kodegiat':'"+kodekegiatan+"'}", json_realisasi).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          // console.log(result_msg['data'])

          this.createRealisasi(); 
          this.apbd_detail.at(i).patchValue({ 
            realisasi: result_msg['data'],   
          }); 

          // console.log(this.apbd_detail.value)
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan[i] = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan[i] = null;
        return null;
      }
    );
  }
  
  onChangeSKPD(eventSKPD, i) {
    // console.log(eventSKPD);

    let target = eventSKPD.source.selected._element.nativeElement;  
    let text = target.innerText.trim(); 
    let value = eventSKPD.value;  
    // console.log(text);
    // console.log(value); 
    this.apbd_detail.at(i).patchValue({ 
      kodeskpd: value,  
      namaskpd: text,  
    }); 
    this.getDataSKPDSub(value, i); 
  }
  
  onChangeSKPDSub(eventSKPDSub, i) {
    // console.log(eventSKPDSub);

    let target = eventSKPDSub.source.selected._element.nativeElement;  
    let text = target.innerText.trim(); 
    let value = eventSKPDSub.value;  
    // console.log(text);
    // console.log(value);
    this.apbd_detail.at(i).patchValue({ 
      kodeskpdsub: value,  
      namaskpdsub: text,  
    }); 
    this.getDataProgram(value, i); 
  }
  
  onChangeProgram(eventProgram, i) {
    // console.log(eventProgram);

    let target = eventProgram.source.selected._element.nativeElement;  
    let text = target.innerText.trim(); 
    let value = eventProgram.value;  
    // console.log(text);
    // console.log(value);
    this.apbd_detail.at(i).patchValue({ 
      kodeprogram: value,  
      namaprogram: text,  
    }); 
    this.getDataKegiatan(value, i); 
  }

  onChangeKegiatan(eventKegiatan, i) {
    // console.log(eventKegiatan);

    let target = eventKegiatan.source.selected._element.nativeElement;
    let split = target.innerText.trim().split('\n');
    let value = eventKegiatan.value;
    let text1 = split[0];
    let text2 = split[1];
    let text3 = split[2];  
    // console.log(text1);
    // console.log(text2);
    // console.log(text3);
    // console.log(value);
    let pagu = text2;
    pagu = pagu.replace('Rp. ', '');
    pagu = pagu.split('.').join('') 
    this.apbd_detail.at(i).patchValue({ 
      no: i,  
      kodekegiatan: value,  
      namakegiatan: text1,  
      pagu: pagu,  
      idbl: text3,  
    });  

    this.sleep(500).then(() => {
      this.getDataRencana(text3, i);
    })

    this.sleep(1000).then(() => {
      // console.log(this.apbd_detail.at(i).get)
      let kodeskpd = this.apbd_detail.controls[i].get('kodeskpd').value;
      let kodeskpdsub = this.apbd_detail.controls[i].get('kodeskpdsub').value;
      let kodeprogram = this.apbd_detail.controls[i].get('kodeprogram').value;
      let kodekegiatan = this.apbd_detail.controls[i].get('kodekegiatan').value;
      this.getDataRealisasi(kodeskpd, kodeprogram, kodekegiatan, i);
    })


    let sum = 0;
    this.apbd_detail.controls.forEach(element => {   
      let anggaran = element['controls']['pagu']['value']; 
      console.log(anggaran) 
      anggaran = anggaran.replace('Rp. ', '');
      anggaran = anggaran.split('.').join('') 
      console.log(anggaran)
      sum = sum + Number(anggaran); 
    });  
    console.log(sum)

    this.myForm.patchValue({
      apbd: sum
    })

  } 

  countAPBD(event, i){   
    let j = 0;
    let sum = 0;
    this.apbd_detail.controls.forEach(element => {   
      if(j === i){
        sum = sum + Number(event);
      }else { 
        sum = sum + Number(element['controls']['pagu']['value']);
      } 
      j = j + 1;
    });   

    
    // this.nonapbd_detail.at(i).patchValue({ 
    //   no: i,   
    // });  

    this.myForm.patchValue({
      apbd: sum
    })
 
  }

  countNonAPBD(event, i){   
    let j = 0;
    let sum = 0;
    this.nonapbd_detail.controls.forEach(element => {   
      if(j === i){
        sum = sum + Number(event);
      }else { 
        sum = sum + Number(element['controls']['anggaran']['value']);
      } 
      j = j + 1;
    });   

    
    // this.nonapbd_detail.at(i).patchValue({ 
    //   no: i,   
    // });  

    this.myForm.patchValue({
      nonapbd: sum
    })
 
  }

  countALL(type, i){    
    let sum_apbd = 0;
    let sum_nonapbd = 0;
    let y = 0;
    let z = 0;

    // console.log(i)

    this.apbd_detail.controls.forEach(element => {     
      sum_apbd = sum_apbd + Number(element['controls']['pagu']['value']);   
    });

    this.nonapbd_detail.controls.forEach(element => {      
      sum_nonapbd = sum_nonapbd + Number(element['controls']['anggaran']['value']);   
    });

    this.myForm.patchValue({
      apbd: sum_apbd,
      nonapbd: sum_nonapbd
    })
 
  }
 
  onUpdate() {
    this.submitted = true;
    let val_periodeanggaran = this.myForm.value;  
  
    val_periodeanggaran['apbd_detail'] = {
      'arr' : val_periodeanggaran['apbd_detail'] 
    };
    val_periodeanggaran['nonapbd_detail'] = {
      'arr' : val_periodeanggaran['nonapbd_detail'] 
    };
  
    val_periodeanggaran['program'] = this.program_id;  
    val_periodeanggaran['is_deleted'] = false; 
    
    console.log(val_periodeanggaran);

    this.loading = true;
    let json_periodeanggaran = JSON.stringify(val_periodeanggaran); 
    this.httpRequest.httpPut(this.url_periodeanggaran + '/' + val_periodeanggaran['id'], json_periodeanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['id']);  
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitKegiatan(val_kegiatan, i, type) { 
  
    val_kegiatan['is_deleted'] = false;   
    console.log(val_kegiatan);
     
    this.loading = true;
    let json_kegiatan = JSON.stringify(val_kegiatan); 
    this.httpRequest.httpPost(this.url_kegiatan, json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

          if(type === 'apbd'){
            
            let val_lapanggaran = {};
            val_lapanggaran['program'] = result_msg['program']['id'];
            val_lapanggaran['periodeanggaran'] = result_msg['periodeanggaran']['id'];
            val_lapanggaran['periode'] = result_msg['periode']['id'];
            val_lapanggaran['kegiatan'] = result_msg['id'];
            
            let realisasi = this.apbd_detail.controls[i].get('realisasi').value;
            realisasi.forEach(element => { 
              
              for(let j=1; j<=12; j++){ 
                let element2 = val_lapanggaran;
                if(j== 1){
                  element2['bulan'] = 'Januari';
                }else if(j== 2){
                  element2['bulan'] = 'Februari';
                }else if(j== 3){
                  element2['bulan'] = 'Maret';
                }else if(j== 4){
                  element2['bulan'] = 'April';
                }else if(j== 5){
                  element2['bulan'] = 'Mei';
                }else if(j== 6){
                  element2['bulan'] = 'Juni';
                }else if(j== 7){
                  element2['bulan'] = 'Juli';
                }else if(j== 8){
                  element2['bulan'] = 'Agustus';
                }else if(j== 9){
                  element2['bulan'] = 'September';
                }else if(j== 10){
                  element2['bulan'] = 'Oktober';
                }else if(j== 11){
                  element2['bulan'] = 'November';
                }else if(j== 12){
                  element2['bulan'] = 'Desember';
                }
                element2['serapan'] = element['bulan'+j];
                element2['persen'] = (Number(element['bulan'+j]) / Number(this.apbd_detail.controls[i].get('pagu').value)) * 100;
                this.onSubmitLapAnggaran(element2); 
              }
              
            });

            let rencana = this.apbd_detail.controls[i].get('rencana').value;
            rencana.forEach(element => { 
              
              for(let j=1; j<=12; j++){ 
                let element2 = val_lapanggaran;
                if(j== 1){
                  element2['bulan'] = 'Januari';
                }else if(j== 2){
                  element2['bulan'] = 'Februari';
                }else if(j== 3){
                  element2['bulan'] = 'Maret';
                }else if(j== 4){
                  element2['bulan'] = 'April';
                }else if(j== 5){
                  element2['bulan'] = 'Mei';
                }else if(j== 6){
                  element2['bulan'] = 'Juni';
                }else if(j== 7){
                  element2['bulan'] = 'Juli';
                }else if(j== 8){
                  element2['bulan'] = 'Agustus';
                }else if(j== 9){
                  element2['bulan'] = 'September';
                }else if(j== 10){
                  element2['bulan'] = 'Oktober';
                }else if(j== 11){
                  element2['bulan'] = 'November';
                }else if(j== 12){
                  element2['bulan'] = 'Desember';
                }
                element2['serapan'] = element['bulan'+j];
                element2['persen'] = (Number(element['bulan'+j]) / Number(this.apbd_detail.controls[i].get('pagu').value)) * 100;
                this.onSubmitLapAnggaranPlan(element2); 
              }
              
            }); 
  
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  onSubmitLapAnggaran(val_lapanggaran) {

    val_lapanggaran['is_deleted'] = false;  
    val_lapanggaran['is_verified'] = true;  
    val_lapanggaran['keterangan_verifikasi'] = 'by system apbd';  
    console.log(val_lapanggaran);

    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPost(this.url_lapanggaran, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmitLapAnggaranPlan(val_lapanggaranplan) {

    val_lapanggaranplan['is_deleted'] = false;  
    val_lapanggaranplan['is_verified'] = true;  
    val_lapanggaranplan['keterangan_verifikasi'] = 'by system apbd';  
    console.log(val_lapanggaranplan);

    this.loading = true;
    let json_lapanggaranplan = JSON.stringify(val_lapanggaranplan); 
    this.httpRequest.httpPost(this.url_lapanggaranplan, json_lapanggaranplan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']); 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onUpdateKegiatan(val_kegiatan) { 
   
    console.log(val_kegiatan);
     
    this.loading = true;
    let json_kegiatan = JSON.stringify(val_kegiatan); 
    this.httpRequest.httpPut(this.url_kegiatan + '/' + val_kegiatan['id'], json_kegiatan).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          // this.showSuccess(result_msg['id']);  

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 
  onDeleteKegiatan(id_kegiatan) { 
   
    console.log(id_kegiatan);
     
    this.loading = true;
    let json_kegiatan = {};
    json_kegiatan['is_deleted'] = true; 
    this.httpRequest.httpPut(this.url_kegiatan + '/' + id_kegiatan, json_kegiatan).subscribe(
      result => {
        try { 

          // get all laporan
          this.httpRequest.httpGet(this.url_lapanggaran + '?kegiatan=' + id_kegiatan, json_kegiatan).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body);  

                // delete 
                if(result_msg.length > 0){
                  result_msg.forEach(element => {
                    let json_laporan = {};
                    json_laporan['is_deleted'] = true; 
                    this.httpRequest.httpPut(this.url_lapanggaran + '/' + element['id'], json_laporan).subscribe(
                      result => {
                        try {} catch (error) {}
                      },
                      error => {}
                    );
                  });
                }
                
      
              } catch (error) {}
            },
            error => {}
          );

          // get all laporan plan
          this.httpRequest.httpGet(this.url_lapanggaranplan + '?kegiatan=' + id_kegiatan, json_kegiatan).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body);  

                // delete 
                if(result_msg.length > 0){
                  result_msg.forEach(element => {
                    let json_laporan = {};
                    json_laporan['is_deleted'] = true; 
                    this.httpRequest.httpPut(this.url_lapanggaranplan + '/' + element['id'], json_laporan).subscribe(
                      result => {
                        try {} catch (error) {}
                      },
                      error => {}
                    );
                  }); 
                }
      
              } catch (error) {}
            },
            error => {}
          );


        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  setForm(dataPeriodeAnggaran, dataKegiatanSave){  

    console.log(dataPeriodeAnggaran); 
    console.log(dataKegiatanSave); 

    this.selectedPeriode = dataPeriodeAnggaran['periode']['tahun'];

    // add dynamic component
    let y = 0;
    let z = 0; 
    dataKegiatanSave.forEach(element => {
      if(element['kategori'] === 'apbd'){  
        if(y > 0){
          this.addFieldValueAPBDDetail(); 
        }
        y = y + 1; 
      }else if(element['kategori'] === 'nonapbd'){ 
        if(z > 0){
          this.addFieldValueNonAPBDDetail();  
        }
        z = z + 1; 
      }  
    }); 

    this.getDataSKPD(); 
    let round = 0;
    for (let i = 0; i < dataPeriodeAnggaran.apbd_detail.arr.length; i++) {   

      round = round + 1;
      this.sleep(500 * round).then(() => {
        this.getDataSKPDSub(dataPeriodeAnggaran.apbd_detail.arr[i]['kodeskpd'], i);
      })
      round = round + 1;
      this.sleep(500 * round).then(() => {
        this.getDataProgram(dataPeriodeAnggaran.apbd_detail.arr[i]['kodeskpdsub'], i);
      })
      round = round + 1;
      this.sleep(500 * round).then(() => {
        this.getDataKegiatan(dataPeriodeAnggaran.apbd_detail.arr[i]['kodeprogram'], i); 
      }) 

      // console.log(dataPeriodeAnggaran.apbd_detail.arr[i]) 
      let pagu = dataPeriodeAnggaran.apbd_detail.arr[i]['pagu'];
      pagu = pagu.replace('Rp. ', '');
      pagu = pagu.split('.').join('');
      // console.log(pagu)
      this.apbd_detail.at(i).patchValue({ 
        id: dataPeriodeAnggaran.apbd_detail.arr[i]['id'],  
        no: dataPeriodeAnggaran.apbd_detail.arr[i]['no'],  
        kodeskpd: dataPeriodeAnggaran.apbd_detail.arr[i]['kodeskpd'],  
        kodeskpdsub: dataPeriodeAnggaran.apbd_detail.arr[i]['kodeskpdsub'],  
        kodeprogram: dataPeriodeAnggaran.apbd_detail.arr[i]['kodeprogram'],  
        kodekegiatan: dataPeriodeAnggaran.apbd_detail.arr[i]['kodekegiatan'],   
        namaskpd: dataPeriodeAnggaran.apbd_detail.arr[i]['namaskpd'],  
        namaskpdsub: dataPeriodeAnggaran.apbd_detail.arr[i]['namaskpdsub'],  
        namaprogram: dataPeriodeAnggaran.apbd_detail.arr[i]['namaprogram'],  
        namakegiatan: dataPeriodeAnggaran.apbd_detail.arr[i]['namakegiatan'],  
        pagu: pagu,  
        idbl: dataPeriodeAnggaran.apbd_detail.arr[i]['idbl'],  
        readonly: true,  
      });  
    } 

    for (let i = 0; i < dataPeriodeAnggaran.nonapbd_detail.arr.length; i++) {  
      this.nonapbd_detail.at(i).patchValue({ 
        id: dataPeriodeAnggaran.nonapbd_detail.arr[i]['id'],  
        no: dataPeriodeAnggaran.nonapbd_detail.arr[i]['no'],  
        kegiatan: dataPeriodeAnggaran.nonapbd_detail.arr[i]['kegiatan'],  
        anggaran: dataPeriodeAnggaran.nonapbd_detail.arr[i]['anggaran'],  
        sumber: dataPeriodeAnggaran.nonapbd_detail.arr[i]['non_sumber'],   
        readonly: true,   
      }); 
    } 

    console.log()

    this.myForm.patchValue({
      id: dataPeriodeAnggaran.id,
      periode: dataPeriodeAnggaran.periode.id,
      apbd: dataPeriodeAnggaran.apbd, 
      nonapbd: dataPeriodeAnggaran.nonapbd,
      sumber: dataPeriodeAnggaran.sumber,
      keterangan: dataPeriodeAnggaran.keterangan,
      is_deleted: dataPeriodeAnggaran.is_deleted, 
      // apbd_detail: dataPeriodeAnggaran.apbd_detail.arr, 
      // nonapbd_detail: dataPeriodeAnggaran.nonapbd_detail.arr, 
    });
 

  }
   

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Periode Anggaran berhasil diupdate.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      // this.router.navigate(['/program/',this.program_id])
    });
    
  }
 
  showDelete(event, row, i, type) {

    console.log(event)
    console.log(row.controls.id.value)
    console.log(i)
    console.log(type)
    try { 
      console.log(this.apbd_detail.at(i).value)
    } catch (error) {
      
    }

    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log()

      if (result) {
        if(typeof row.controls.id !== "undefined"){
          if(type === 'apbd'){ 
            this.removeFieldValueAPBDDetail(i); 
            this.dataSKPDSub.splice(i, 1);
            this.dataProgram.splice(i, 1);
            this.dataKegiatan.splice(i, 1);
            this.countALL('apbd', i)
            this.onDeleteKegiatan(row.controls.id.value);
            this.onUpdate();
          }else if(type === 'nonapbd'){ 
            this.removeFieldValueNonAPBDDetail(i);
            this.countALL('nonapbd', i)
            this.onDeleteKegiatan(row.controls.id.value);
            this.onUpdate();
          }
        }else{ 
          if(type === 'apbd'){ 
            this.removeFieldValueAPBDDetail(i); 
          }else if(type === 'nonapbd'){ 
            this.removeFieldValueNonAPBDDetail(i);
          }
        }
      }
      
    });
  }


}
