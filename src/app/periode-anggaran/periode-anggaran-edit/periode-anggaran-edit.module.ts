import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {PeriodeAnggaranEditComponent} from './periode-anggaran-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import {MatInputModule, MatSelectModule, MatFormFieldModule} from '@angular/material';
import { Select2Module } from 'ng2-select2';

export const PeriodeAnggaranEditRoutes: Routes = [{
    path: '',
    component: PeriodeAnggaranEditComponent,
    data: {
        breadcrumb: "Edit Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PeriodeAnggaranEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        Select2Module
    ],
    declarations: [PeriodeAnggaranEditComponent]
})

export class PeriodeAnggaranEditModule {}
