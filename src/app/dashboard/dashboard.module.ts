import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import {SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(DashboardRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({}),
  ],
  declarations: [DashboardComponent]
})

export class DashboardModule {}
