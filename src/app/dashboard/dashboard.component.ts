import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../shared/service/converter.service';
import { HttpRequestService } from '../shared/service/http-request.service';
import { SessionService } from '../shared/service/session.service';
import { ErrorMessageService } from '../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import  "../../assets/jq-vmap/jquery.vmap.js";
import "../../assets/jq-vmap/maps/jquery.vmap.world.js";
import {cardToggle, cardClose} from './card-animation'; 
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
declare var $:any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './dashboard.component.css',
    "../../assets/jq-vmap/jqvmap.css"
  ],
  animations: [cardToggle, cardClose],
})

export class DashboardComponent implements OnInit {

  public loading = false;
  public finish_loading = false;
  private url_program = '/api/programs';   
  private url_indikator = '/api/indikators';   
  private url_dinas = '/api/dinas';   
  private url_lapprogress = '/api/lapprogresses';   

  dataProgram = [];
  dataIndikator = [];  
  chart = [];
  countProgram = 0;
  countDinas = 0;  
  
  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _sanitizer: DomSanitizer) {
    this.getDataProgram(); 
    this.getCountProgram();
    this.getCountDinas();
  } 

  ngOnInit() { 
    if(!this.session.checkAccess('dashboard','list')){
      this.router.navigate(['/error/403']);
    }
  }

  basicChart = {
    type: 'doughnut',
    data: { 
      labels: [
        'Finish', 'Unfinish'
      ], 
      datasets: [{
        data: [0, 100], 
        backgroundColor: [
          '#23038e',
          '#E6E6E6', 
        ],
        borderWidth: [
          '0px',
          '0px', 
        ],
        borderColor: [
          '#23038e',
          '#E6E6E6', 
  
        ]
      }]
    },
    options: {
      title: {
        display: true,
        text: 'Progress terhadap Output Indikator (5 tahun)',
        position: 'bottom'
      },
      legend: {
        display: false,
        labels: {
          fontColor: '#000000'
        },
        position: 'top'
      },
      tooltips: {
        enabled: false
      },
      plugins: {
        labels: {
        render: 'percentage',
        fontColor: ['#ffffff', ''],
        precision: 2
        }
      }
    }
  };
  
  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  toggleCard(row) { 
    let index = this.dataProgram.indexOf(row); 
    let stat = '';
    if(this.dataProgram[index]['cardToggle']  === 'collapsed'){
      stat = 'expanded';
    }else{
      stat = 'collapsed';
    } 
    this.dataProgram[index]['cardToggle'] = stat; 
  } 

  getCountProgram() {
    const json_program = {};
    this.loading = true;

    const sess = JSON.parse(this.session.getData());
    // console.log(sess);
    // console.log(sess['role']['type']);

    let query = "";
    if((sess['role']['type'] == 'dinas') || (sess['role']['type'] == 'pelapor')){
      query = "/count?is_deleted=false&dinas="+sess['dinas']+"&_sort=id";
    }else{
      query = "/count?is_deleted=false&_sort=id";
    }
    // console.log(query);
    this.httpRequest.httpGet(this.url_program + query, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.countProgram = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getCountDinas() {
    const json_program = {};
    this.loading = true;

    const sess = JSON.parse(this.session.getData());
    // console.log(sess['dinas']);

    let query = ""; 
    query = "/count?is_deleted=false&_sort=kode_skpd:ASC";
     
    // console.log(query);
    this.httpRequest.httpGet(this.url_dinas + query, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.countDinas = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataProgram() {

    const json_program = {};
    this.loading = true;

    const sess = JSON.parse(this.session.getData());
    // console.log(sess);
    // console.log(sess['role']['type']);

    let query = "";
    if((sess['role']['type'] == 'dinas') || (sess['role']['type'] == 'pelapor')){
      query = "?is_deleted=false&dinas="+sess['dinas']+"&_sort=nama:asc";
    }else{
      query = "?is_deleted=false&_sort=nama:asc";
    }
    // console.log(query);

    this.httpRequest.httpGet(this.url_program + query, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataProgram = result_msg; 
          // this.loading = false;
          // console.log(result_msg)
          // console.log("=============")
          result_msg.forEach(element => { 
            let index = this.dataProgram.indexOf(element);
            let temp = {
              'finish' : 0,
              'unfinish': 100
            };
            this.dataProgram[index]['finish_loading'] = false;
            this.dataProgram[index]['dataPersen'] = temp;
            this.dataProgram[index]['cardToggle'] = 'collapsed';
            this.dataProgram[index]['cardClose'] = 'open';  
            this.dataProgram[index]['chart'] = this.basicChart; 
            this.getDataIndikator(element); 
          });

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataIndikator(row) {

    // console.log(row)
    // console.log('-----------');
    const json_indikator = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_indikator + '?program='+ row.id  + '&is_deleted=false&_sort=id:asc', json_indikator).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // this.loading = false;
          
          let index = this.dataProgram.indexOf(row);
          this.dataProgram[index]['dataIndikator'] = result_msg; 
          this.countPencapaianIndikator(result_msg, index);
          // console.log(this.dataProgram); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIndikator = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIndikator = null;
        return null;
      }
    );
  }
 

  countPencapaianIndikator(row, index){

    // console.log(row)
    row.forEach(element => {
      
      let i = 0;
      const json_lapprogress = {};
      this.loading = true; 
      // console.log(query);

      this.httpRequest.httpGet(this.url_lapprogress+ '/?indikator=' + element.id + '&is_deleted=false&is_verified=true' , json_lapprogress).subscribe(
        result => {
          try {
            // console.log(result);
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;
            temp.forEach(element => {
              // console.log(element); 
              kuantitas = kuantitas + element.kuantitas;
              target = element.indikator.target_kuantitatif;
            });
            pencapaian = (kuantitas / target) * 100;
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            // console.log(i);
            // console.log("indikator id " + element.id);
            // console.log("target " + target);
            // console.log("kuantitas " + kuantitas);
            // console.log("pencapaian " + pencapaian);
            // console.log('-----------'); 
            // console.log(row[i]);
            let index2 = this.dataProgram[index]['dataIndikator'].indexOf(element);
            this.dataProgram[index]['dataIndikator'][index2]['pencapaian_kuantitas'] = kuantitas;
            this.dataProgram[index]['dataIndikator'][index2]['pencapaian_persen'] = pencapaian;

            // console.log(this.dataProgram[index]['dataIndikator'])
            // this.loading = false;
            // i = i + 1; 
            this.countAllProgressIndikator(index, index2);

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
      
    });
  }

  
  countAllProgressIndikator(index, index2){
 
    try {
      let sum_persen = 0;
      let persen = 0;
      let tableBar = [];
  
      // tableBar.push(['Indikator', 'Persentase']);
  
      this.dataProgram[index]['dataIndikator'].forEach(element => { 
        sum_persen = sum_persen + parseFloat(element.pencapaian_persen); 
        // console.log(element.pencapaian_persen)
        // console.log(sum_persen) 
      });
      persen = sum_persen / this.dataProgram[index]['dataIndikator'].length;
      // console.log(persen) 
      this.dataProgram[index]['dataPersen']['finish'] = persen;  
      this.dataProgram[index]['dataPersen']['unfinish'] = 100-persen;   
      this.dataProgram[index]['chart'] = 
      {
        type: 'doughnut',
        data: { 
          labels: [
            'Selesai', 'Belum Selesai'
          ], 
          datasets: [{
            data: [persen, 100-persen], 
            backgroundColor: [
              '#23038e',
              '#E6E6E6', 
            ], 
          }]
        },
        options: {
          title: {
            display: false,
            text: 'Progress terhadap Output Indikator (5 tahun)',
            position: 'bottom'
          },
          legend: {
            display: false, 
          },
          tooltips: {
            enabled: true,
            callbacks: {
              label: function(tooltipItem, data) {
                return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
              }
            } 
          },
          plugins: {
            labels: {
            render: 'percentage',
            fontColor: ['#ffffff', ''],
            precision: 2
            }
          }
        }
      };

      let arr_label = [];
      let arr_data = [];
      this.dataProgram[index]['dataIndikator'].forEach(element => {
        // arr_label.push(element.indikator);
        arr_label.push(element.indikator);
        arr_data.push(element.pencapaian_persen);
      });
      this.dataProgram[index]['chart_bar'] = 
      {
        type: 'bar',
        data: { 
          labels: arr_label, 
          datasets: [{
            data: arr_data, 
            backgroundColor: '#23038e', 
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          cutoutPercentage: 80,
          legend: {
            display: false, 
          },
          tooltips: {
            enabled: true,
            callbacks: {
              label: function(tooltipItem, data) {
                return data['datasets'][0]['data'][tooltipItem['index']] + '%';
              }
            } 
          },
          scales:{
            xAxes: [{ 
              display: false,  
            }], 
            yAxes: [{
              ticks : {
                max: 100
              }
            }]
          },
          plugins: {
            labels: false
          }
        }
      };
      this.loading = false;
      this.dataProgram[index]['finish_loading'] = true;    
      // console.log(this.dataProgram)
      
    } catch (error) {
      console.log(error)
    }
  }


  
  


}
