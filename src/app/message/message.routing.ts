import {Routes} from '@angular/router';
// import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

export const MessageRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Pesan',
      status: true
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './message-list/message-list.module#MessageListModule'
      },
      {
        path: 'detail/:id',
        // component: NotificationDetailComponent
        loadChildren: './message-detail/message-detail.module#MessageDetailModule'
      },
    ]
  }
];
