import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {MessageDetailComponent} from './message-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const MessageDetailRoutes: Routes = [{
    path: '',
    component: MessageDetailComponent,
    data: {
        breadcrumb: "Detail Notifikasi"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MessageDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [MessageDetailComponent]
})

export class MessageDetailModule {}
