import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MessageListComponent } from './message-list.component';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const MessageListRoutes: Routes = [{
    path: '',
    component: MessageListComponent,
    data: {
        breadcrumb: "List Pesan",
        status: true
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(MessageListRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [MessageListComponent]
})

export class MessageListModule {}
