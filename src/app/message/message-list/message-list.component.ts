import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import swal from 'sweetalert2';


@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: [ './message-list.component.css',],
  animations: [fadeInOutTranslate]
})

export class MessageListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_notification = '/api/notifications'; 
  idUser: any;
  dataMessage: [];

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 
  
  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _location: Location
  ) { 
    this._constant.changeMessage();
  }

  ngOnInit() {
    if(!this.session.checkAccess('message','list')){
      this.router.navigate(['/error/403']);
    }

    const sess = JSON.parse(this.session.getData());
    this.idUser = sess['id'];

    this.getData();
 
    this.is_list = this.session.checkAccess('message','list');
    this.is_detail = this.session.checkAccess('message','detail');
    this.is_add = this.session.checkAccess('message','add');
    this.is_edit = this.session.checkAccess('message','edit');
    this.is_delete = this.session.checkAccess('message','delete'); 
  }

  getData() {
    const filter = '?type=broadcast&penerima=' + this.idUser + '&_sort=created_at:DESC';
    const json_notification = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_notification + filter, json_notification).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataMessage = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      });
  }

  showList(){
    this.router.navigate(['/message/list'])
  }

  showDetail(row){
    this.router.navigate(['/message/detail', row.id])
  }
  
  showBack(){
    this._location.back();
  }

}
