import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageRoutes } from './message.routing';
// import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MessageRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    // NotificationDetailComponent
  ]
})

export class MessageModule {}


