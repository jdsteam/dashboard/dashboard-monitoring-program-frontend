import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {IndikatorAddComponent} from './indikator-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const IndikatorAddRoutes: Routes = [{
    path: '',
    component: IndikatorAddComponent,
    data: {
        breadcrumb: "Tambah Indikator"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(IndikatorAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [IndikatorAddComponent]
})

export class IndikatorAddModule {}
