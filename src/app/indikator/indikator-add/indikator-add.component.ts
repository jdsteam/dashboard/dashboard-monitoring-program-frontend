import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-indikator-add',
  templateUrl: './indikator-add.component.html',
  styleUrls: [ './indikator-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class IndikatorAddComponent implements OnInit {

  public loading = false; 
  private url_indikator = '/api/indikators';  
  private url_periode = '/api/periodes'; 

  disableTextbox:Boolean = true; 
  dataIndikator = [];
  dataPeriode = []; 
  
  myForm: FormGroup;   

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';
  
  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('indikator','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/'); 
    this.program_id = routing[2];

    this.getDataPeriode();

    this.myForm = new FormGroup({ 
      indikator: new FormControl({value: '', disabled: false}, [Validators.required]), 
      target: new FormControl({value: '', disabled: false}, [Validators.required]),  
      target_kuantitatif: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]), 
      target_satuan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      is_deleted: new FormControl(),    
    });
 
  }
 

  getDataPeriode() {

    const json_periode = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periode + '?is_deleted=false&_sort=id:asc', json_periode).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriode = result_msg;
          // console.log(this.dataPeriode);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriode = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriode = null;
        return null;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_indikator = this.myForm.value;  
  
    val_indikator['program'] = this.program_id; 
    val_indikator['is_deleted'] = false; 
 
    delete val_indikator['periodeprogress'];
    console.log(val_indikator);
     
    this.loading = true;
    let json_indikator = JSON.stringify(val_indikator); 
    this.httpRequest.httpPost(this.url_indikator, json_indikator).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['id']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  showBack(){
    this._location.back();
  }
  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Indikator berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      this.router.navigate(['/program/', this.program_id])
    });
    
  }
}
