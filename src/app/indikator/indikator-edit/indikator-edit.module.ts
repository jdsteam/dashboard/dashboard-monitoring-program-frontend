import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {IndikatorEditComponent} from './indikator-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const IndikatorEditRoutes: Routes = [{
    path: '',
    component: IndikatorEditComponent,
    data: {
        breadcrumb: "Edit Indikator"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(IndikatorEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [IndikatorEditComponent]
})

export class IndikatorEditModule {}
