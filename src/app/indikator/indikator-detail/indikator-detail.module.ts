import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {IndikatorDetailComponent} from './indikator-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const IndikatorDetailRoutes: Routes = [{
    path: '',
    component: IndikatorDetailComponent,
    data: {
        breadcrumb: "Detail Indikator"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(IndikatorDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [IndikatorDetailComponent]
})

export class IndikatorDetailModule {}
