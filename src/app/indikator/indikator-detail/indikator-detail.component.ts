import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-indikator-detail',
  templateUrl: './indikator-detail.component.html',
  styleUrls: [ './indikator-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class IndikatorDetailComponent implements OnInit {

  public loading = false; 
  public finish_loading = false; 
  private url_program = '/api/programs'; 
  private url_indikator = '/api/indikators';  
  private url_periodeprogress = '/api/periodeprogresses';  
  private url_lapprogress = '/api/lapprogresses';  

  disableTextbox:Boolean = true; 
  dataProgram = [];
  dataIndikator = [];  
  dataPeriodeProgress = [];   
 
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  pencapaianIndikatorKuantitas = 0;
  pencapaianIndikatorPersen = 0;
  pencapaianPeriodeKuantitas = 0;
  pencapaianPeriodePersen = 0;

  myForm: FormGroup;  
  periodeprogress: FormArray; 

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';

  submitted: boolean; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('indikator','detail')){
      this.router.navigate(['/error/403']);
    }
    
    this.is_detail = this.session.checkAccess('periodeprogress','detail');
    this.is_add = this.session.checkAccess('periodeprogress','add');
    this.is_edit = this.session.checkAccess('periodeprogress','edit');
    this.is_delete = this.session.checkAccess('periodeprogress','delete'); 

    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4];

    this.getDataIndikator(id); 
    this.countPencapaianIndikator(this.indikator_id);

    this.myForm = new FormGroup({      
    });
 
  }
 
  getDataIndikator(id) {

    const json_indikator = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_indikator + '/' + id, json_indikator).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIndikator = result_msg;
          this.getDataProgram(result_msg['program']['id']);   
          this.getDataPeriodeProgress(this.program_id, this.indikator_id);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIndikator = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIndikator = null;
        return null;
      }
    );
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/' + id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.finish_loading = true; 
          this.dataProgram = result_msg;
          // console.log(this.dataPeriode); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram = null;
        return null;
      }
    );
  } 

  getDataPeriodeProgress(program_id, indikator_id) {

    const json_periodeprogress = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periodeprogress + '?program=' + program_id + '&indikator=' + indikator_id + '&is_deleted=false', json_periodeprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataPeriodeProgress = result_msg;
          // console.log(this.dataPeriodeProgress);
          this.countPencapaianPeriode(result_msg);
          // this.setForm(this.dataIndikator, this.dataPeriodeProgress);
 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriodeProgress = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriodeProgress = null;
        return null;
      }
    );
  }
  
  countPencapaianPeriode(row){
 
    let i = 0;
    row.forEach(element => {
 
      const json_lapprogress = {};
      this.loading = true;  

      this.httpRequest.httpGet(this.url_lapprogress+ '/?periodeprogress=' + element.id + '&is_deleted=false' , json_lapprogress).subscribe(
        result => {
          try {
            // console.log(result);
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;
            temp.forEach(element => {
              // console.log(element);
              kuantitas = kuantitas + element.kuantitas;
              target = element.periodeprogress.target;
            });
            pencapaian = (kuantitas / target) * 100; 
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            // console.log('-----------');
            // console.log(i);
            // console.log(element.id);
            // console.log(kuantitas);
            // console.log(target);
            // console.log(pencapaian)
            // console.log('-----------'); 
            // console.log(row[i]);
            let index = this.dataPeriodeProgress.indexOf(element);
            this.dataPeriodeProgress[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataPeriodeProgress[index]['pencapaian_persen'] = pencapaian;

            this.loading = false; 
            i = i + 1;

          } catch (error) {
            console.log(error);
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
      
    });
    
  }

  countPencapaianIndikator(id){
    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/?indikator=' + id + '&is_deleted=false' , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + element.kuantitas;
            target = element.indikator.target_kuantitatif;
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianIndikatorKuantitas = kuantitas;
          this.pencapaianIndikatorPersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  onDeletePeriodeProgress(row) {
 
    // let json_indikator = row;
    let json_periodeprogress = {};
    json_periodeprogress['is_deleted'] = true;
    // console.log(json_indikator);

    this.loading = true;
    this.httpRequest.httpPut(this.url_periodeprogress + '/' + row['id'], json_periodeprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          // get lap progress by periode progress
          this.httpRequest.httpGet(this.url_lapprogress + '?periodeprogress=' + row['id'], json_periodeprogress).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body);
                result_msg.forEach(element => {

                  // delete lap progress
                  this.httpRequest.httpPut(this.url_lapprogress + '/' + element['id'], json_periodeprogress).subscribe();
                  
                });
              } catch (error) { 
              }
            }
          );
          
          this.loading = false;
          this.showSuccessPeriodeProgress(row['nama'], result_msg['program']['id']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
   
  showBack(){
    this._location.back();
  }  
   
  showPeriodeProgressAdd(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', 'add'])
  }
  showPeriodeProgressDetail(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', row['id'], 'detail'])
  }
  showPeriodeProgressEdit(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', row['id'], 'edit'])
  } 
  showPeriodeProgressDelete(event, row){
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeletePeriodeProgress(row);
      }
    });
  }

  showSuccessPeriodeProgress(program_id, indikator_id){
    swal({
      title: 'Success',
      text: 'Periode Progress berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataPeriodeProgress= [];
      this.getDataPeriodeProgress(this.program_id, this.indikator_id); 
    });
    
  }


}
