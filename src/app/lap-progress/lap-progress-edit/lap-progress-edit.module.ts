import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapProgressEditComponent} from './lap-progress-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';

export const LapProgressEditRoutes: Routes = [{
    path: '',
    component: LapProgressEditComponent,
    data: {
        breadcrumb: "Detail Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapProgressEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        AngularMultiSelectModule
    ],
    declarations: [LapProgressEditComponent]
})

export class LapProgressEditModule {}
