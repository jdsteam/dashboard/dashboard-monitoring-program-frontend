import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-lap-progress-edit',
  templateUrl: './lap-progress-edit.component.html',
  styleUrls: [ './lap-progress-edit.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapProgressEditComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 
  
  private url_lapprogress = '/api/lapprogresses'; 
  private url_upload = '/api/upload'; 
  private url_kota = '/api/refkotas'; 
  private url_kecamatan = '/api/refkecamatans'; 
  private url_kelurahan = '/api/refkelurahans'; 
 
  dataLapProgress = []; 
  dataKota = [];  
  dataKecamatan = [];  
  dataKelurahan = [];  
  dataLokasi = [];  

  date_now = {};

  myForm: FormGroup;  
  lokasi: FormArray; 
  upload_gambar: FormArray; 
  upload_ppt: FormArray; 
  upload_pdf: FormArray; 
  upload_link: FormArray; 

  program_id: string = '0';
  indikator_id: string = '0';
  periode_id: string = '0';
  periodeprogress_id: string = '0';
  lapprogress_id: string = '0';
   
  data_upload_gambar = [];
  data_upload_ppt = [];
  data_upload_pdf = [];
  data_upload_link = [];
  selectedItemsKota = []; 
  selectedItemsKecamatan = []; 
  selectedItemsKelurahan = []; 
  dropdownSettingsKota = {};
  dropdownSettingsKecamatan = {};
  dropdownSettingsKelurahan = {};

  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-image.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapprogress','edit')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4]; 
    this.periode_id = '0'; 
    this.periodeprogress_id = routing[6];  
    this.lapprogress_id = routing[8];  
 
    this.getDataLapProgress(this.lapprogress_id);
    this.getDataKota();
    // this.getDataKecamatan();
    // this.getDataKelurahan();
    
    let today = new Date();
    this.date_now['year'] = today.getFullYear();
    this.date_now['month'] = today.getMonth()+1;
    this.date_now['day'] = today.getDate();
    // console.log('--------');
    // console.log(this.date_now)
    // console.log('--------');

    this.myForm = new FormGroup({ 
      id: new FormControl(), 
      program: new FormControl(), 
      indikator: new FormControl(), 
      periode: new FormControl(), 
      periodeprogress: new FormControl(), 
      // pelaksana: new FormControl({value: '', disabled: false}, [Validators.required]),  
      waktu_pelaporan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      kuantitas: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),  
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),  
      keterangan: new FormControl({value: '', disabled: false}, []),   
      lokasi: new FormArray([this.createLokasi()]),   
      upload_gambar: new FormArray([this.createUploadGambar()]),
      upload_ppt: new FormArray([this.createUploadPPT()]), 
      upload_link: new FormArray([this.createUploadLink()]),
      is_verified: new FormControl(), 
      is_deleted: new FormControl(), 
    });

    
    // this.addFieldValueUploadGambar();
    // this.addFieldValueUploadPPT();

    this.dropdownSettingsKota = {  
      "singleSelection": false, 
      "text": "-- Pilih Kota/Kab --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kota",
      "labelKey": "nama_kota",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
    };

    this.dropdownSettingsKecamatan = { 
      "singleSelection": false, 
      "text": "-- Pilih Kecamatan --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kecamatan",
      "labelKey": "nama_kecamatan",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
    };

    this.dropdownSettingsKelurahan = { 
      "singleSelection": false, 
      "text": "-- Pilih Kelurahan/Desa --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kelurahan",
      "labelKey": "nama_kelurahan",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
    };

    this.addFieldValueLokasi();
    this.addFieldValueUploadGambar();
    this.addFieldValueUploadPPT();

    this.lokasi.removeAt(0);
    this.upload_gambar.removeAt(0);
    this.upload_ppt.removeAt(0);
 
  }
 
  createLokasi(): FormGroup {
    return new FormGroup({
      kode_kota: new FormControl({value: '', disabled: false}, []), 
      kode_kecamatan: new FormControl({value: '', disabled: false}, []), 
      kode_kelurahan: new FormControl({value: '', disabled: false}, []), 
      nama_kota: new FormControl({value: '', disabled: false}, []), 
      nama_kecamatan: new FormControl({value: '', disabled: false}, []), 
      nama_kelurahan: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadGambar(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadPPT(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadPDF(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadLink(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }

  addFieldValueLokasi() {
    this.lokasi = this.myForm.get('lokasi') as FormArray;
    this.lokasi.push(this.createLokasi());
    return false; 
  }
  addFieldValueUploadGambar() {
    this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
    this.upload_gambar.push(this.createUploadGambar());
    return false;
  }
  addFieldValueUploadPPT() {
    this.upload_ppt = this.myForm.get('upload_ppt') as FormArray;
    this.upload_ppt.push(this.createUploadPPT());
    return false;
  }
  addFieldValueUploadPDF() {
    this.upload_pdf = this.myForm.get('upload_pdf') as FormArray;
    this.upload_pdf.push(this.createUploadPDF());
    return false;
  }
  addFieldValueUploadLink() {
    this.upload_link = this.myForm.get('upload_link') as FormArray;
    this.upload_link.push(this.createUploadLink());
    return false;
  }

  
  removeFieldValueLokasi(i) {
    if(this.lokasi){
      this.lokasi.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadGambar(i) {
    if(this.upload_gambar){
      this.upload_gambar.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadPPT(i) {
    if(this.upload_ppt){
      this.upload_ppt.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadPDF(i) {
    if(this.upload_pdf){
      this.upload_pdf.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadLink(i) {
    if(this.upload_link){
      this.upload_link.removeAt(i);
    }
    return false;
  }
 
 
  getDataKota() {

    const json_kota = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kota + '?_sort=nama_kota', json_kota).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataKota = result_msg; 
          this.loading = false;
          // console.log(this.dataKota);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKecamatan(kode_kota, i) {

    const json_kecamatan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kecamatan + '?kode_kota='+kode_kota+'&_sort=nama_kecamatan', json_kecamatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // this.dataKecamatan = result_msg;  
          if(this.dataKecamatan.length === 0){
            this.dataKecamatan = result_msg;
          }else{ 
            result_msg.forEach(element => {
              this.dataKecamatan.push(element)
            });
          } 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKelurahan(kode_kecamatan, i) {

    const json_kelurahan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kelurahan + '?kode_kecamatan='+kode_kecamatan+'&_sort=nama_kelurahan&_limit=6000', json_kelurahan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // this.dataKelurahan = result_msg; 
          if(this.dataKelurahan.length === 0){
            this.dataKelurahan = result_msg;
          }else{ 
            result_msg.forEach(element => {
              this.dataKelurahan.push(element)
            });
          }
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 

  getDataLapProgress(id) {

    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/' + id , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataLapProgress = result_msg;  
          this.periode_id = result_msg['periodeprogress']['periode'];
          this.setForm(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  
  setForm(data){ 
 
    console.log(data); 
    
    // for (let i = 0; i < data.lokasi.arr.length-1; i++) {
    //   this.addFieldValueLokasi();
    // }  
    for (let i = 0; i < data.upload_gambar.arr.length-1; i++) {
      this.addFieldValueUploadGambar();
    } 
    for (let i = 0; i < data.upload_ppt.arr.length-1; i++) {
      this.addFieldValueUploadPPT();
    } 
    for (let i = 0; i < data.upload_link.arr.length-1; i++) {
      this.addFieldValueUploadLink();
    } 

    
    // for (let i = 0; i < data.lokasi.arr.length; i++) {
    //   this.getDataKecamatan(data.lokasi.arr[i]['kode_kota'], i);
    //   this.getDataKelurahan(data.lokasi.arr[i]['kode_kecamatan'], i);
    //   this.getDataKecamatan(data.lokasi.arr[i]['kode_kota'], i);
    //   this.getDataKelurahan(data.lokasi.arr[i]['kode_kecamatan'], i);
    // } 
    for (let i = 0; i < data.lokasi.arr.length; i++) { 
      if(data.lokasi.arr[i]['kode_kelurahan']){
        this.selectedItemsKelurahan.push(data.lokasi.arr[i]);
        this.dataLokasi.push(data.lokasi.arr[i]);
      }else if(data.lokasi.arr[i]['kode_kecamatan']){
        this.selectedItemsKecamatan.push(data.lokasi.arr[i]);
        this.dataLokasi.push(data.lokasi.arr[i]);
      }else if(data.lokasi.arr[i]['kode_kota']){
        this.selectedItemsKota.push(data.lokasi.arr[i]);
        this.dataLokasi.push(data.lokasi.arr[i]);
      } 
    } 
    // console.log(this.dataLokasi)
    // console.log(this.selectedItemsKelurahan)
    // console.log(this.selectedItemsKecamatan)
    // console.log(this.selectedItemsKota)

    for (let i = 0; i < data.upload_gambar.arr.length; i++) {
      this.data_upload_gambar.push(data.upload_gambar.arr[i]['value']);
    } 
    for (let i = 0; i < data.upload_ppt.arr.length; i++) {
      // console.log(data.upload_ppt.arr[i])
      this.data_upload_ppt.push(data.upload_ppt.arr[i]['value']);
      this.data_upload_pdf.push(data.upload_ppt.arr[i]['name']);
    }  

    let date_temp = this.convert.formatDateYMD(data.waktu_pelaporan);
    // console.log(data.waktu_pelaporan);
    // console.log(date_temp);
    let date_temps = date_temp.split('-');
    let date_tempss = {year:parseInt(date_temps[0]), month: parseInt(date_temps[1]), day: parseInt(date_temps[2])};
    
    // console.log(data.waktu_pelaporan)
    // console.log(date_temp)
    // console.log(date_temps)
    // console.log(date_tempss)
    this.myForm.patchValue({
      id: data.id,
      // pelaksana: data.pelaksana,
      kuantitas: data.kuantitas, 
      persen: data.persen.toFixed(2),
      keterangan: data.keterangan,
      waktu_pelaporan: date_tempss,  
      // lokasi: data.lokasi.arr,  
      upload_gambar: data.upload_gambar.arr,  
      upload_ppt: data.upload_ppt.arr,  
      upload_link: data.upload_link.arr,  
      is_verified: data.is_verified,  
    });
  }

  checkPersen(value){ 
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataLapProgress['periodeprogress']['target']);
    let persen = (value / this.dataLapProgress['periodeprogress']['target']) * 100;
    // console.log(persen)
    this.myForm.patchValue({
      persen: persen.toFixed(2)
    });
  }

  
  onChangeKota(eventKota, i) {
    // console.log(eventKota);
    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kota: text,  
    // }); 
    // this.getDataKecamatan(event.target.value, i);

    let text = eventKota['nama_kota'];
    let value = eventKota['kode_kota']  
    
    this.dataLokasi.push({
        kode_provinsi: eventKota['kode_provinsi'],
        kode_kota: eventKota['kode_kota'],
        kode_kecamatan: '',
        kode_kelurahan: '',
        nama_provinsi: eventKota['nama_provinsi'],
        nama_kota: eventKota['nama_kota'],
        nama_kecamatan: '',
        nama_kelurahan: '',
      }
    ); 
    this.getDataKecamatan(value, i);
    console.log(this.dataLokasi);
  }

  onChangeKecamatan(eventKecamatan, i) { 
    // console.log(eventKecamatan);
    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kecamatan: text,  
    // }); 
    // this.getDataKelurahan(event.target.value, i);

    let text = eventKecamatan['nama_kecamatan']; 
    let value = eventKecamatan['kode_kecamatan'];   
    
    this.dataLokasi.push({
      kode_provinsi: eventKecamatan['kode_provinsi'],
      kode_kota: eventKecamatan['kode_kota'],
      kode_kecamatan: eventKecamatan['kode_kecamatan'],
      kode_kelurahan: '',
      nama_provinsi: eventKecamatan['nama_provinsi'],
      nama_kota: eventKecamatan['nama_kota'],
      nama_kecamatan: eventKecamatan['nama_kecamatan'],
      nama_kelurahan: '',
    }); 
    this.getDataKelurahan(value, i);
    console.log(this.dataLokasi);
  }

  onChangeKelurahan(eventKelurahan, i) {
    // console.log(eventKelurahan);
    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kelurahan: text,  
    // }); 

    let text = eventKelurahan['nama_kelurahan'];
    let value = eventKelurahan['kode_kelurahan'];   
    
    this.dataLokasi.push({
      kode_provinsi: eventKelurahan['kode_provinsi'],
      kode_kota: eventKelurahan['kode_kota'],
      kode_kecamatan: eventKelurahan['kode_kecamatan'],
      kode_kelurahan: eventKelurahan['kode_kelurahan'],
      nama_provinsi: eventKelurahan['nama_provinsi'],
      nama_kota: eventKelurahan['nama_kota'],
      nama_kecamatan: eventKelurahan['nama_kecamatan'],
      nama_kelurahan: eventKelurahan['nama_kelurahan'],
    }); 
    console.log(this.dataLokasi);
  }
  
  onUnchangeKota(eventKota, i) {
    // console.log(eventKota);
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKota['kode_provinsi'];
    dataSplice['kode_kota'] = eventKota['kode_kota'];
    dataSplice['kode_kecamatan'] = '';
    dataSplice['kode_kelurahan'] = '';
    dataSplice['nama_provinsi'] = eventKota['nama_provinsi'];
    dataSplice['nama_kota'] = eventKota['nama_kota'];
    dataSplice['nama_kecamatan'] = '';
    dataSplice['nama_kelurahan'] = ''; 

    for( var j = 0; j < this.dataLokasi.length; j++){  
      let ordered1 = {}; 
      ordered1 = dataSplice;  
      let ordered2 = {}; 
      // ordered2 = this.dataLokasi[j];  
      ordered2['kode_provinsi'] = this.dataLokasi[j]['kode_provinsi'];
      ordered2['kode_kota'] = this.dataLokasi[j]['kode_kota'];
      ordered2['kode_kecamatan'] = this.dataLokasi[j]['kode_kecamatan'];
      ordered2['kode_kelurahan'] = this.dataLokasi[j]['kode_kelurahan'];
      ordered2['nama_provinsi'] = this.dataLokasi[j]['nama_provinsi'];
      ordered2['nama_kota'] = this.dataLokasi[j]['nama_kota'];
      ordered2['nama_kecamatan'] = this.dataLokasi[j]['nama_kecamatan'];
      ordered2['nama_kelurahan'] = this.dataLokasi[j]['nama_kelurahan'];
      
      // console.log(j)
      // console.log(ordered1)
      // console.log(ordered2)
      
      if ( JSON.stringify(ordered2) === JSON.stringify(ordered1)) { 
        // console.log("true")
        this.dataLokasi.splice(j, 1); 
      }
    }
    console.log(this.dataLokasi);
  }

  onUnchangeKecamatan(eventKecamatan, i) {
    // console.log(eventKota);
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKecamatan['kode_provinsi'];
    dataSplice['kode_kota'] = eventKecamatan['kode_kota'];
    dataSplice['kode_kecamatan'] = eventKecamatan['kode_kecamatan'];
    dataSplice['kode_kelurahan'] = '';
    dataSplice['nama_provinsi'] = eventKecamatan['nama_provinsi'];
    dataSplice['nama_kota'] = eventKecamatan['nama_kota'];
    dataSplice['nama_kecamatan'] = eventKecamatan['nama_kecamatan'];
    dataSplice['nama_kelurahan'] = ''; 
 

    for( var j = 0; j < this.dataLokasi.length; j++){  
      let ordered1 = {}; 
      ordered1 = dataSplice;  
      let ordered2 = {}; 
      // ordered2 = this.dataLokasi[j];  
      ordered2['kode_provinsi'] = this.dataLokasi[j]['kode_provinsi'];
      ordered2['kode_kota'] = this.dataLokasi[j]['kode_kota'];
      ordered2['kode_kecamatan'] = this.dataLokasi[j]['kode_kecamatan'];
      ordered2['kode_kelurahan'] = this.dataLokasi[j]['kode_kelurahan'];
      ordered2['nama_provinsi'] = this.dataLokasi[j]['nama_provinsi'];
      ordered2['nama_kota'] = this.dataLokasi[j]['nama_kota'];
      ordered2['nama_kecamatan'] = this.dataLokasi[j]['nama_kecamatan'];
      ordered2['nama_kelurahan'] = this.dataLokasi[j]['nama_kelurahan'];

      // console.log(j)
      // console.log(ordered1)
      // console.log(ordered2)

      if ( JSON.stringify(ordered2) === JSON.stringify(ordered1)) { 
        // console.log("true") 
        this.dataLokasi.splice(j, 1); 
      }
      // this.dataLokasi.splice(this.dataLokasi.indexOf(dataSplice),1)
    }
    console.log(this.dataLokasi);
  }

  onUnchangeKelurahan(eventKelurahan, i) {
    // console.log(eventKota); 
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKelurahan['kode_provinsi'];
    dataSplice['kode_kota'] = eventKelurahan['kode_kota'];
    dataSplice['kode_kecamatan'] = eventKelurahan['kode_kecamatan'];
    dataSplice['kode_kelurahan'] = eventKelurahan['kode_kelurahan'];
    dataSplice['nama_provinsi'] = eventKelurahan['nama_provinsi'];
    dataSplice['nama_kota'] = eventKelurahan['nama_kota'];
    dataSplice['nama_kecamatan'] = eventKelurahan['nama_kecamatan'];
    dataSplice['nama_kelurahan'] = eventKelurahan['nama_kelurahan'];

    for( var j = 0; j < this.dataLokasi.length; j++){  
      let ordered1 = {}; 
      ordered1 = dataSplice;  
      let ordered2 = {}; 
      // ordered2 = this.dataLokasi[j];  
      ordered2['kode_provinsi'] = this.dataLokasi[j]['kode_provinsi'];
      ordered2['kode_kota'] = this.dataLokasi[j]['kode_kota'];
      ordered2['kode_kecamatan'] = this.dataLokasi[j]['kode_kecamatan'];
      ordered2['kode_kelurahan'] = this.dataLokasi[j]['kode_kelurahan'];
      ordered2['nama_provinsi'] = this.dataLokasi[j]['nama_provinsi'];
      ordered2['nama_kota'] = this.dataLokasi[j]['nama_kota'];
      ordered2['nama_kecamatan'] = this.dataLokasi[j]['nama_kecamatan'];
      ordered2['nama_kelurahan'] = this.dataLokasi[j]['nama_kelurahan'];

      // console.log(j)
      // console.log(ordered1)
      // console.log(ordered2)

      if ( JSON.stringify(ordered2) === JSON.stringify(ordered1)) { 
        // console.log("true")
        this.dataLokasi.splice(j, 1); 
      }
    }
    console.log(this.selectedItemsKelurahan)
    console.log(this.dataLokasi);
  }

  onUpload(event, type, i) {
    let fileList: FileList = event.target.files; 
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData(); 
      formData.append('files', file, file.name);
      // console.log(formData);
      // this.loading = true;

      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            // console.log(result_msg);

            if(type === 'img'){
              this.upload_gambar.at(i).patchValue({ 
                name: "/api" + result_msg[0]['url'], 
                value: "/api" + result_msg[0]['url'], 
              }); 
              this.data_upload_gambar.push("/api" + result_msg[0]['url']);
            }else if(type === 'ppt'){
              this.upload_ppt.at(i).patchValue({
                name: "/api" + result_msg[0]['url'], 
                value: "/api" + result_msg[0]['url'], 
              });
              this.data_upload_ppt.push("/api" + result_msg[0]['url']);
            }

            this.loading = false; 

          } catch (error) {
            this.loading = false;
            console.log(error)
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );

    }
  }


  onSubmit() {
    this.submitted = true;
    let val_lapprogress = this.myForm.value;  

    // val_lapprogress['lokasi'] = {
    //   'arr' : val_lapprogress['lokasi'] 
    // };
    val_lapprogress['lokasi'] = {
      'arr' : this.dataLokasi 
    };
    val_lapprogress['upload_gambar'] = {
      'arr' : val_lapprogress['upload_gambar'] 
    };
    val_lapprogress['upload_ppt'] = {
      'arr' : val_lapprogress['upload_ppt'] 
    };
    val_lapprogress['upload_link'] = {
      'arr' : val_lapprogress['upload_link'] 
    }; 
    // console.log(val_lapprogress['waktu_pelaporan']);
    val_lapprogress['waktu_pelaporan'] = val_lapprogress['waktu_pelaporan']['year'] + '-' + val_lapprogress['waktu_pelaporan']['month'] + '-' +  val_lapprogress['waktu_pelaporan']['day'];
  
    val_lapprogress['program'] = this.program_id;
    val_lapprogress['indikator'] = this.indikator_id;
    val_lapprogress['periode'] = this.periode_id;
    val_lapprogress['periodeprogress'] = this.periodeprogress_id;
    val_lapprogress['is_deleted'] = false;
    val_lapprogress['is_verified'] = false;
     
    console.log(val_lapprogress);
    console.log(val_lapprogress.value); 
     
    this.loading = true;
    let json_lapprogress = JSON.stringify(val_lapprogress); 
    this.httpRequest.httpPut(this.url_lapprogress + '/' + val_lapprogress['id'], json_lapprogress).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']);  

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Laporan Progress berhasil diupdate.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id])
    });
    
  }

}
