import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapProgressDetailComponent} from './lap-progress-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import 'hammerjs';
import 'mousetrap';
import { ModalGalleryModule } from 'angular-modal-gallery'; 

export const LapProgressDetailRoutes: Routes = [{
    path: '',
    component: LapProgressDetailComponent,
    data: {
        breadcrumb: "Detail Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapProgressDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ModalGalleryModule.forRoot(), 
    ],
    declarations: [LapProgressDetailComponent]
})

export class LapProgressDetailModule {}
