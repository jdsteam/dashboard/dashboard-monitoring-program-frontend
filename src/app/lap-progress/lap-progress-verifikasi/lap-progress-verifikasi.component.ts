import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import {
  AccessibilityConfig, Action, AdvancedLayout, ButtonEvent, ButtonsConfig, ButtonsStrategy, ButtonType, Description, DescriptionStrategy,
  DotsConfig, GridLayout, Image, ImageModalEvent, LineLayout, PlainGalleryConfig, PlainGalleryStrategy, PreviewConfig
} from 'angular-modal-gallery'; 


@Component({
  selector: 'app-lap-progress-verifikasi',
  templateUrl: './lap-progress-verifikasi.component.html',
  styleUrls: [ './lap-progress-verifikasi.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapProgressVerifikasiComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 

  private url_program = '/api/programs'; 
  private url_indikator = '/api/indikators'; 
  private url_periode = '/api/periodes'; 
  private url_periodeprogress = '/api/periodeprogresses'; 
  private url_lapprogress = '/api/lapprogresses';  

  dataProgram = [];
  dataIndikator = [];
  dataPeriode = []; 
  dataPeriodeProgress = []; 
  dataLapProgress = [];  


  pencapaianPeriodeKuantitas = 0;
  pencapaianIndikatorKuantitas = 0; 
  pencapaianPeriodePersen = 0;
  pencapaianIndikatorPersen = 0;

  myForm: FormGroup;   

  program_id: string = '0';
  indikator_id: string = '0';
  periode_id: string = '0';
  periodeprogress_id: string = '0';
  lapprogress_id: string = '0';
 

  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-image.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapprogress','verifikasi')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4]; 
    this.periode_id = '0'; 
    this.periodeprogress_id = routing[6];  
    this.lapprogress_id = routing[8];  
    this.countPencapaianPeriode(this.periodeprogress_id);
    this.countPencapaianIndikator(this.indikator_id);

    this.getDataLapProgress(this.lapprogress_id); 
    
    this.myForm = new FormGroup({ 
      id: new FormControl(),  
      kuantitas: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),  
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),     
      keterangan_verifikasi: new FormControl({value: '', disabled: false}, [Validators.required]),    
      is_verified: new FormControl(),  
    });

  }

   

  getDataLapProgress(id) {

    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/' + id , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);

          let arr_images: Image[] = [];
          let i = 0;

          try {
            result_msg.upload_gambar.arr.forEach(element => { 
              const newImage = new Image(i,  {img: element['value']}); 
              arr_images = [...arr_images, newImage];  
              i = i + 1;
            });
          } catch (error) {
            console.log(error)
          }  

          result_msg['upload_gallery'] = arr_images;
          this.dataLapProgress = result_msg;     
           
          this.setForm(result_msg);
          
          this.getDataPeriodeProgress(result_msg['periodeprogress']['id']); 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  
  getDataPeriodeProgress(id) {

    const json_periodeprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_periodeprogress + '/' + id , json_periodeprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataPeriodeProgress = result_msg; 
          this.getDataProgram(result_msg['program']['id']); 
          this.dataIndikator = result_msg['indikator'];   
          this.periode_id = result_msg['periode']['id']; 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_program+ '/' + id , json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataProgram = result_msg;   
          this.loading = false;
          this.finish_loading = true;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 
  countPencapaianPeriode(id){
    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/?periodeprogress=' + id + '&is_deleted=false' , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + element.kuantitas;
            target = element.periodeprogress.target;
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianIndikator(id){
    const json_lapprogress = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapprogress+ '/?indikator=' + id + '&is_deleted=false' , json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + element.kuantitas;
            target = element.indikator.target_kuantitatif;
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianIndikatorKuantitas = kuantitas;
          this.pencapaianIndikatorPersen = pencapaian;
          // console.log(kuantitas);
          // console.log(target);
          // console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  checkPersen(value){ 
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataLapProgress['periodeprogress']['target']);
    let persen = (value / this.dataLapProgress['periodeprogress']['target']) * 100;
    // console.log(persen)
    this.myForm.patchValue({
      persen: persen.toFixed(2)
    });
  }


  onSubmit() {
    this.submitted = true;
    let val_lapprogress = this.myForm.value;  
  
    val_lapprogress['is_verified'] = true;
     
    console.log(val_lapprogress);
    console.log(val_lapprogress.value); 
     
    this.loading = true;
    let json_lapprogress = JSON.stringify(val_lapprogress); 
    this.httpRequest.httpPut(this.url_lapprogress + '/' + val_lapprogress['id'], json_lapprogress).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']);  

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  setForm(data){ 
 
    console.log(data);   
    this.myForm.patchValue({
      id: data.id, 
      kuantitas: data.kuantitas, 
      persen: data.persen.toFixed(2),
      keterangan_verifikasi: data.keterangan_verifikasi, 
      is_verified: true,  
    });
  }

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Laporan Progress berhasil diverifikasi.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this._location.back();
    });
    
  }

}
