import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapProgressVerifikasiComponent} from './lap-progress-verifikasi.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import 'hammerjs';
import 'mousetrap';
import { ModalGalleryModule } from 'angular-modal-gallery'; 

export const LapProgressVerifikasiRoutes: Routes = [{
    path: '',
    component: LapProgressVerifikasiComponent,
    data: {
        breadcrumb: "Verifikasi Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapProgressVerifikasiRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ModalGalleryModule.forRoot(), 
    ],
    declarations: [LapProgressVerifikasiComponent]
})

export class LapProgressVerifikasiModule {}
