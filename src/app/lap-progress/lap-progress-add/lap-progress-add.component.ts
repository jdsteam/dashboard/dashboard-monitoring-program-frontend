import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray, AbstractControl, ValidatorFn, ValidationErrors} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { HttpEventType } from '@angular/common/http';


@Component({
  selector: 'app-lap-progress-add',
  templateUrl: './lap-progress-add.component.html',
  styleUrls: [ './lap-progress-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapProgressAddComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 
  
  private url_lapprogress = '/api/lapprogresses'; 
  private url_periodeprogres = '/api/periodeprogresses'; 
  private url_upload = '/api/upload'; 
  private url_kota = '/api/refkotas'; 
  private url_kecamatan = '/api/refkecamatans'; 
  private url_kelurahan = '/api/refkelurahans'; 
 
  dataLapProgress = []; 
  dataPeriodeProgress = []; 
  dataKota = [];  
  dataKecamatan = [];  
  dataKelurahan = [];  
  dataLokasi = [];  

  date_now = {};
  
  myForm: FormGroup;  
  lokasi: FormArray; 
  upload_gambar: FormArray; 
  upload_ppt: FormArray; 
  upload_pdf: FormArray; 
  upload_link: FormArray; 

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';
   
  data_upload_gambar = [];
  data_upload_ppt = [];
  data_upload_link = [];
  selectedItemsKota = []; 
  selectedItemsKecamatan = []; 
  selectedItemsKelurahan = []; 
  dropdownSettingsKota = {};
  dropdownSettingsKecamatan = {};
  dropdownSettingsKelurahan = {};

  data_upload_ppt_validation_size: string = 'berhasil';
  data_upload_ppt_progres: boolean = false;
  data_upload_ppt_progres_width: number = 0;

  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-image.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
  ) { }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapprogress','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.indikator_id = routing[4]; 
    this.periode_id = '0'; 
    this.periodeprogress_id = routing[6];  
 
    this.getDataKota();
    // this.getDataKecamatan();
    // this.getDataKelurahan();
 
    this.dataKota = [];
    this.dataKecamatan = [];
    this.dataKelurahan = [];
  
    let today = new Date();
    this.date_now['year'] = today.getFullYear();
    this.date_now['month'] = today.getMonth()+1;
    this.date_now['day'] = today.getDate();
    // console.log('--------');
    // console.log(this.date_now)
    // console.log('--------');

    this.myForm = new FormGroup({ 
      program: new FormControl(), 
      indikator: new FormControl(), 
      periode: new FormControl(), 
      periodeprogress: new FormControl(), 
      // pelaksana: new FormControl({value: '', disabled: false}, [Validators.required]),  
      waktu_pelaporan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      kuantitas: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),  
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),  
      keterangan: new FormControl({value: '', disabled: false}, []),   
      lokasi: new FormArray([this.createLokasi()]),   
      upload_gambar: new FormArray([this.createUploadGambar()]),
      upload_ppt: new FormArray([this.createUploadPPT()]), 
      upload_link: new FormArray([this.createUploadLink()]),
      is_verified: new FormControl(), 
      is_deleted: new FormControl(), 
    });

    this.dropdownSettingsKota = {  
      "singleSelection": false, 
      "text": "-- Pilih Kota/Kab --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kota",
      "labelKey": "nama_kota",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
    };

    this.dropdownSettingsKecamatan = { 
      "singleSelection": false, 
      "text": "-- Pilih Kecamatan --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kecamatan",
      "labelKey": "nama_kecamatan",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
      // "groupBy": "nama_kota"
    };

    this.dropdownSettingsKelurahan = { 
      "singleSelection": false, 
      "text": "-- Pilih Kelurahan/Desa --",
      "selectAllText":'Select All',
      "unSelectAllText":'UnSelect All',
      "primaryKey": "kode_kelurahan",
      "labelKey": "nama_kelurahan",
      "enableCheckAll": false, 
      "enableSearchFilter": true,
      "badgeShowLimit": 100,
      // "groupBy": "nama_kecamatan"
    };
  
    this.addFieldValueLokasi();
    this.addFieldValueUploadGambar();
    this.addFieldValueUploadPPT();

    this.lokasi.removeAt(0);
    this.upload_gambar.removeAt(0);
    this.upload_ppt.removeAt(0);

    this.getDataPeriodeProgress(this.periodeprogress_id);
 
  }
 
  createLokasi(): FormGroup {
    return new FormGroup({
      kode_kota: new FormControl({value: '', disabled: false}, []), 
      kode_kecamatan: new FormControl({value: '', disabled: false}, []), 
      kode_kelurahan: new FormControl({value: '', disabled: false}, []), 
      nama_kota: new FormControl({value: '', disabled: false}, []), 
      nama_kecamatan: new FormControl({value: '', disabled: false}, []), 
      nama_kelurahan: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadGambar(): FormGroup {
    return new FormGroup({
      name: new FormControl({value: '', disabled: false}, []), 
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadPPT(): FormGroup {
    return new FormGroup({
      name: new FormControl({value: '', disabled: false}), 
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }

  validateUploadSize(value: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } => {
      if (value == 'gagal') {
        return { 'uploadSize': true };
      }
      return null;
    };
  }

  createUploadPDF(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createUploadLink(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }

  addFieldValueLokasi() {
    this.lokasi = this.myForm.get('lokasi') as FormArray;
    this.lokasi.push(this.createLokasi());
    return false; 
  }
  addFieldValueUploadGambar() {
    this.upload_gambar = this.myForm.get('upload_gambar') as FormArray;
    this.upload_gambar.push(this.createUploadGambar());
    return false;
  }
  addFieldValueUploadPPT() {
    this.upload_ppt = this.myForm.get('upload_ppt') as FormArray;
    this.upload_ppt.push(this.createUploadPPT());
    return false;
  }
  addFieldValueUploadPDF() {
    this.upload_pdf = this.myForm.get('upload_pdf') as FormArray;
    this.upload_pdf.push(this.createUploadPDF());
    return false;
  }
  addFieldValueUploadLink() {
    this.upload_link = this.myForm.get('upload_link') as FormArray;
    this.upload_link.push(this.createUploadLink());
    return false;
  }

  removeFieldValueLokasi(i) {
    if(this.lokasi){
      this.lokasi.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadGambar(i) {
    if(this.upload_gambar){
      this.upload_gambar.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadPPT(i) {
    if(this.upload_ppt){
      this.upload_ppt.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadPDF(i) {
    if(this.upload_pdf){
      this.upload_pdf.removeAt(i);
    }
    return false;
  }
  removeFieldValueUploadLink(i) {
    if(this.upload_link){
      this.upload_link.removeAt(i);
    }
    return false;
  }
 

  getDataKota() {

    const json_kota = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kota + '?_sort=nama_kota', json_kota).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataKota = result_msg; 
          this.loading = false;
          // console.log(this.dataKota);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKecamatan(kode_kota, i) {

    const json_kecamatan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kecamatan + '?kode_kota='+kode_kota+'&_sort=nama_kecamatan', json_kecamatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body); 
          // this.dataKecamatan = result_msg;  
          if(this.dataKecamatan.length === 0){
            this.dataKecamatan = result_msg;
          }else{ 
            result_msg.forEach(element => {
              this.dataKecamatan.push(element)
            });
          } 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKelurahan(kode_kecamatan, i) {

    const json_kelurahan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kelurahan + '?kode_kecamatan='+kode_kecamatan+'&_sort=nama_kelurahan&_limit=6000', json_kelurahan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // this.dataKelurahan = result_msg; 
          if(this.dataKelurahan.length === 0){
            this.dataKelurahan = result_msg;
          }else{ 
            result_msg.forEach(element => {
              this.dataKelurahan.push(element)
            });
          }
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 
  
  getDataPeriodeProgress(id) {

    const json_periodeprogres = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_periodeprogres + '/' + id , json_periodeprogres).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataPeriodeProgress = result_msg; 
          this.periode_id = result_msg['periode']['id'];
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  checkPersen(value){ 
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataPeriodeProgress['target']);
    let persen = (value / this.dataPeriodeProgress['target']) * 100;
    console.log(persen)
    this.myForm.patchValue({
      persen: persen.toFixed(2)
    });
  }

  

  onChangeKota(eventKota, i) {
    // console.log(eventKota);

    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kota: text,  
    // }); 
    // this.getDataKecamatan(event.target.value, i);
    
    let text = eventKota['nama_kota'];
    let value = eventKota['kode_kota']  

    this.dataLokasi.push({
        kode_provinsi: eventKota['kode_provinsi'],
        kode_kota: eventKota['kode_kota'],
        kode_kecamatan: '',
        kode_kelurahan: '',
        nama_provinsi: eventKota['nama_provinsi'],
        nama_kota: eventKota['nama_kota'],
        nama_kecamatan: '',
        nama_kelurahan: '',
      }
    ); 
    this.getDataKecamatan(value, i);
    console.log(this.dataLokasi);
  }

  onChangeKecamatan(eventKecamatan, i) { 
    // console.log(eventKecamatan);

    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kecamatan: text,  
    // }); 
    // this.getDataKelurahan(event.target.value, i);
 
    let text = eventKecamatan['nama_kecamatan']; 
    let value = eventKecamatan['kode_kecamatan'];   
    
    this.dataLokasi.push({
      kode_provinsi: eventKecamatan['kode_provinsi'],
      kode_kota: eventKecamatan['kode_kota'],
      kode_kecamatan: eventKecamatan['kode_kecamatan'],
      kode_kelurahan: '',
      nama_provinsi: eventKecamatan['nama_provinsi'],
      nama_kota: eventKecamatan['nama_kota'],
      nama_kecamatan: eventKecamatan['nama_kecamatan'],
      nama_kelurahan: '',
    }); 
    this.getDataKelurahan(value, i);
    console.log(this.dataLokasi);
  }

  onChangeKelurahan(eventKelurahan, i) {
    // console.log(eventKelurahan);

    // let text = event.target['options'][event.target['options'].selectedIndex].text; 
    // let value = event.target.value;  
    // console.log(text);
    // console.log(value);
    // this.lokasi.at(i).patchValue({ 
    //   nama_kelurahan: text,  
    // }); 

    let text = eventKelurahan['nama_kelurahan'];
    let value = eventKelurahan['kode_kelurahan'];   
    
    this.dataLokasi.push({
      kode_provinsi: eventKelurahan['kode_provinsi'],
      kode_kota: eventKelurahan['kode_kota'],
      kode_kecamatan: eventKelurahan['kode_kecamatan'],
      kode_kelurahan: eventKelurahan['kode_kelurahan'],
      nama_provinsi: eventKelurahan['nama_provinsi'],
      nama_kota: eventKelurahan['nama_kota'],
      nama_kecamatan: eventKelurahan['nama_kecamatan'],
      nama_kelurahan: eventKelurahan['nama_kelurahan'],
    }); 
    console.log(this.dataLokasi);
  }

  onUnchangeKota(eventKota, i) {
    // console.log(eventKota);
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKota['kode_provinsi'];
    dataSplice['kode_kota'] = eventKota['kode_kota'];
    dataSplice['kode_kecamatan'] = '';
    dataSplice['kode_kelurahan'] = '';
    dataSplice['nama_provinsi'] = eventKota['nama_provinsi'];
    dataSplice['nama_kota'] = eventKota['nama_kota'];
    dataSplice['nama_kecamatan'] = '';
    dataSplice['nama_kelurahan'] = ''; 
    for( var j = 0; j < this.dataLokasi.length; j++){  
      if ( JSON.stringify(this.dataLokasi[j]) === JSON.stringify(dataSplice)) { 
        this.dataLokasi.splice(j, 1); 
      }
    }
    console.log(this.dataLokasi);
  }

  onUnchangeKecamatan(eventKecamatan, i) {
    // console.log(eventKecamatan);
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKecamatan['kode_provinsi'];
    dataSplice['kode_kota'] = eventKecamatan['kode_kota'];
    dataSplice['kode_kecamatan'] = eventKecamatan['kode_kecamatan'];
    dataSplice['kode_kelurahan'] = '';
    dataSplice['nama_provinsi'] = eventKecamatan['nama_provinsi'];
    dataSplice['nama_kota'] = eventKecamatan['nama_kota'];
    dataSplice['nama_kecamatan'] = eventKecamatan['nama_kecamatan'];
    dataSplice['nama_kelurahan'] = ''; 
    for( var j = 0; j < this.dataLokasi.length; j++){  
      if ( JSON.stringify(this.dataLokasi[j]) === JSON.stringify(dataSplice)) { 
        this.dataLokasi.splice(j, 1); 
      }
    }
    console.log(this.dataLokasi);
  }

  onUnchangeKelurahan(eventKelurahan, i) {
    // console.log(eventKelurahan);
    let dataSplice = {};
    dataSplice['kode_provinsi'] = eventKelurahan['kode_provinsi'];
    dataSplice['kode_kota'] = eventKelurahan['kode_kota'];
    dataSplice['kode_kecamatan'] = eventKelurahan['kode_kecamatan'];
    dataSplice['kode_kelurahan'] = eventKelurahan['kode_kelurahan'];
    dataSplice['nama_provinsi'] = eventKelurahan['nama_provinsi'];
    dataSplice['nama_kota'] = eventKelurahan['nama_kota'];
    dataSplice['nama_kecamatan'] = eventKelurahan['nama_kecamatan'];
    dataSplice['nama_kelurahan'] = eventKelurahan['nama_kelurahan'];
    for( var j = 0; j < this.dataLokasi.length; j++){   
      if ( JSON.stringify(this.dataLokasi[j]) === JSON.stringify(dataSplice)) {  
        this.dataLokasi.splice(j, 1); 
      }
    }
    console.log(this.dataLokasi);
  }

  onUploadValidation(event, type, i) {
    // Validation Size
    let name = event.target.files[0].name;
    let size = event.target.files[0].size;
    if(parseInt(size) < 20971520){
      this.data_upload_ppt_validation_size = 'berhasil';
      this.upload_ppt.at(i).get('name').clearValidators();
      this.upload_ppt.at(i).get('name').updateValueAndValidity();
      this.onUpload(event, type, i);
    } else {
      console.log(size);
      this.data_upload_ppt_validation_size = 'gagal';
      this.upload_ppt.at(i).get('name').setValidators([this.validateUploadSize(this.data_upload_ppt_validation_size).bind(this)]);
      this.upload_ppt.at(i).get('name').updateValueAndValidity();
    }
  }

  onUpload(event, type, i) {
    let fileList: FileList = event.target.files; 
    // console.log(fileList);

    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData(); 
      formData.append('files', file, file.name);
      // console.log(formData);
      // this.loading = true;

      this.httpRequest.httpUploadClient(this.url_upload, formData).subscribe(
        result => {

          if (result.type === HttpEventType.UploadProgress) {
            this.data_upload_ppt_progres = true;
            this.data_upload_ppt_progres_width = Math.round(100 * result.loaded / result.total);
          }
          else if (result.type === HttpEventType.Response) {

            this.data_upload_ppt_progres = false;
            this.data_upload_ppt_progres_width = 0;

            try {
              const result_msg = result.body;

              if(type === 'img'){
                this.upload_gambar.at(i).patchValue({ 
                  // name: result_msg[0]['name'], 
                  value: "/api" + result_msg[0]['url'], 
                }); 
                this.data_upload_gambar.push("/api" + result_msg[0]['url']);
              }else if(type === 'ppt'){
                this.upload_ppt.at(i).patchValue({
                  // name: result_msg[0]['name'], 
                  value: "/api" + result_msg[0]['url'], 
                });
                this.data_upload_ppt.push("/api" + result_msg[0]['url']);
              }

              // this.loading = false; 

            } catch (error) {
              // this.loading = false;
              console.log(error)
              this.errorMessage.openErrorSwal('Something wrong.');
            }

          }
        },
        error => {
          console.log(error);
          // this.loading = false;
        }
      );

    }
  }


  onSubmit() {
    this.submitted = true;
    let val_lapprogress = this.myForm.value;  

    // val_lapprogress['lokasi'] = {
    //   'arr' : val_lapprogress['lokasi'] 
    // };
    val_lapprogress['lokasi'] = {
      'arr' : this.dataLokasi 
    };
    val_lapprogress['upload_gambar'] = {
      'arr' : val_lapprogress['upload_gambar'] 
    };
    val_lapprogress['upload_ppt'] = {
      'arr' : val_lapprogress['upload_ppt'] 
    };
    val_lapprogress['upload_link'] = {
      'arr' : val_lapprogress['upload_link'] 
    }; 
    console.log(val_lapprogress['waktu_pelaporan']);
    val_lapprogress['waktu_pelaporan'] = val_lapprogress['waktu_pelaporan']['year'] + '-' + val_lapprogress['waktu_pelaporan']['month'] + '-' +  val_lapprogress['waktu_pelaporan']['day'];
  
    val_lapprogress['program'] = this.program_id;
    val_lapprogress['indikator'] = this.indikator_id;
    val_lapprogress['periode'] = this.periode_id;
    val_lapprogress['periodeprogress'] = this.periodeprogress_id;
    val_lapprogress['is_deleted'] = false;
    val_lapprogress['is_verified'] = false;
     
    console.log(val_lapprogress);
    console.log(val_lapprogress.value); 
     
    this.loading = true;
    let json_lapprogress = JSON.stringify(val_lapprogress); 
    this.httpRequest.httpPost(this.url_lapprogress, json_lapprogress).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Laporan Progress berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      this.router.navigate(['/program/', this.program_id, 'indikator', this.indikator_id, 'periode', this.periodeprogress_id])
    });
    
  }

}
