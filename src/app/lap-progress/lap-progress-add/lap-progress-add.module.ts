import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapProgressAddComponent} from './lap-progress-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { AngularMultiSelectModule } from 'angular4-multiselect-dropdown/angular4-multiselect-dropdown';

export const LapProgressAddRoutes: Routes = [{
    path: '',
    component: LapProgressAddComponent,
    data: {
        breadcrumb: "Detail Periode Progress"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapProgressAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        AngularMultiSelectModule
    ],
    declarations: [LapProgressAddComponent]
})

export class LapProgressAddModule {}
