import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';

import swal from 'sweetalert2';
import { IOption } from "ng-select";


@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: [ './notification-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class NotificationDetailComponent implements OnInit {

  public loading = false;
  private url_notification = '/api/notifications';
  dataNotification = [];

  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
  ) {
    this._constant.changeMessage();
  }

  ngOnInit() { 
    if(!this.session.checkAccess('notification','detail')){
      this.router.navigate(['/error/403']);
    }

    this.route.params.subscribe(params => {
      let id = params['id']; 
      this.getDataNotification(id);
    });
  }

  getDataNotification(id) {
    const json_notification = {
      is_read: true
    };
    this.loading = true;
    this.httpRequest.httpPut(this.url_notification + '/'+ id, json_notification).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataNotification = result_msg;
          console.log(this.dataNotification);
          this.loading = false;
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showList(){
    this.router.navigate(['/notification/list'])
  }
  
  showDetail(row){
    this.router.navigate(['/notification/detail', row.id])
  }
  
  showBack(){
    this._location.back();
  }
 


}
