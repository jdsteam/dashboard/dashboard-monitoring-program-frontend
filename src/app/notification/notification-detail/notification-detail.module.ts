import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {NotificationDetailComponent} from './notification-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const NotificationDetailRoutes: Routes = [{
    path: '',
    component: NotificationDetailComponent,
    data: {
        breadcrumb: "Detail Notifikasi"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(NotificationDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [NotificationDetailComponent]
})

export class NotificationDetailModule {}
