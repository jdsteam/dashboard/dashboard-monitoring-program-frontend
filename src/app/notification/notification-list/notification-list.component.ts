import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import swal from 'sweetalert2';


@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: [ './notification-list.component.css',],
  animations: [fadeInOutTranslate]
})

export class NotificationListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_notification = '/api/notifications'; 
  idUser: any;
  dataNotification: [];

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 
  
  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _location: Location
  ) {
    this._constant.changeMessage();
  }

  ngOnInit() {
    if(!this.session.checkAccess('notification','list')){
      console.log('masuk');
      // this.router.navigate(['/error/403']);
    }

    const sess = JSON.parse(this.session.getData());
    this.idUser = sess['id'];

    this.getData();
 
    this.is_list = this.session.checkAccess('notification','list');
    this.is_detail = this.session.checkAccess('notification','detail');
    this.is_add = this.session.checkAccess('notification','add');
    this.is_edit = this.session.checkAccess('notification','edit');
    this.is_delete = this.session.checkAccess('notification','delete'); 
  }

  getData() {
    const filter = '?penerima=' + this.idUser + '&_sort=created_at:DESC';
    const json_notification = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_notification + filter, json_notification).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataNotification = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      });
  }

  showList(){
    this.router.navigate(['/notification/list'])
  }

  showDetail(row){
    this.router.navigate(['/notification/detail', row.id])
  }
  
  showBack(){
    this._location.back();
  }

}
