import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NotificationListComponent } from './notification-list.component';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const NotificationListRoutes: Routes = [{
    path: '',
    component: NotificationListComponent,
    data: {
        breadcrumb: "List Notifikasi",
        status: true
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(NotificationListRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [NotificationListComponent]
})

export class NotificationListModule {}
