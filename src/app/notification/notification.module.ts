import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationRoutes } from './notification.routing';
// import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(NotificationRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    // NotificationDetailComponent
  ]
})

export class NotificationModule {}
