import { Component } from '@angular/core';

import { GlobalsService } from '../../shared/service/globals.service';
import { ConverterService } from '../../shared/service/converter.service';

@Component({
  selector: 'app-notification-message',
  templateUrl: './notification-message.component.html',
  styleUrls: ['./notification-message.component.css'],
})
export class NotificationMessageComponent {

  jumlahMessage: number;
  dataMessage = [];

  constructor(
    private _constant: GlobalsService,
    private convert: ConverterService,
  ) { }

  ngOnInit() {
    this._constant.currentMessage.subscribe(
      message => {
        this.jumlahMessage = message[0].jumlah
        this.dataMessage = message[0].data
      })
  }

}
