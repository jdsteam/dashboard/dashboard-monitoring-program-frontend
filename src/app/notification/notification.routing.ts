import {Routes} from '@angular/router';
// import { NotificationDetailComponent } from './notification-detail/notification-detail.component';

export const NotificationRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Notifikasi',
      status: true
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './notification-list/notification-list.module#NotificationListModule'
      },
      {
        path: 'detail/:id',
        // component: NotificationDetailComponent
        loadChildren: './notification-detail/notification-detail.module#NotificationDetailModule'
      },
    ]
  }
];


