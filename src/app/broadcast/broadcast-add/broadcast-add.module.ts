import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {BroadcastAddComponent} from './broadcast-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const BroadcastAddRoutes: Routes = [{
    path: '',
    component: BroadcastAddComponent,
    data: {
        breadcrumb: "Tambah Pesan Broadcast"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BroadcastAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [BroadcastAddComponent]
})

export class BroadcastAddModule {}
