import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';

import swal from 'sweetalert2';
import { IOption } from "ng-select";


@Component({
  selector: 'app-broadcast-add',
  templateUrl: './broadcast-add.component.html',
  styleUrls: [ './broadcast-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class BroadcastAddComponent implements OnInit {

  public loading = false; 
  private url_broadcast = '/api/broadcasts'; 
  private url_notification = '/api/notifications'; 
  private url_user = '/api/users'; 
  disableTextbox:Boolean = true;
  idUser: any;
  dataUser = [];
  dataBroadcast = [];
  dataNotification = [];
  dataRole = [];
  
  myForm: FormGroup;
  submitted: boolean;
  submittedDraft: boolean;
  submittedSend: boolean;

  penerimaOption: Array<IOption> = [
    {value: 'semua', label: 'Semua'},
    {value: '9', label: 'OPD'},
    {value: '5', label: 'Pelapor'},
    {value: '7', label: 'Pemantau'},
    {value: '6', label: 'Verifikator'}
  ];
   
  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
  ) {
    this._constant.changeMessage();
  }

  ngOnInit() {
    if(!this.session.checkAccess('broadcast','add')){
      this.router.navigate(['/error/403']);
    }

    const sess = JSON.parse(this.session.getData());
    this.idUser = sess['id'];

    this.myForm = new FormGroup({ 
      judul: new FormControl({value: '', disabled: false}, [Validators.required]), 
      penerima: new FormControl({value: '', disabled: false}, [Validators.required]), 
      pesan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      tanggal_buat: new FormControl(), 
      tanggal_kirim: new FormControl(), 
      is_draft: new FormControl(), 
      is_deleted: new FormControl(), 
    });
  }

  onDraft(): void {
    this.submittedDraft = true;
    this.submittedSend = false;
  }

  onSend(): void {
    this.submittedDraft = false;
    this.submittedSend = true;
  }

  onSubmit() {
    this.submitted = true;
    let val_broadcast = this.myForm.value;
    
    this.myForm.patchValue({ 
      tanggal_buat: new Date(), 
      tanggal_kirim: null, 
      is_draft: true, 
      is_deleted: false, 
    });

    // Jika Kirim
    if(!this.submittedDraft && this.submittedSend) {
      this.myForm.controls['tanggal_kirim'].patchValue(new Date());
      this.myForm.controls['is_draft'].patchValue(false);
    }

    val_broadcast = this.myForm.value; 
    let json_broadcast = JSON.stringify(val_broadcast);

    this.loading = true;
    this.httpRequest.httpPost(this.url_broadcast, json_broadcast).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);

          // Jika Kirim
          if(!this.submittedDraft && this.submittedSend) {
            const json_user = {};
            // Get All User by Role
            let filter = (result_msg.penerima === 'semua') ? '' : '&role=' + result_msg.penerima;
            this.httpRequest.httpGet(this.url_user + '?is_deleted=false' + filter, json_user).subscribe(
              result_user => {
                try {
                  const result_msg_user = JSON.parse(result_user._body);

                  result_msg_user.forEach((user) => {
                    // Post Notification
                    const json_notification = {
                      'type': 'broadcast', 
                      'type_id': result_msg.id, 
                      'pengirim': this.idUser, 
                      'penerima': user.id, 
                      'judul': result_msg.judul, 
                      'text': result_msg.pesan, 
                      'is_read': false, 
                    };
                    this.httpRequest.httpPost(this.url_notification, json_notification).subscribe(
                      result_notification => {
                        try {
                          const result_msg_notification = JSON.parse(result_notification._body);
                          this.dataNotification.push(result_msg_notification);
                        } catch (error) {
                          this.loading = false;
                          this.errorMessage.openErrorSwal('Something wrong.');
                        }
                      }, 
                      error => {
                        console.log(error);
                        this.loading = false;
                      })
                  });

                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                }
              },
              error => {
                console.log(error);
                this.loading = false;
              })
          }

          this.loading = false;
          this.showSuccess(result_msg['judul']);
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  showList(){
    this.router.navigate(['/broadcast/list'])
  }

  showAdd(){
    this.router.navigate(['/broadcast/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/broadcast/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/broadcast/edit', row.id])
  }

  
  showBack(){
    this._location.back();
  }

  showSuccess(nama){
    swal({
      title: 'Informasi',
      text: nama + ' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/broadcast/list'])
    });
    
  }



}
