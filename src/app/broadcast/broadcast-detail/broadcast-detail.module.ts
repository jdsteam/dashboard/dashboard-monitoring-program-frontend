import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {BroadcastDetailComponent} from './broadcast-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';

export const BroadcastDetailRoutes: Routes = [{
    path: '',
    component: BroadcastDetailComponent,
    data: {
        breadcrumb: "Detail Pesan Broadcast"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BroadcastDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [BroadcastDetailComponent]
})

export class BroadcastDetailModule {}
