import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';

import swal from 'sweetalert2';
import { IOption } from "ng-select";


@Component({
  selector: 'app-broadcast-detail',
  templateUrl: './broadcast-detail.component.html',
  styleUrls: [ './broadcast-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class BroadcastDetailComponent implements OnInit {

  public loading = false;
  private url_broadcast = '/api/broadcasts';
  private url_role = '/api/users-permissions/roles';
  disableTextbox:Boolean = true;
  dataBroadcast = [];
  dataRole = [];
  
  myForm: FormGroup;
  submitted: boolean;

  penerimaOption: Array<IOption> = [
    {value: '9', label: 'Dinas'},
    {value: '5', label: 'Pelapor'},
    {value: '7', label: 'Pemantau'},
    {value: '6', label: 'Verifikator'}
  ];

  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location
  ) {
    this._constant.changeMessage();
  }

  ngOnInit() { 
    if(!this.session.checkAccess('broadcast','detail')){
      this.router.navigate(['/error/403']);
    }
    let id = this.route.snapshot.paramMap.get('id'); 
    this.getDataBroadcast(id);

    this.myForm = new FormGroup({
      id: new FormControl({value: '', disabled: true}, [Validators.required]), 
      judul: new FormControl({value: '', disabled: false}, [Validators.required]), 
      penerima: new FormControl({value: '', disabled: false}, [Validators.required]), 
      pesan: new FormControl({value: '', disabled: false}, [Validators.required]),  
    });
  }

  getDataBroadcast(id) {

    const json_broadcast = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_broadcast + '/'+ id, json_broadcast).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataBroadcast = result_msg;

          const json_role = {};
          if(result_msg.penerima === 'semua') {
            this.dataRole['name'] = 'Semua';
            this.loading = false;
          } else {
            this.httpRequest.httpGet(this.url_role + '/'+ result_msg.penerima, json_role).subscribe(
              result_role => {
                try {
                  const result_msg_role = JSON.parse(result_role._body);
                  this.dataRole = result_msg_role.role;
                  // console.log(result_msg_role.role);
                  this.loading = false;
                } catch (error) {
                  this.loading = false;
                  this.errorMessage.openErrorSwal('Something wrong.');
                  console.log(error);
                  this.dataBroadcast = null;
                  this.dataRole = null;
                }
              },
              error => {
                console.log(error);
                this.dataBroadcast = null;
                this.dataRole = null;
                this.loading = false;
              })
          }

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.dataBroadcast = null;
          this.dataRole = null;
        }
      },
      error => {
        console.log(error);
        this.dataBroadcast = null;
        this.dataRole = null;
        this.loading = false;
      }
    );
  }

  // setForm(data){ 
  //   this.myForm.patchValue({
  //     id: data.id,
  //     judul: data.judul,
  //     penerima: data.penerima,
  //     pesan: data.pesan,
  //   });
  // }

  showList(){
    this.router.navigate(['/broadcast/list'])
  }

  showAdd(){
    this.router.navigate(['/broadcast/add'])
  }
  
  showDetail(row){
    // console.log(row);
    this.router.navigate(['/broadcast/detail', row.id])
  }
 
  showEdit(row){
    // console.log(row);
    this.router.navigate(['/broadcast/edit', row.id])
  }
  
  showBack(){
    this._location.back();
  }
 


}
