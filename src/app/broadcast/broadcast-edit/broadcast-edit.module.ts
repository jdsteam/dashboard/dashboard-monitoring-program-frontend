import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {BroadcastEditComponent} from './broadcast-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { NgxLoadingModule } from 'ngx-loading';

export const BroadcastEditRoutes: Routes = [{
    path: '',
    component: BroadcastEditComponent,
    data: {
        breadcrumb: "Update Pesan Broadcast"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BroadcastEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
        ShowHidePasswordModule
    ],
    declarations: [BroadcastEditComponent]
})

export class BroadcastEditModule {}
