import {Routes} from '@angular/router';

export const BroadcastRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Pesan Broadcast',
      status: true
    },
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        loadChildren: './broadcast-list/broadcast-list.module#BroadcastListModule'
      },
      {
        path: 'add',
        loadChildren: './broadcast-add/broadcast-add.module#BroadcastAddModule'
      },
      {
        path: 'detail/:id',
        loadChildren: './broadcast-detail/broadcast-detail.module#BroadcastDetailModule'
      },
      {
        path: 'edit/:id',
        loadChildren: './broadcast-edit/broadcast-edit.module#BroadcastEditModule'
      },
    ]
  }
];


