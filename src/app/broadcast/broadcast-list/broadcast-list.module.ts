import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {BroadcastListComponent} from './broadcast-list.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const BroadcastListRoutes: Routes = [{
    path: '',
    component: BroadcastListComponent,
    data: {
        breadcrumb: "List Pesan Broadcast",
        status: true
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(BroadcastListRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [BroadcastListComponent]
})

export class BroadcastListModule {}
