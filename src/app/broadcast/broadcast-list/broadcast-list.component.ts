import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { GlobalsService } from '../../shared/service/globals.service'
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import swal from 'sweetalert2';


@Component({
  selector: 'app-broadcast-list',
  templateUrl: './broadcast-list.component.html',
  styleUrls: [ './broadcast-list.component.css',],
  animations: [fadeInOutTranslate]
})

export class BroadcastListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_broadcast = '/api/broadcasts';
  private url_notification = '/api/notifications'; 
  private url_user = '/api/users'; 
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;
  idUser: any;

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false; 

  dataMessage: any;
  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }
  
  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];
  
  constructor(
    private _constant: GlobalsService,
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _location: Location
  ) {
    this._constant.changeMessage();
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() {
    if(!this.session.checkAccess('broadcast','list')){
      this.router.navigate(['/error/403']);
    }

    const sess = JSON.parse(this.session.getData());
    this.idUser = sess['id'];
 
    this.is_list = this.session.checkAccess('broadcast','list');
    this.is_detail = this.session.checkAccess('broadcast','detail');
    this.is_add = this.session.checkAccess('broadcast','add');
    this.is_edit = this.session.checkAccess('broadcast','edit');
    this.is_delete = this.session.checkAccess('broadcast','delete'); 
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        // TODO[Dmitry Teplov] test with server-side paging.
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  getData(cb) {

    const json_broadcast = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_broadcast + '?is_deleted=false&_sort=tanggal_buat:DESC', json_broadcast).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.judul.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  onDelete(row) {
 
    // let json_broadcast = row;
    let json_broadcast = {};
    json_broadcast['is_deleted'] = true;
    // console.log(json_broadcast);

    this.loading = true;
    this.httpRequest.httpPut(this.url_broadcast + '/' + row['id'], json_broadcast).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          // console.log(result_msg);

          this.loading = false;
          this.showSuccess(row['judul'], 'hapus');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSend(row) {
    let json_broadcast = {};
    json_broadcast['tanggal_kirim'] = new Date();
    json_broadcast['is_draft'] = false;

    this.loading = true;
    this.httpRequest.httpPut(this.url_broadcast + '/' + row['id'], json_broadcast).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);

          // Kirim
          const json_user = {};
          // Get All User by Role
          let filter = (result_msg.penerima === 'semua') ? '' : '&role=' + result_msg.penerima;
          this.httpRequest.httpGet(this.url_user + '?is_deleted=false' + filter, json_user).subscribe(
            result_user => {
              try {
                const result_msg_user = JSON.parse(result_user._body);

                result_msg_user.forEach((user) => {
                  // Post Notification
                  const json_notification = {
                    'type': 'broadcast', 
                    'type_id': result_msg.id, 
                    'pengirim': this.idUser, 
                    'penerima': user.id, 
                    'judul': result_msg.judul, 
                    'text': result_msg.pesan, 
                    'is_read': false, 
                  };
                  this.httpRequest.httpPost(this.url_notification, json_notification).subscribe(
                    result_notification => {
                      try {
                        const result_msg_notification = JSON.parse(result_notification._body);
                      } catch (error) {
                        this.loading = false;
                        this.errorMessage.openErrorSwal('Something wrong.');
                      }
                    }, 
                    error => {
                      console.log(error);
                      this.loading = false;
                    })
                });

              } catch (error) {
                this.loading = false;
                this.errorMessage.openErrorSwal('Something wrong.');
              }
            },
            error => {
              console.log(error);
              this.loading = false;
            })

          this.loading = false;
          this.showSuccess(row['judul'], 'kirim');

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showList(){
    this.router.navigate(['/broadcast/list'])
  }

  showAdd(){
    this.router.navigate(['/broadcast/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/broadcast/detail', row.id])
  }

  showEdit(row){
    // console.log(row);
    this.router.navigate(['/broadcast/edit', row.id])
  }

  
  showBack(){
    this._location.back();
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }

  showSend(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan mengirim data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onSend(row);
      }
    });
  }
  
  showSuccess(nama, type){
    swal({
      title: 'Informasi',
      html: 'Pesan Broadcast ' + nama + ' berhasil di' + type + '.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.rowsFilter = [];
      this.tempFilter = [];
      this.getData((data) => {
        this.tempFilter = [...data];
        this.rowsFilter = data;
      });
    });
    
  }


}
