import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BroadcastRoutes } from './broadcast.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(BroadcastRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class BroadcastModule {}
