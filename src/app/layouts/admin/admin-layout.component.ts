import {Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit} from '@angular/core';
import 'rxjs/add/operator/filter';
import {state, style, transition, animate, trigger, AUTO_STYLE} from '@angular/animations';

import { AccessRole } from '../../shared/menu-items/access-role';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { MenuItemsAdmin } from '../../shared/menu-items/menu-items-admin';
import { MenuItemsDinas } from '../../shared/menu-items/menu-items-dinas';
import { MenuItemsPelapor } from '../../shared/menu-items/menu-items-pelapor';
import { MenuItemsVerifikator } from '../../shared/menu-items/menu-items-verifikator';
import { MenuItemsPemantau } from '../../shared/menu-items/menu-items-pemantau';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';

export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('slideInOut', [
      state('in', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('out', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('in => out', animate('400ms ease-in-out')),
      transition('out => in', animate('400ms ease-in-out'))
    ]),
    trigger('slideOnOff', [
      state('on', style({
        transform: 'translate3d(0, 0, 0)'
      })),
      state('off', style({
        transform: 'translate3d(100%, 0, 0)'
      })),
      transition('on => off', animate('400ms ease-in-out')),
      transition('off => on', animate('400ms ease-in-out'))
    ]),
    trigger('mobileMenuTop', [
        state('no-block, void',
            style({
                overflow: 'hidden',
                height: '0px',
            })
        ),
        state('yes-block',
            style({
                height: AUTO_STYLE,
            })
        ),
        transition('no-block <=> yes-block', [
            animate('400ms ease-in-out')
        ])
    ])
  ]
})

export class AdminLayoutComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'collapsed';
  verticalEffect = 'shrink';
  chatToggle = 'out';
  chatInnerToggle = 'off';
  innerHeight: string;
  isScrolled = false;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;

  public loading = false;
  private url_notification = '/api/notifications'; 

  jumlahMessage: number;
  jumlahNotification: number;
  dataMessage = [];
  dataNotification = [];
  idUser: string;
  username: string;
  role: string;

  @ViewChild('searchFriends') search_friends: ElementRef;
  @ViewChild('toggleButton') toggle_button: ElementRef;
  @ViewChild('sideMenu') side_menu: ElementRef;
  constructor(
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    public accessRole: AccessRole,
    public menuItems: MenuItems,
    public menuItemsAdmin: MenuItemsAdmin,
    public menuItemsDinas: MenuItemsDinas,
    public menuItemsPelapor: MenuItemsPelapor,
    public menuItemsVerifikator: MenuItemsVerifikator,
    public menuItemsPemantau: MenuItemsPemantau,
    ) 
  {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  ngOnInit() {
    const sess = JSON.parse(this.session.getData());
    this.idUser = sess['id'];
    this.username = sess['username'];
    this.role = this.session.getRole();

    this.getJumlahMessage();

    this.getJumlahNotification();
    this.getDataNotification();
  }

  getJumlahMessage() {
    const filter = '?type=broadcast&penerima=' + this.idUser + '&is_read=false';
    const json_message = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_notification + '/count' + filter, json_message).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.jumlahMessage = result_msg;
        } catch (error) {
          this.jumlahMessage = 0;
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.jumlahMessage = 0;
        this.loading = false;
      });
  }

  getJumlahNotification() {
    const filter = '?penerima=' + this.idUser + '&is_read=false';
    const json_notification = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_notification + '/count' + filter, json_notification).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.jumlahNotification = result_msg;
        } catch (error) {
          this.jumlahNotification = 0;
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.jumlahNotification = 0;
        this.loading = false;
      });
  }

  getDataNotification() {
    const filter = '?penerima=' + this.idUser + '&_limit=5&_sort=created_at:DESC';
    const json_notification = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_notification + filter, json_notification).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.dataNotification = result_msg;
        } catch (error) {
          this.dataNotification = null;
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.dataNotification = null;
        this.loading = false;
      });
  }

  onClickedOutside(e: Event) {
      if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
          this.toggleOn = true;
          // this.verticalNavType = 'offcanvas';
      }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
      if (windowWidth >= 768 && windowWidth <= 1024) {
        this.deviceType = 'tablet';
        // this.verticalNavType = 'collapsed';
        this.verticalEffect = 'push';
      } else if (windowWidth < 768) {
        this.deviceType = 'mobile';
        // this.verticalNavType = 'offcanvas';
        this.verticalEffect = 'overlay';
      } else {
        this.deviceType = 'desktop';
        // this.verticalNavType = 'expanded';
        this.verticalEffect = 'shrink';
      }
  }

  searchFriendList(event) {
    const search = (this.search_friends.nativeElement.value).toLowerCase();
    let search_input: string;
    let search_parent: any;
    const friendList = document.querySelectorAll('.userlist-box .media-body .chat-header');
    Array.prototype.forEach.call(friendList, function(elements, index) {
      search_input = (elements.innerHTML).toLowerCase();
      search_parent = (elements.parentNode).parentNode;
      if (search_input.indexOf(search) !== -1) {
        search_parent.classList.add('show');
        search_parent.classList.remove('hide');
      } else {
        search_parent.classList.add('hide');
        search_parent.classList.remove('show');
      }
    });
  }

  toggleChat() {
    this.chatToggle = this.chatToggle === 'out' ? 'in' : 'out';
  }

  toggleChatInner() {
    this.chatInnerToggle = this.chatInnerToggle === 'off' ? 'on' : 'off';
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
        this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
        // this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
        // this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar = this.isCollapsedSideBar === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }
}
