import {Routes} from '@angular/router';

export const ProgramRoutes: Routes = [
  {
    path: '',
    data: {
      breadcrumb: 'Kegiatan',
      status: true
    },
    children: [
      {
        path: '',
        loadChildren: './program-list/program-list.module#ProgramListModule'
      },
      {
        path: 'list',
        loadChildren: './program-list/program-list.module#ProgramListModule'
      },
      {
        path: 'add',
        loadChildren: './program-add/program-add.module#ProgramAddModule'
      },
      {
        path: ':id',
        children: [
          {
            path: '',
            loadChildren: './program-detail/program-detail.module#ProgramDetailModule',
          },
          {
            path: 'detail',
            loadChildren: './program-detail/program-detail.module#ProgramDetailModule',
          },
          {
            path: 'edit',
            loadChildren: './program-edit/program-edit.module#ProgramEditModule',
          },
          {
            path: 'statistik',
            loadChildren: './program-statistic/program-statistic.module#ProgramStatisticModule',
          },
          {
            path: 'indikator',
            children: [
              {
                path: 'add',
                loadChildren: '../indikator/indikator-add/indikator-add.module#IndikatorAddModule',
              },
              {
                path: ':id',
                children: [
                  {
                    path: '',
                    loadChildren: '../indikator/indikator-detail/indikator-detail.module#IndikatorDetailModule',
                  },
                  {
                    path: 'detail',
                    loadChildren: '../indikator/indikator-detail/indikator-detail.module#IndikatorDetailModule',
                  },
                  {
                    path: 'edit',
                    loadChildren: '../indikator/indikator-edit/indikator-edit.module#IndikatorEditModule',
                  },
                  {
                    path: 'periode',
                    children: [
                      {
                        path: 'add',
                        loadChildren: '../periode-progress/periode-progress-add/periode-progress-add.module#PeriodeProgressAddModule',
                      },
                      {
                        path: ':id',
                        children: [
                          {
                            path: '',
                            loadChildren: '../periode-progress/periode-progress-detail/periode-progress-detail.module#PeriodeProgressDetailModule',
                          }, 
                          {
                            path: 'detail',
                            loadChildren: '../periode-progress/periode-progress-detail/periode-progress-detail.module#PeriodeProgressDetailModule',
                          },
                          {
                            path: 'edit',
                            loadChildren: '../periode-progress/periode-progress-edit/periode-progress-edit.module#PeriodeProgressEditModule',
                          },
                          {
                            path: 'progress',
                            children: [ 
                              {
                                path: 'add',
                                loadChildren: '../lap-progress/lap-progress-add/lap-progress-add.module#LapProgressAddModule',
                              },
                              {
                                path: ':id',
                                children: [
                                  {
                                    path: '',
                                    loadChildren: '../lap-progress/lap-progress-detail/lap-progress-detail.module#LapProgressDetailModule',
                                  }, 
                                  {
                                    path: 'detail',
                                    loadChildren: '../lap-progress/lap-progress-detail/lap-progress-detail.module#LapProgressDetailModule',
                                  },
                                  {
                                    path: 'edit',
                                    loadChildren: '../lap-progress/lap-progress-edit/lap-progress-edit.module#LapProgressEditModule',
                                  }, 
                                  {
                                    path: 'verifikasi',
                                    loadChildren: '../lap-progress/lap-progress-verifikasi/lap-progress-verifikasi.module#LapProgressVerifikasiModule',
                                  }, 
                                ] 
                              }
                            ]
                          }
                        ]
                      }, 
                    ]
                  },
                ]
              },
            ]
          },
          {
            path: 'anggaran',
            children: [
              {
                path: 'add',
                loadChildren: '../periode-anggaran/periode-anggaran-add/periode-anggaran-add.module#PeriodeAnggaranAddModule',
              },
              {
                path: ':id',
                children: [
                  {
                    path: '',
                    loadChildren: '../periode-anggaran/periode-anggaran-detail/periode-anggaran-detail.module#PeriodeAnggaranDetailModule',
                  },
                  {
                    path: 'detail',
                    loadChildren: '../periode-anggaran/periode-anggaran-detail/periode-anggaran-detail.module#PeriodeAnggaranDetailModule',
                  },
                  {
                    path: 'edit',
                    loadChildren: '../periode-anggaran/periode-anggaran-edit/periode-anggaran-edit.module#PeriodeAnggaranEditModule',
                  },
                  {
                    path: 'kegiatan',
                    children: [
                      {
                        path: 'add',
                        loadChildren: '../kegiatan/kegiatan-add/kegiatan-add.module#KegiatanAddModule',
                      },
                      {
                        path: ':id',
                        children: [
                          {
                            path: '',
                            loadChildren: '../kegiatan/kegiatan-detail/kegiatan-detail.module#KegiatanDetailModule',
                          }, 
                          {
                            path: 'detail',
                            loadChildren: '../kegiatan/kegiatan-detail/kegiatan-detail.module#KegiatanDetailModule',
                          },
                          {
                            path: 'edit',
                            loadChildren: '../kegiatan/kegiatan-edit/kegiatan-edit.module#KegiatanEditModule',
                          },
                          {
                            path: 'serapan',
                            children: [ 
                              {
                                path: 'add',
                                loadChildren: '../lap-anggaran/lap-anggaran-add/lap-anggaran-add.module#LapAnggaranAddModule',
                              },
                              {
                                path: ':id',
                                children: [
                                  {
                                    path: '',
                                    loadChildren: '../lap-anggaran/lap-anggaran-detail/lap-anggaran-detail.module#LapAnggaranDetailModule',
                                  }, 
                                  {
                                    path: 'detail',
                                    loadChildren: '../lap-anggaran/lap-anggaran-detail/lap-anggaran-detail.module#LapAnggaranDetailModule',
                                  },
                                  {
                                    path: 'edit',
                                    loadChildren: '../lap-anggaran/lap-anggaran-edit/lap-anggaran-edit.module#LapAnggaranEditModule',
                                  }, 
                                  {
                                    path: 'verifikasi',
                                    loadChildren: '../lap-anggaran/lap-anggaran-verifikasi/lap-anggaran-verifikasi.module#LapAnggaranVerifikasiModule',
                                  }, 
                                ] 
                              }
                            ]
                          }
                        ]
                      }, 
                    ]
                  },
                ]
              },
            ]
          },
        ]
      }, 
    ]
  }
];


