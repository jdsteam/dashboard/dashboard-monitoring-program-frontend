import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgramStatisticComponent} from './program-statistic.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { EsriMapComponent } from '../../esri-map/esri-map.component'; 

export const ProgramStatisticRoutes: Routes = [{
    path: '',
    component: ProgramStatisticComponent,
    data: {
        breadcrumb: "Statistik Kegiatan",
        status: false
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgramStatisticRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [
        ProgramStatisticComponent,
        EsriMapComponent],
    providers: [
        EsriMapComponent
    ]
})

export class ProgramStatisticModule {}
