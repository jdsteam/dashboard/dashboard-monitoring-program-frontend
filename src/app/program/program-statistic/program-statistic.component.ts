import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import {ChartEvent, ChartOptions, ChartType} from 'ng-chartist';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import {EsriMapComponent} from '../../esri-map/esri-map.component';
import * as moment from 'moment';

import * as Chartist from 'chartist';

@Component({
  selector: 'app-program-statistic',
  templateUrl: './program-statistic.component.html',
  styleUrls: [ './program-statistic.component.css',],
  animations: [fadeInOutTranslate]
})
export class ProgramStatisticComponent implements OnInit {

  public loading = false; 
  public finish_loading = false; 
  public finish_chart_kegiatan = false; 
  public finish_chart_indikator = false; 
  public finish_chart_anggaran = false;  
  private url_periode = '/api/periodes';
  private url_program = '/api/programs';
  private url_anggaran = '/api/periodeanggarans';
  private url_progress = '/api/periodeprogresses';
  private url_indikator = '/api/indikators'; 
  private url_kegiatan = '/api/kegiatans'; 
  private url_lapprogress = '/api/lapprogresses'; 
  private url_lapanggaran = '/api/lapanggarans'; 
  private url_lapanggaranplan = '/api/lapanggaranplans'; 
  private url_refkelurahan = '/api/refkelurahans'; 
  private url_refkecamatan = '/api/refkecamatans'; 
  private url_refkota = '/api/refkotas'; 
  private url_iku = '/api/ikus';  

  disableTextbox:Boolean = true;  
  dataPeriode = []; 
  dataProgram = []; 
  dataAnggaran = []; 
  dataProgress = []; 
  dataPeriodeProgressNonFilter = []; 
  dataKegiatan = [];  
  dataKegiatanIndikator = [];  
  dataIndikator = [];  
  dataLapAnggaran = []; 
  dataLapAnggaranAPBD = []; 
  dataLapAnggaranNonAPBD = []; 
  dataLapAnggaranPlan = []; 
  dataLapAnggaranPlanAPBD = []; 
  dataLapAnggaranPlanNonAPBD = []; 
  dataLapProgress = []; 
  dataLapProgressNonFilter = []; 
  dataLapOutput = []; 
  dataIKU = [];   

  anggaran_apbd = 0;
  anggaran_non_apbd = 0;
  anggaran_max = 0;
  anggaran_sumber = [];
  
  allProgress = [];  
  donutChartData = {};
  donutChartData2 = [];
  barChartData = {};
  lineChartData = {};

  tahunperiode = 0;

  myForm: FormGroup; 
  pic_asn: FormArray;
  pic_non_asn: FormArray;
  tajj: FormArray;
  fasilitator: FormArray;
  mitra: FormArray; 
  problem: FormArray;
  goal: FormArray;

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';


  submitted: boolean;
  logoPath = "";
  logoNull = "/api/uploads/default-logo.png";   

  mapCenter = [107.604, -6.922];
  basemapType = 'hybrid';
  mapZoomLevel = 8;
  mapData = {};

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location,
    private _sanitizer: DomSanitizer,
    private esri: EsriMapComponent) {
 
  }

  ngOnInit() {
 
    if(!this.session.checkAccess('program','statistik')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = id; 

    // set data 
    this.myForm = new FormGroup({  
      periode: new FormControl({value: '2'})
    });

    this.getDataPeriode();
    this.getDataProgram(id);
    this.getDataAnggaran(id); 
    this.getDataIndikator(id);  
    this.getDataKegiatan(id);  
  
  }
 
  // See app.component.html
  mapLoadedEvent(status: boolean) {
    console.log('The map loaded: ' + status);
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  onChangePeriode(event, i) {
    let text = event.target['options'][event.target['options'].selectedIndex].text; 
    let value = event.target.value;  
    console.log(text);
    console.log(value);
    this.tahunperiode = Number(value);
      
    this.dataAnggaran = []; 
    this.dataProgress = []; 
    this.dataPeriodeProgressNonFilter = []; 
    this.dataKegiatan = []; 
    this.dataKegiatanIndikator = [];  
    this.dataIndikator = [];   
    this.dataLapProgress = [];  
    this.dataLapProgressNonFilter = [];  
    this.dataLapAnggaran = []; 
    this.dataLapAnggaranAPBD = []; 
    this.dataLapAnggaranNonAPBD = []; 
    this.dataLapAnggaranPlan = []; 
    this.dataLapAnggaranPlanAPBD = []; 
    this.dataLapAnggaranPlanNonAPBD = []; 
    
    this.anggaran_apbd = 0;
    this.anggaran_non_apbd = 0;
    this.anggaran_max = 0;
    this.anggaran_sumber = [];
 
    this.allProgress = [];  
    this.donutChartData =  {
      type: 'doughnut',
      data: { 
        labels: [
          'Selesai', 'Belum Selesai'
        ], 
        datasets: [{
          data: [], 
          backgroundColor: [
            '#23038e',
            '#E6E6E6', 
          ], 
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Progress Kegiatan',
          position: 'bottom'
        },
        legend: {
          display: false, 
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function(tooltipItem, data) {
              return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
            }
          } 
        },
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['#FFFFFF', ''],
            precision: 2
          }
        }
      }
    };
  
    this.barChartData =  {
      type: 'bar',
      data: { 
        labels: [], 
        datasets: [{
          data: [], 
          backgroundColor: '#23038e', 
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Progress Indikator Kegiatan',
          position: 'bottom'
        },
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 80,
        legend: {
          display: false, 
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']] + '%';
            }
          } 
        },
        scales:{
          xAxes: [{ 
            display: true,  
          }], 
          yAxes: [{
            ticks : {
              max: 100
            }
          }]
        },
        plugins: {
          labels: {
            render: 'value',  
          }
        }
      }
    };

    this.lineChartData =  {
      type: 'line',
      data: { 
        labels: [], 
        datasets: [{
          data: [],  
          fill: false,
          borderColor: 'red',
          backgroundColor: 'transparent',
          pointBorderColor: 'red',
          pointBackgroundColor: 'rgba(255,0,0,0.5)',
          pointRadius: 5,
          pointHoverRadius: 10,
          pointHitRadius: 30,
          pointBorderWidth: 2, 
        }]
      },
      options: {
        title: {
          display: true,
          text: 'Kurva S Serapan Anggaran',
          position: 'bottom'
        }, 
        responsive: true,
        maintainAspectRatio: false, 
        legend: {
          display: false, 
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function(tooltipItem, data) {
              return 'Rp. ' + data['datasets'][0]['data'][tooltipItem['index']];
            }
          } 
        },
        scales:{
          xAxes: [{ 
            position: 'top',
            display: true,  
          },
          { 
            position: 'bottom',
            display: false
          }], 
          yAxes: [{
            position: 'right',
            ticks : {
              max: this.anggaran_max
            }
          }]
        },
        plugins: {
          labels: {
            render: 'value',  
            position: 'outside'
          }
        }
      }
    };

    this.mapData = {};
  
    this.getDataAnggaran(this.program_id); 
    this.getDataIndikator(this.program_id);  
    this.getDataKegiatan(this.program_id);  
  
  }

  getDataPeriode() {

    const json_periode = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_periode + '?_sort=id', json_periode).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false; 
          this.dataPeriode = result_msg;
          // console.log(this.dataPeriode);

          this.myForm.patchValue({
            periode: 2, 
          });

          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataPeriode = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataPeriode = null;
        return null;
      }
    );
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/'+ id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.finish_loading = true;
          this.dataProgram = result_msg;
          // console.log(this.dataProgram);
          this.getDataIKU();  
          // set data 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram = null;
        return null;
      }
    );
  }

  getDataAnggaran(id) {
 
    this.loading = true;
    let params = '';
    if(this.tahunperiode === 0 || this.tahunperiode === 2){ 
      params = '?program='+ id + '&is_deleted=false&_sort=id:asc';
    }else{
      params = '?program='+ id + '&periode=' + this.tahunperiode + '&is_deleted=false&_sort=id:asc';
    }
    this.httpRequest.httpGet(this.url_anggaran + params, {}).subscribe(
      result => {
        try { 
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataAnggaran = result_msg; 
          this.countPencapaianAnggaran(result_msg);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataAnggaran = null;
        return null;
      }
    );
 
    
  }
 
  getDataIndikator(id) {
    this.loading = true;

    const json_indikator = {};
    this.httpRequest.httpGet(this.url_indikator + '?program='+ id  + '&is_deleted=false&_sort=id:asc', json_indikator).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataKegiatanIndikator = result_msg;
          this.dataIndikator = result_msg;
          this.countPencapaianKegiatan(result_msg);
          this.countPencapaianIndikator(result_msg);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatanIndikator = null;
          this.dataIndikator = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatanIndikator = null;
        this.dataIndikator = null;
        return null;
      });
  }

  getDataKegiatan(id) {

    const json_kegiatan = {};
    this.loading = true;
    let params = '';
    if(this.tahunperiode === 0 || this.tahunperiode === 2){ 
      params = '?program='+ id + '&is_deleted=false&_sort=id:asc';
    }else{
      params = '?program='+ id + '&periode=' + this.tahunperiode + '&is_deleted=false&_sort=id:asc';
    }
    this.httpRequest.httpGet(this.url_kegiatan + params, json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataKegiatan = result_msg;
          this.countKegiatan(result_msg);
          // console.log(this.dataKegiatan); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataKegiatan = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataKegiatan = null;
        return null;
      }
    );
  }
 
  getDataIKU() {

    const json_iku = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_iku + '?is_deleted=false&_sort=nama:asc', json_iku).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIKU = result_msg;
          // console.log(this.dataDinas);

          let i = 0;
          this.dataProgram['iku']['arr'].forEach(element2 => { 
            let result = this.dataIKU.filter(obj => {
              return String(obj.id) === element2['value']
            })
            try { 
              this.dataProgram['iku']['arr'][i]['nama'] = result[0]['nama']; 
            } catch (error) {
              
            }
            i++;
          }); 

          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIKU = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIKU = null;
        return null;
      }
    );
  }

  countPencapaianKegiatan(row){
    row.forEach(element => {
      this.loading = true;

      const json_lapprogress = {};
      let params = '';
      if(this.tahunperiode === 0 || this.tahunperiode === 2){ 
        params = '/?indikator=' + element.id + '&is_deleted=false&is_verified=true';
      }else{
        params = '/?indikator=' + element.id + '&periode=' + this.tahunperiode + '&is_deleted=false&is_verified=true';
      }

      this.httpRequest.httpGet(this.url_lapprogress+  params, json_lapprogress).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;

            temp.forEach(element => {
              this.dataLapProgress.push(element);
              kuantitas = kuantitas + element.kuantitas;
              target = element.indikator.target_kuantitatif;
            });

            pencapaian = (kuantitas / target) * 100;
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            let index = this.dataKegiatanIndikator.indexOf(element);
            this.dataKegiatanIndikator[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataKegiatanIndikator[index]['pencapaian_persen'] = pencapaian;
            this.dataKegiatanIndikator[index]['chart'] = {};

            this.loading = false;
            this.countAllProgressKegiatan();
            this.generateFeatureLayer();

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        });
    });
  }
    
  countPencapaianIndikator(row){
    row.forEach(async indikator => {
      this.loading = true; 

      const json_progress = {};
      let params_progress = '?indikator=' + indikator.id + '&is_deleted=false';
      await this.httpRequest.httpGet(this.url_progress+  params_progress, json_progress).subscribe(
        result => {
          const result_msg = JSON.parse(result._body);
          // console.log('periodeprogress', result_msg);

          result_msg.forEach(periodeprogress => {
            this.dataPeriodeProgressNonFilter.push(periodeprogress);
          });

          this.loading = false;
        }, 
        error => {
          console.log(error);
          this.loading = false;
        });

      const json_lapprogress = {};
      let params = '?indikator=' + indikator.id + '&is_deleted=false&is_verified=true';
      await this.httpRequest.httpGet(this.url_lapprogress+  params, json_lapprogress).subscribe(
        result => {
          const result_msg = JSON.parse(result._body);
          // console.log('lapprogress', result_msg);

          let kuantitas = 0;
          let pencapaianperindikator = 0;
          result_msg.forEach(lapprogress => {
            this.dataLapProgressNonFilter.push(lapprogress);
            kuantitas = kuantitas + Number(lapprogress.kuantitas);
          });

          pencapaianperindikator = (kuantitas / indikator.target_kuantitatif) * 100;
          pencapaianperindikator = +pencapaianperindikator.toFixed(2);
          pencapaianperindikator = pencapaianperindikator || 0;

          let index = this.dataIndikator.indexOf(indikator);
          this.dataIndikator[index]['chart'] = {
            type: 'doughnut',
            data: { 
              labels: [
                'Selesai', 'Belum Selesai'
              ], 
              datasets: [{
                data: [pencapaianperindikator, 100 - pencapaianperindikator], 
                backgroundColor: [
                  '#23038e',
                  '#E6E6E6', 
                ], 
              }]
            },
            options: {
              title: {
                display: false,
                text: 'Progress Indikator',
                position: 'bottom'
              },
              legend: {
                display: false, 
              },
              tooltips: {
                enabled: true,
                callbacks: {
                  label: function(tooltipItem, data) {
                    return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
                  }
                } 
              },
              plugins: {
                labels: {
                  render: 'percentage',
                  fontColor: ['#FFFFFF', ''],
                  precision: 2
                }
              }
            }
          };

          this.loading = false;
          this.countAllProgressIndikator();
        }, 
        error => {
          console.log(error);
          this.loading = false;
        });
    });
  }
  
  countPencapaianAnggaran(row){

    row.forEach(element => {
      // this.loading = true;
      let params = '';
      if(this.tahunperiode === 0 || this.tahunperiode === 2){ 
        params = '?periodeanggaran='+ element.id + '&is_deleted=false&_sort=id:asc';
      }else{
        params = '?periodeanggaran='+ element.id + '&periode=' + this.tahunperiode + '&is_deleted=false&_sort=id:asc';
      }
      // console.log(params)

      this.httpRequest.httpGet(this.url_lapanggaran + params, {}).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.loading = false; 
            result_msg.forEach(element => { 
              // console.log(element); 
              this.dataLapAnggaran.push(element);
              if(element['kegiatan']['kategori'] === 'apbd'){
                this.dataLapAnggaranAPBD.push(element);
              }else if(element['kegiatan']['kategori'] === 'nonapbd'){
                this.dataLapAnggaranNonAPBD.push(element);
              }
            });
            this.countAllProgressAnggaran();
            return result_msg;

          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
            // this.loading = false;
            this.dataLapAnggaran = null;
            return null;
          }
        },
        error => {
          console.log(error);
          // this.loading = false;
          this.dataLapAnggaran = null;
          return null;
        }
      );


      this.httpRequest.httpGet(this.url_lapanggaranplan + params, {}).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.loading = false; 
            result_msg.forEach(element => { 
              // console.log(element); 
              this.dataLapAnggaranPlan.push(element);
              if(element['kegiatan']['kategori'] === 'apbd'){
                this.dataLapAnggaranPlanAPBD.push(element);
              }else if(element['kegiatan']['kategori'] === 'nonapbd'){
                this.dataLapAnggaranPlanNonAPBD.push(element);
              }
            });
            this.countAllProgressAnggaran();
            return result_msg;

          } catch (error) {
            this.errorMessage.openErrorSwal('Something wrong.');
            console.log(error);
            // this.loading = false;
            this.dataLapAnggaran = null;
            return null;
          }
        },
        error => {
          console.log(error);
          // this.loading = false;
          this.dataLapAnggaran = null;
          return null;
        }
      );
    });
 
  }

  countKegiatan(row){

    row.forEach(element => { 
      
      let params = '';
      if(this.tahunperiode === 0 || this.tahunperiode === 2){ 
        params = '/?kegiatan=' + element.id + '&is_deleted=false&is_verified=true';
      }else{
        params = '/?kegiatan=' + element.id + '&periode=' + this.tahunperiode + '&is_deleted=false&is_verified=true';
      }
      this.httpRequest.httpGet(this.url_lapanggaran+ params , {}).subscribe(
        result => {
          try { 
            const result_msg = JSON.parse(result._body);  
            let kuantitas = 0; 
            let pencapaian = 0;
            result_msg.forEach(element2 => { 
              kuantitas = kuantitas + parseInt(element2.serapan); 
            });
            pencapaian = (kuantitas / parseInt(element.anggaran)) * 100;
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0; 
            let index = this.dataKegiatan.indexOf(element); 
            this.dataKegiatan[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataKegiatan[index]['pencapaian_persen'] = pencapaian;  
          } catch (error) {
            // this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          // this.loading = false;
        }
      );
    });

    // console.log(this.dataKegiatan)
  }

  countAllProgressKegiatan(){
    this.finish_chart_kegiatan = false;

    let sum_persen = 0;
    let persen = 0;
    let arr_label = [];
    let arr_data = [];

    this.dataKegiatanIndikator.forEach(element => {
      arr_label.push(element.indikator);
      arr_data.push(Number(element.pencapaian_persen)); 
      sum_persen = sum_persen + element.pencapaian_persen;  
    });

    persen = sum_persen / this.dataKegiatanIndikator.length;

    this.donutChartData =  {
      type: 'doughnut',
      data: { 
        labels: [
          'Selesai', 'Belum Selesai'
        ], 
        datasets: [{
          data: [persen, 100-persen], 
          backgroundColor: [
            '#23038e',
            '#E6E6E6', 
          ], 
        }]
      },
      options: {
        title: {
          display: false,
          text: 'Progress Kegiatan',
          position: 'bottom'
        },
        legend: {
          display: false, 
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function(tooltipItem, data) {
              return data['labels'][tooltipItem['index']] + ': ' + data['datasets'][0]['data'][tooltipItem['index']] + '%';
            }
          } 
        },
        plugins: {
          labels: {
            render: 'percentage',
            fontColor: ['#FFFFFF', ''],
            precision: 2
          }
        }
      }
    };
  
    this.barChartData =  {
      type: 'bar',
      data: { 
        labels: arr_label, 
        datasets: [{
          data: arr_data, 
          backgroundColor: '#23038e', 
        }]
      },
      options: {
        title: {
          display: false,
          text: 'Progress Indikator Kegiatan',
          position: 'bottom'
        },
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 80,
        legend: {
          display: false, 
        },
        tooltips: {
          enabled: true,
          callbacks: {
            label: function(tooltipItem, data) {
              return data['datasets'][0]['data'][tooltipItem['index']] + '%';
            }
          } 
        },
        scales:{
          xAxes: [{ 
            display: false,  
          }], 
          yAxes: [{
            ticks : {
              max: 100, 
              beginAtZero: true, 
            }
          }]
        },
        plugins: {
          labels: {
            render: 'value',  
          }
        }
      }
    };
 
    this.finish_chart_kegiatan = true; 
  }

  countAllProgressIndikator(){
    this.finish_chart_indikator = false;

    let group_perprogress = this.dataPeriodeProgressNonFilter.reduce(function(r, a) {
      r[a.indikator.id] = r[a.indikator.id] || [];
      r[a.indikator.id].push(a);
      return r;
    }, {});  
    // console.log(group_perprogress);

    let group_lapprogress = this.dataLapProgressNonFilter.reduce(function(r, a) {
      r[a.indikator.id] = r[a.indikator.id] || [];
      r[a.indikator.id].push(a);
      return r;
    }, {});  
    // console.log(group_lapprogress);

    // console.log(this.dataPeriodeProgressNonFilter);
    // console.log(this.dataLapProgressNonFilter);
    let index = 0;
    this.dataIndikator.forEach(indikator => {

      let hasil = [];
      let g_tahun = [];
      let g_persen = [];
      this.dataPeriode.forEach(periode => {
        if(periode.id !== 2){

          let target = 0;
          if(group_lapprogress[indikator.id]){
            let target_arr = group_perprogress[indikator.id].find(obj => {
              return obj.periode.id === periode.id
            })
            target = (target_arr) ? Number(target_arr.target) : 0;
          }
          
          let pencapaian_arr = [];
          if(group_lapprogress[indikator.id]){
            let pencapaian_find = group_lapprogress[indikator.id].find(obj => {
              if(obj.periode.id === periode.id){
                pencapaian_arr.push(obj)
              }
            })
          }

          let pencapaian = pencapaian_arr.reduce((acc, curr)=> acc + curr.kuantitas, 0);
          let progress = (pencapaian / target) * 100;
          progress = +progress.toFixed(2);
          progress = progress || 0;

          hasil.push({
            tahun: periode.tahun,
            target: target,
            pencapaian: pencapaian,
            satuan: indikator.target_satuan,
            progress: progress,
          });

          g_tahun.push(periode.tahun);
          g_persen.push(progress);

        }
      });

      this.dataIndikator[index]['table'] = hasil;

      this.dataIndikator[index]['chart_bar'] =  {
        type: 'horizontalBar',
        data: { 
          labels: g_tahun, 
          datasets: [{
            data: g_persen, 
            backgroundColor: '#23038e', 
          }]
        },
        options: {
          title: {
            display: false,
            text: 'Progress Target Periode',
            position: 'bottom'
          },
          responsive: true,
          maintainAspectRatio: false,
          cutoutPercentage: 80,
          legend: {
            display: false, 
          },
          tooltips: {
            enabled: true,
            callbacks: {
              label: function(tooltipItem, data) {
                return data['datasets'][0]['data'][tooltipItem['index']] + '%';
              }
            } 
          },
          scales:{
            xAxes: [{
              ticks: {
                max: 100, 
                beginAtZero: true, 
              }
            }], 
            yAxes: [{
              ticks : {
                max: 100, 
                beginAtZero: true, 
              }
            }]
          },
          plugins: {
            labels: {
              render: 'value',  
            }
          }
        }
      }; 

      index = index + 1; 
    });
 
    this.finish_chart_indikator = true; 
  }

  countAllProgressAnggaran(){

    this.finish_chart_anggaran = false;
    let arr_data = [];
    let arr_label = [];   
    let max_anggaran = 0;
    let apbd_anggaran = 0;
    let non_apbd_anggaran = 0;
    let sumber_anggaran = [];

    // console.log(this.dataAnggaran);
    this.dataAnggaran.forEach(element => {
      if(Number(element.apbd) > max_anggaran){  
        max_anggaran = Number(element.apbd); 
      }
      if(Number(element.nonapbd) > max_anggaran){  
        max_anggaran = Number(element.nonapbd); 
      } 
      apbd_anggaran = Number(apbd_anggaran) +  Number(element.apbd);
      non_apbd_anggaran = Number(non_apbd_anggaran) + Number(element.nonapbd);
      sumber_anggaran.push(element.sumber)
    });

    this.anggaran_max = max_anggaran;
    this.anggaran_apbd = apbd_anggaran;
    this.anggaran_non_apbd = non_apbd_anggaran;
    this.anggaran_sumber = sumber_anggaran; 
  
    let tempsAPBD = []; 
    let tempsNonAPBD = [];
    let tempsPlanAPBD = []; 
    let tempsPlanNonAPBD = []; 
    let tempsOutput = []; 
    
    this.sleep(500).then(() => {
      tempsAPBD = this.orderBulan(this.dataLapAnggaranAPBD); 
      tempsNonAPBD = this.orderBulan(this.dataLapAnggaranNonAPBD);
      tempsPlanAPBD = this.orderBulan(this.dataLapAnggaranPlanAPBD); 
      tempsPlanNonAPBD = this.orderBulan(this.dataLapAnggaranPlanNonAPBD); 
      tempsOutput = this.orderBulanTanggal(this.dataLapProgress); 
 
      // bring label
      let labelNonAPBD = [];
      for(let i=0; i<tempsNonAPBD.length; i++){
        labelNonAPBD.push(tempsNonAPBD[i]['bulan'])
      }
      arr_label = labelNonAPBD;

      if(tempsAPBD.length > 0){
        let labelAPBD = [];
        for(let i=0; i<tempsAPBD.length; i++){
          labelAPBD.push(tempsAPBD[i]['bulan'])
        }
        arr_label = labelAPBD;

      }

      // bring data
      let dataAPBD = [];
      let serapan_totalAPBD = 0;
      for(let i=0; i<tempsAPBD.length; i++){
        serapan_totalAPBD = serapan_totalAPBD + tempsAPBD[i]['serapan']; 
        dataAPBD.push(serapan_totalAPBD); 
      } 
      arr_data.push(dataAPBD);

      let dataNonPBD = [];
      let serapan_totalNonAPBD = 0;
      for(let i=0; i<tempsNonAPBD.length; i++){
        serapan_totalNonAPBD = serapan_totalNonAPBD + tempsNonAPBD[i]['serapan']; 
        dataNonPBD.push(serapan_totalNonAPBD); 
      } 
      arr_data.push(dataNonPBD);

      let dataPlanAPBD = [];
      let serapan_totalPlanAPBD = 0;
      for(let i=0; i<tempsPlanAPBD.length; i++){
        serapan_totalPlanAPBD = serapan_totalPlanAPBD + tempsPlanAPBD[i]['serapan']; 
        dataPlanAPBD.push(serapan_totalPlanAPBD); 
      } 
      arr_data.push(dataPlanAPBD);

      let dataPlanNonPBD = [];
      let serapan_totalPlanNonAPBD = 0;
      for(let i=0; i<tempsPlanNonAPBD.length; i++){
        serapan_totalPlanNonAPBD = serapan_totalPlanNonAPBD + tempsPlanNonAPBD[i]['serapan']; 
        dataPlanNonPBD.push(serapan_totalPlanNonAPBD); 
      } 
      arr_data.push(dataPlanNonPBD);
  
      let dataOutput = [];
      let serapan_totalOutput = 0;
      for(let i=0; i<tempsOutput.length; i++){
        serapan_totalOutput = serapan_totalOutput + tempsOutput[i]['persen']; 
        dataOutput.push(serapan_totalOutput.toFixed(2)); 
      }  
      arr_data.push(dataOutput)

      // console.log(arr_label)
      // console.log(arr_data)
  
      this.lineChartData =  {
        type: 'line',
        data: { 
          labels: arr_label, 
          datasets: [
          {
            label: 'Rencana APBD',
            data: arr_data[2],  
            fill: false,
            yAxisID: 'anggaran',
            borderColor: '#FFD3D3',
            backgroundColor: 'transparent',
            pointBorderColor: '#FFD3D3',
            pointBackgroundColor: 'rgba(255,211,211,0.5)',
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2, 
          },
          {
            label: 'Realisasi APBD',
            data: arr_data[0],  
            fill: false,
            yAxisID: 'anggaran',
            borderColor: '#bb1542',
            backgroundColor: 'transparent',
            pointBorderColor: '#bb1542',
            pointBackgroundColor: 'rgba(208,0,54,0.5)',
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2, 
          },
          {
            label: 'Rencana Non APBD',
            data: arr_data[3],  
            fill: false,
            yAxisID: 'anggaran',
            borderColor: '#93CD7C',
            backgroundColor: 'transparent',
            pointBorderColor: '#93CD7C',
            pointBackgroundColor: 'rgba(147,205,124,0.5)',
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2, 
          },
          {
            label: 'Realisasi Non APBD',
            data: arr_data[1],  
            fill: false,
            yAxisID: 'anggaran',
            borderColor: '#207561',
            backgroundColor: 'transparent',
            pointBorderColor: '#207561',
            pointBackgroundColor: 'rgba(0,119,99,0.5)',
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2, 
          },
          {
            label: 'Output',
            data: arr_data[4],  
            fill: true,
            yAxisID: 'output',
            borderColor: '#3382C7',
            // backgroundColor: 'rgba(51,130,199,0.2)',
            backgroundColor: 'transparent',
            pointBorderColor: '#3382C7',
            pointBackgroundColor: 'rgba(51,130,199,0.5)', 
            pointRadius: 5,
            pointHoverRadius: 10,
            pointHitRadius: 30,
            pointBorderWidth: 2,  
          },
        ]
        },
        options: {
          title: {
            display: false,
            text: 'Kurva S Serapan Anggaran',
            position: 'bottom'
          }, 
          responsive: true,
          maintainAspectRatio: false, 
          legend: {
            display: true, 
            position: 'top',
          },
          tooltips: {
            enabled: true,
            mode: 'index',
            intersect: false,
            callbacks: {
              label: function(tooltipItem, data) {
                if (tooltipItem.datasetIndex === 0) {
                  return 'Rencana APBD          Rp. ' + 
                    Number(data['datasets'][0]['data'][tooltipItem['index']]).toFixed(0).replace(/./g, function(c, i, a) {
                      return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
                    }); 
                }
                else if (tooltipItem.datasetIndex === 1) {
                  return 'Realisasi APBD          Rp. ' + 
                    Number(data['datasets'][1]['data'][tooltipItem['index']]).toFixed(0).replace(/./g, function(c, i, a) {
                      return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
                    }); 
                }
                else if (tooltipItem.datasetIndex === 2) {
                  return 'Rencana Non APBD   Rp. ' + 
                    Number(data['datasets'][2]['data'][tooltipItem['index']]).toFixed(0).replace(/./g, function(c, i, a) {
                      return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
                    }); 
                }
                else if (tooltipItem.datasetIndex === 3) {
                  return 'Realisasi Non APBD   Rp. ' + 
                    Number(data['datasets'][3]['data'][tooltipItem['index']]).toFixed(0).replace(/./g, function(c, i, a) {
                      return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
                    }); 
                }
                else if (tooltipItem.datasetIndex === 4) {
                  return 'Output                              ' + data['datasets'][4]['data'][tooltipItem['index']] + " %"
                }
              }
            } 
          },
          scales:{
            xAxes: [{ 
              ticks : {
                beginAtZero: true,
              },
              position: 'bottom',
              display: true,  
            },
            { 
              position: 'bottom',
              display: false
            }], 
            yAxes: [
              {
                id: 'anggaran',
                position: 'right',
                ticks : {
                  beginAtZero: true,
                  max: max_anggaran, 
                  callback: function(value, index, values) { 
                    return 'Rp. ' + value.toString().replace(/./g, function(c, i, a) {
                      return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
                    });  
                  }
                },
              },
              {
                id: 'output',
                position: 'left',
                ticks : {
                  beginAtZero: true,
                  max: 100,
                  callback: function(value, index, values) { 
                    return value.toString() + '%';
                  }
                },
              }
            ]
          },
          plugins: {
            labels: {
              render: 'value',  
              position: 'outside'
            }, 
          }
        }
      };
      this.finish_chart_anggaran = true;
    
    })
  }

 
  generateFeatureLayer(){

    // console.log(this.dataLapProgress);
    let featurelayer = 
    {
      "type": "FeatureCollection",
      "metadata": {
        "generated": 1459785466000,
        "url": "http://localhost:4200",
        "title": "Kota di Jawa Barat",
        "status": 200,
        "api": "1.5.0",
        "count": 2
      },
      "features": [
        // {
        //   "type": "Feature",
        //   "properties": {
        //     "id": 1,
        //     "program_nama": 'Kredit Mesra',
        //     "indikator_nama": 'Jumlah Penerima Manfaat',
        //     "indikator_target": '8000 orang penerima manfaat',
        //     "periode_tahun": '2019',
        //     "periode_target": '4000',
        //     "progress_pelaksana": 'BJB',
        //     "progress_kuantitas": '2000',
        //     "progress_persen": '50',
        //     "progress_waktu_pelaporan": "2019-07-19T00:00:00.000Z",
        //     "progress_keterangan": "oke"
        //   },
        //   "geometry": {
        //     "type": "Point",
        //     "coordinates": [107.609319, -6.920669]
        //   },
        //   "id": "1"
        // },
        // {
        //   "type": "Feature",
        //   "properties": {
        //     "id": 2,
        //     "program_nama": 'Kredit Mesra',
        //     "indikator_nama": 'Jumlah Penerima Manfaat',
        //     "indikator_target": '8000 orang penerima manfaat',
        //     "periode_tahun": '2019',
        //     "periode_target": '4000',
        //     "progress_pelaksana": 'BJB',
        //     "progress_kuantitas": '2000',
        //     "progress_persen": '50',
        //     "progress_waktu_pelaporan": "2019-07-19T00:00:00.000Z",
        //     "progress_keterangan": "oke"
        //   },
        //   "geometry": {
        //     "type": "Point",
        //     "coordinates": [107.509419, -6.960769]
        //   },
        //   "id": "2"
        // }
      ]
    };

    let i = 0;
    this.dataLapProgress.forEach(element => {
  
      element.lokasi.arr.forEach(element2 => {
         
        if(element2.kode_kelurahan !== ""){
  
          // console.log(element2.kode_kelurahan);
          this.httpRequest.httpGet(this.url_refkelurahan+ '?kode_kelurahan=' + element2.kode_kelurahan, {}).subscribe(
            result => {
              try { 
                const result_msg = JSON.parse(result._body);  
                let temp = {
                  "type": "Feature",
                  "properties": {
                    "id": i+1,
                    "program_nama": element.program.nama,
                    "indikator_nama": element.indikator.indikator,
                    "indikator_target": element.indikator.target,
                    "periode_tahun": element.periode.tahun,
                    "periode_target": element.periodeprogress.target,
                    "progress_pelaksana": element.pelaksana,
                    "progress_kuantitas": element.kuantitas,
                    "progress_persen": element.persen + '%',
                    "progress_waktu_pelaporan": this.convert.formatDateIndoTgl(element.waktu_pelaporan),
                    "progress_keterangan": element.keterangan,
                    "nama_kota": result_msg[0].nama_kota,
                    "nama_kecamatan": result_msg[0].nama_kecamatan,
                    "nama_kelurahan": result_msg[0].nama_kelurahan,
                  },
                  "geometry": {
                    "type": "Point",
                    "coordinates": [result_msg[0].longitude, result_msg[0].latitude]
                  },
                  "id": i+1
                };
                featurelayer.features[i] = temp;
                i = i + 1; 
              } catch (error) {}
            }
          ); 

        }else if(element2.kode_kecamatan !== ""){
 
          this.httpRequest.httpGet(this.url_refkecamatan+ '?kode_kecamatan=' + element2.kode_kecamatan, {}).subscribe(
            result => {
              try { 
                const result_msg = JSON.parse(result._body); 
                let temp = {
                  "type": "Feature",
                  "properties": {
                    "id": i+1,
                    "program_nama": element.program.nama,
                    "indikator_nama": element.indikator.indikator,
                    "indikator_target": element.indikator.target,
                    "periode_tahun": element.periode.tahun,
                    "periode_target": element.periodeprogress.target,
                    "progress_pelaksana": element.pelaksana,
                    "progress_kuantitas": element.kuantitas,
                    "progress_persen": element.persen + '%',
                    "progress_waktu_pelaporan": this.convert.formatDateIndoTgl(element.waktu_pelaporan),
                    "progress_keterangan": element.keterangan,
                    "nama_kota": result_msg[0].nama_kota,
                    "nama_kecamatan": result_msg[0].nama_kecamatan, 
                  },
                  "geometry": {
                    "type": "Point",
                    "coordinates": [result_msg[0].longitude, result_msg[0].latitude]
                  },
                  "id": i+1
                };
                featurelayer.features.push(temp);
                i = i + 1;
              } catch (error) {}
            }
          ); 

        }else if(element2.kode_kota !== ""){
  
          this.httpRequest.httpGet(this.url_refkota+ '?kode_kota=' + element2.kode_kota, {}).subscribe(
            result => {
              try { 
                const result_msg = JSON.parse(result._body); 
                let temp = {
                  "type": "Feature",
                  "properties": {
                    "id": i+1,
                    "program_nama": element.program.nama,
                    "indikator_nama": element.indikator.indikator,
                    "indikator_target": element.indikator.target,
                    "periode_tahun": element.periode.tahun,
                    "periode_target": element.periodeprogress.target,
                    "progress_pelaksana": element.pelaksana,
                    "progress_kuantitas": element.kuantitas,
                    "progress_persen": element.persen + '%',
                    "progress_waktu_pelaporan": this.convert.formatDateIndoTgl(element.waktu_pelaporan),
                    "progress_keterangan": element.keterangan,
                    "nama_kota": result_msg[0].nama_kota, 
                  },
                  "geometry": {
                    "type": "Point",
                    "coordinates": [result_msg[0].longitude, result_msg[0].latitude]
                  },
                  "id": i+1
                };
                featurelayer.features.push(temp);
                i = i + 1;
              } catch (error) {}
            }
          ); 

        }else{
           
        }

      });
  
    });

    this.mapData = featurelayer;
    

  }
 
  showBack(){
    this._location.back();
  }
   

  orderBulan(ress){ 
    
    // console.log(ress)
    let temps = [];
    ress.reduce(function(res, value) {
      if (!res[value.bulan]) {
        res[value.bulan] = { bulan: value.bulan, serapan: 0 };
        temps.push(res[value.bulan])
      }
      res[value.bulan].serapan += Number(value.serapan);
      return res;
    }, {}); 
 
    // console.log(temps);

    let def = [ 
      {bulan : 'Januari', serapan : 0},
      {bulan : 'Februari', serapan : 0},
      {bulan : 'Maret', serapan : 0},
      {bulan : 'April', serapan : 0},
      {bulan : 'Mei', serapan : 0},
      {bulan : 'Juni', serapan : 0},
      {bulan : 'Juli', serapan : 0},
      {bulan : 'Agustus', serapan : 0},
      {bulan : 'September', serapan : 0},
      {bulan : 'Oktober', serapan : 0},
      {bulan : 'November', serapan : 0},
      {bulan : 'Desember', serapan : 0}
    ];
 
    for (let i = 0; i < def.length; i++) { 
      temps.forEach(element => { 
        if(element['bulan'] === def[i]['bulan']){ 
          def[i]['serapan'] = Number(element['serapan']);
        } 
      });
    }

    // console.log(def)
  
    return def;
  }

  orderBulanTanggal(ress){ 
    
    // console.log(ress)
    let temps = []; 
    ress.forEach(element => { 
   
      let month = ''
      let bulan = ''
      try { 
        moment.locale('id');
        month = moment(element['waktu_pelaporan']).format('MM');    
      } catch (error) {
        console.log(error)
      }

      // console.log();
      // console.log(element['id']);
      // console.log(element['waktu_pelaporan']);
      // console.log(bln);
      // console.log(month);

      if(parseInt(month) === 1){
        bulan = 'Januari';
      }
      else if(parseInt(month) === 2){
        bulan = 'Februari';
      }
      else if(parseInt(month) === 3){
        bulan = 'Maret';
      }
      else if(parseInt(month) === 4){
        bulan = 'April';
      }
      else if(parseInt(month) === 5){
        bulan = 'Mei';
      }
      else if(parseInt(month) === 6){
        bulan = 'Juni';
      }
      else if(parseInt(month) === 7){
        bulan = 'Juli';
      }
      else if(parseInt(month) === 8){
        bulan = 'Agustus';
      }
      else if(parseInt(month) === 9){
        bulan = 'September';
      }
      else if(parseInt(month) === 10){
        bulan = 'Oktober';
      }
      else if(parseInt(month) === 11){
        bulan = 'November';
      }
      else if(parseInt(month) === 12){
        bulan = 'Desember';
      }

      temps.push({
        bulan: bulan, 
        serapan: element['kuantitas'], 
        target: element['indikator']['target_kuantitatif'], 
        persen: (Number(element['kuantitas'])/Number(element['indikator']['target_kuantitatif'])*100).toFixed(2)
      })
    });

    // console.log(temps)
    let temps2 = []; 
    let x=[]; 
    temps.reduce(function(res, value) {  
      if (!res[value.bulan]) { 
        res[value.bulan] = { bulan: value.bulan, persen: 0, entitas: 0 };
        temps2.push(res[value.bulan])
        x[value.bulan] = 1; 
      }else{  
        x[value.bulan] = x[value.bulan] + 1;
      } 
      res[value.bulan].persen = (Number(res[value.bulan].persen) + Number(value.persen)).toFixed(2);
      res[value.bulan].entitas = x[value.bulan]; 
      return res;
    }, {}); 
 
    // console.log(temps2);

    let def = [ 
      {bulan: 'Januari', persen: 0, entitas: 0},
      {bulan: 'Februari', persen: 0, entitas: 0},
      {bulan: 'Maret', persen: 0, entitas: 0},
      {bulan: 'April', persen: 0, entitas: 0},
      {bulan: 'Mei', persen: 0, entitas: 0},
      {bulan: 'Juni', persen: 0, entitas: 0},
      {bulan: 'Juli', persen: 0, entitas: 0},
      {bulan: 'Agustus', persen: 0, entitas: 0},
      {bulan: 'September', persen: 0, entitas: 0},
      {bulan: 'Oktober', persen: 0, entitas: 0},
      {bulan: 'November', persen: 0, entitas: 0},
      {bulan: 'Desember', persen: 0, entitas: 0}
    ];
 
    for (let i = 0; i < def.length; i++) { 
      temps2.forEach(element => { 
        if(element['bulan'] === def[i]['bulan']){ 
          def[i]['persen'] = Number(element['persen']) / this.dataIndikator.length;
        } 
      });
    }

    // console.log(def)
    // let def = [];
    return def;
  }

  
  sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }


}
