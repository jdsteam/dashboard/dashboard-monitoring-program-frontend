import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-program-edit',
  templateUrl: './program-edit.component.html',
  styleUrls: [ './program-edit.component.css',],
  animations: [fadeInOutTranslate]
})
export class ProgramEditComponent implements OnInit {

  public loading = false; 
  private url_upload = '/api/upload'; 
  private url_dinas = '/api/dinas'; 
  private url_program = '/api/programs';  
  private url_iku = '/api/ikus';  

  disableTextbox:Boolean = false; 
  dataDinas = [];  
  dataProgram = [];   
  dataIKU = [];   
  
  myForm: FormGroup; 
  pic_asn: FormArray;
  pic_non_asn: FormArray;
  tajj: FormArray;
  fasilitator: FormArray;
  mitra: FormArray; 
  problem: FormArray;
  goal: FormArray;
  dampak_pendek: FormArray;
  dampak_menengah: FormArray;
  dampak_panjang: FormArray;
  iku: FormArray;

  submitted: boolean;
  logoPath = "";
  logoNull = "/api/uploads/default-logo.png";   


  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }

  ngOnInit() {

    if(!this.session.checkAccess('program','edit')){
      this.router.navigate(['/error/403']);
    }
    let id = this.route.snapshot.paramMap.get('id'); 
    this.getDataProgram(id); 
    this.getDataDinas();  
    this.getDataIKU();  

    this.myForm = new FormGroup({ 
      id: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
      nama: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
      nama_lain: new FormControl({value: '', disabled: this.disableTextbox}, []),  
      dinas: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
      logo: new FormControl({value: '/api/uploads/default-logo.png', disabled: this.disableTextbox}, []),  
      deskripsi: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]),    
      url_instagram: new FormControl({value: '', disabled: this.disableTextbox}, []),   
      url_youtube: new FormControl({value: '', disabled: this.disableTextbox}, []),   
      url_facebook: new FormControl({value: '', disabled: this.disableTextbox}, []),   
      url_twitter: new FormControl({value: '', disabled: this.disableTextbox}, []),   
      dampak_pendek: new FormArray([this.createDampakPendek()]),   
      dampak_menengah: new FormArray([this.createDampakMenengah()]),   
      dampak_panjang: new FormArray([this.createDampakPanjang()]),    
      iku: new FormArray([this.createIKU()]),   
      is_deleted: new FormControl(),   
      pic_asn: new FormArray([this.createPICASN()]), 
      pic_non_asn: new FormArray([this.createPICNONASN()]), 
      tajj: new FormArray([this.createTAJJ()]), 
      fasilitator: new FormArray([this.createFasilitator()]), 
      mitra: new FormArray([this.createMitra()]),  
      problem: new FormArray([this.createProblem()]), 
      goal: new FormArray([this.createGoal()]),  
    });

  }


  createPICASN(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
    });
  }
  createPICNONASN(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, []), 
    });
  }
  createTAJJ(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, []), 
    });
  }
  createFasilitator(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, []), 
    });
  }
  createMitra(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, []), 
    });
  } 
  createProblem(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
    });
  }
  createGoal(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: this.disableTextbox}, [Validators.required]), 
    });
  }
  createDampakPendek(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createDampakMenengah(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createDampakPanjang(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createIKU(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/'+ id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataProgram = result_msg;
          // console.log(this.dataProgram);
          // set data
          this.setForm(this.dataProgram);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }
  
  getDataDinas() {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?is_deleted=false&_sort=kode_skpd:ASC', json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataDinas = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }
 
  getDataIKU() {

    const json_iku = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_iku + '?is_deleted=false&_sort=nama:asc', json_iku).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIKU = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIKU = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIKU = null;
        return null;
      }
    );
  }
 
  setForm(data){ 
 
    for (let i = 0; i < data.pic_asn.arr.length-1; i++) {
      this.addFieldValuePICASN();
    }
    for (let i = 0; i < data.pic_non_asn.arr.length-1; i++) {
      this.addFieldValuePICNONASN();
    }
    for (let i = 0; i < data.tajj.arr.length-1; i++) {
      this.addFieldValueTAJJ();
    }
    for (let i = 0; i < data.fasilitator.arr.length-1; i++) {
      this.addFieldValueFasilitator();
    }
    for (let i = 0; i < data.mitra.arr.length-1; i++) {
      this.addFieldValueMitra();
    }
    for (let i = 0; i < data.problem.arr.length-1; i++) {
      this.addFieldValueProblem();
    }
    for (let i = 0; i < data.goal.arr.length-1; i++) {
      this.addFieldValueGoal();
    }
    for (let i = 0; i < data.dampak_pendek.arr.length-1; i++) {
      this.addFieldValueDampakPendek();
    }
    for (let i = 0; i < data.dampak_menengah.arr.length-1; i++) {
      this.addFieldValueDampakMenengah();
    }
    for (let i = 0; i < data.dampak_panjang.arr.length-1; i++) {
      this.addFieldValueDampakPanjang();
    }
    // let temp_iku = [];
    for (let i = 0; i < data.iku.arr.length-1; i++) {
      this.addFieldValueIKU(); 
      // if( Object.keys(data.iku.arr[i]).length > 1){
      //   temp_iku[i] = data.iku.arr[i]['value'];
      // }else{
      //   temp_iku[i] = '0'
      // }
    }
    // this.myForm.patchValue({
    //   iku: temp_iku
    // }); 

    this.myForm.patchValue({
      id: data.id,
      nama: data.nama,
      nama_lain: data.nama_lain,
      dinas: data.dinas.id,
      logo: data.logo,
      deskripsi: data.deskripsi,
      dampak_pendek: data.dampak_pendek.arr,
      dampak_menengah: data.dampak_menengah.arr,
      dampak_panjang: data.dampak_panjang.arr, 
      iku: data.iku.arr, 
      url_instagram: data.url_instagram,
      url_youtube: data.url_youtube,
      url_facebook: data.url_facebook,
      url_twitter: data.url_twitter,
      is_deleted: data.is_deleted,
      pic_asn: data.pic_asn.arr,
      pic_non_asn: data.pic_non_asn.arr,
      tajj: data.tajj.arr,
      fasilitator: data.fasilitator.arr,
      mitra: data.mitra.arr,
      problem: data.problem.arr,
      goal: data.goal.arr, 
    });

    

    let dataUser = JSON.parse(this.session.getData());
    console.log(dataUser);
    if(dataUser['role']['type'] === 'dinas'){ 
      this.myForm.patchValue({
        dinas: dataUser['dinas'],
      });

      this.myForm.controls.dinas.disable(); 
    }

    this.logoPath = data.logo;
  }
  

  onUpload(event) {
    let fileList: FileList = event.target.files;
    // console.log(event.target.files[0]); // outputs the first file
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData();
      // console.log(file);
      // console.log(file.name);
      formData.append('files', file, file.name);
      // console.log(formData);
      this.loading = true;

      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            // console.log(result_msg);

            this.myForm.patchValue({ 
              logo: "/api" + result_msg[0]['url'], 
            });
            this.logoPath = "/api" + result_msg[0]['url'];
            this.loading = false; 

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );

    }
  }

  onSubmit() {
    this.submitted = true;
    let val_program = this.myForm.value;  

    val_program['pic_asn'] = {
      'arr' : val_program['pic_asn'] 
    };
    val_program['pic_non_asn'] = {
      'arr' : val_program['pic_non_asn'] 
    };
    val_program['tajj'] = {
      'arr' : val_program['tajj'] 
    };
    val_program['fasilitator'] = {
      'arr' : val_program['fasilitator'] 
    };
    val_program['mitra'] = {
      'arr' : val_program['mitra'] 
    };
    val_program['problem'] = {
      'arr' : val_program['problem'] 
    };
    val_program['goal'] = {
      'arr' : val_program['goal'] 
    }; 
    val_program['dampak_pendek'] = {
      'arr' : val_program['dampak_pendek'] 
    }; 
    val_program['dampak_menengah'] = {
      'arr' : val_program['dampak_menengah'] 
    }; 
    val_program['dampak_panjang'] = {
      'arr' : val_program['dampak_panjang'] 
    }; 
    val_program['iku'] = {
      'arr' : val_program['iku'] 
    }; 
    val_program['is_deleted'] = false; 
  
    console.log(val_program); 
     
    this.loading = true;
    let json_program = JSON.stringify(val_program); 
    this.httpRequest.httpPut(this.url_program + '/' + val_program['id'], json_program).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          // console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
    
  addFieldValuePICASN() {
    this.pic_asn = this.myForm.get('pic_asn') as FormArray;
    this.pic_asn.push(this.createPICASN());
    return false;
  }

  addFieldValuePICNONASN() {
    this.pic_non_asn = this.myForm.get('pic_non_asn') as FormArray;
    this.pic_non_asn.push(this.createPICNONASN());
    return false;
  }
  

  addFieldValueTAJJ() {
    this.tajj = this.myForm.get('tajj') as FormArray;
    this.tajj.push(this.createTAJJ());
    return false;
  }
  

  addFieldValueFasilitator() {
    this.fasilitator = this.myForm.get('fasilitator') as FormArray;
    this.fasilitator.push(this.createFasilitator());
    return false;
  }
  

  addFieldValueMitra() {
    this.mitra = this.myForm.get('mitra') as FormArray;
    this.mitra.push(this.createMitra());
    return false;
  }
     
  addFieldValueProblem() {
    this.problem = this.myForm.get('problem') as FormArray;
    this.problem.push(this.createProblem());
    return false;
  }

  addFieldValueGoal() {
    this.goal = this.myForm.get('goal') as FormArray;
    this.goal.push(this.createGoal());
    return false;
  }
  
  addFieldValueDampakPendek() {
    this.dampak_pendek = this.myForm.get('dampak_pendek') as FormArray;
    this.dampak_pendek.push(this.createDampakPendek());
    return false;
  }
  addFieldValueDampakMenengah() {
    this.dampak_menengah = this.myForm.get('dampak_menengah') as FormArray;
    this.dampak_menengah.push(this.createDampakMenengah());
    return false;
  }
  addFieldValueDampakPanjang() {
    this.dampak_panjang = this.myForm.get('dampak_panjang') as FormArray;
    this.dampak_panjang.push(this.createDampakPanjang());
    return false;
  }
  addFieldValueIKU() {
    this.iku = this.myForm.get('iku') as FormArray;
    this.iku.push(this.createIKU());
    return false;
  }

  removeFieldValuePICASN(i) {
    if(this.pic_asn){
      this.pic_asn.removeAt(i);
    }
    return false;
  }
  removeFieldValuePICNONASN(i) {
    if(this.pic_non_asn){
      this.pic_non_asn.removeAt(i);
    }
    return false;
  }
  removeFieldValueTAJJ(i) {
    if(this.tajj){
      this.tajj.removeAt(i);
    }
    return false;
  }
  removeFieldValueFasilitator(i) {
    if(this.fasilitator){
      this.fasilitator.removeAt(i);
    }
    return false;
  }
  removeFieldValueMitra(i) {
    if(this.mitra){
      this.mitra.removeAt(i);
    }
    return false;
  }
  removeFieldValueProblem(i) {
    if(this.problem){
      this.problem.removeAt(i);
    }
    return false;
  }
  removeFieldValueGoal(i) {
    if(this.goal){
      this.goal.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakPendek(i) {
    if(this.dampak_pendek){
      this.dampak_pendek.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakMenengah(i) {
    if(this.dampak_menengah){
      this.dampak_menengah.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakPanjang(i) {
    if(this.dampak_panjang){
      this.dampak_panjang.removeAt(i);
    }
    return false;
  }
  removeFieldValueIKU(i) {
    if(this.iku){
      this.iku.removeAt(i);
    }
    return false;
  }
 
  
  showBack(){
    this._location.back();
  }

  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Data berhasil diupdate.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/program/list'])
    });
    
  } 

}
