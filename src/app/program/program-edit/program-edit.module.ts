import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgramEditComponent} from './program-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const ProgramEditRoutes: Routes = [{
    path: '',
    component: ProgramEditComponent,
    data: {
        breadcrumb: "Edit Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgramEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [ProgramEditComponent]
})

export class ProgramEditModule {}
