import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgramAddComponent} from './program-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const ProgramAddRoutes: Routes = [{
    path: '',
    component: ProgramAddComponent,
    data: {
        breadcrumb: "Tambah Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgramAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [ProgramAddComponent]
})

export class ProgramAddModule {}
