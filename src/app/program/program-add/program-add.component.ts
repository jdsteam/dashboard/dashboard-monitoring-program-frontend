import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-program-add',
  templateUrl: './program-add.component.html',
  styleUrls: [ './program-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class ProgramAddComponent implements OnInit {

  public loading = false; 
  private url_upload = '/api/upload'; 
  private url_dinas = '/api/dinas'; 
  private url_program = '/api/programs'; 
  private url_iku = '/api/ikus'; 
  
  disableTextbox:Boolean = true; 
  dataDinas = [];  
  dataIKU = [];  
  
  isreadonly = false;
  myForm: FormGroup; 
  pic_asn: FormArray;
  pic_non_asn: FormArray;
  tajj: FormArray;
  fasilitator: FormArray;
  mitra: FormArray; 
  problem: FormArray;
  goal: FormArray;
  dampak_pendek: FormArray;
  dampak_menengah: FormArray;
  dampak_panjang: FormArray;
  iku: FormArray;

  submitted: boolean;
  logoPath = "";
  logoNull = "/api/uploads/default-logo.png"; 


  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }

  ngOnInit() {

    if(!this.session.checkAccess('program','add')){
      this.router.navigate(['/error/403']);
    }
    this.getDataDinas(); 
    this.getDataIKU(); 
  
    this.myForm = new FormGroup({ 
      nama: new FormControl({value: '', disabled: false}, [Validators.required]), 
      nama_lain: new FormControl({value: '', disabled: false}, []),  
      dinas: new FormControl({value: '', disabled: false}, [Validators.required]), 
      logo: new FormControl({value: '/api/uploads/default-logo.png', disabled: false}, []),  
      deskripsi: new FormControl({value: '', disabled: false}, [Validators.required]),   
      url_instagram: new FormControl({value: '', disabled: false}, []),   
      url_youtube: new FormControl({value: '', disabled: false}, []),   
      url_facebook: new FormControl({value: '', disabled: false}, []),   
      url_twitter: new FormControl({value: '', disabled: false}, []),  
      dampak_pendek: new FormArray([this.createDampakPendek()]),   
      dampak_menengah: new FormArray([this.createDampakMenengah()]),   
      dampak_panjang: new FormArray([this.createDampakPanjang()]),    
      iku: new FormArray([this.createIKU()]),   
      is_deleted: new FormControl(),   
      pic_asn: new FormArray([this.createPICASN()]), 
      pic_non_asn: new FormArray([this.createPICNONASN()]), 
      tajj: new FormArray([this.createTAJJ()]), 
      fasilitator: new FormArray([this.createFasilitator()]), 
      mitra: new FormArray([this.createMitra()]),  
      problem: new FormArray([this.createProblem()]), 
      goal: new FormArray([this.createGoal()]),  
    });
 
    let dataUser = JSON.parse(this.session.getData());
    console.log(dataUser);
    if(dataUser['role']['type'] === 'dinas'){ 
      this.myForm.patchValue({
        dinas: dataUser['dinas'],
      });

      this.myForm.controls.dinas.setValidators([]); 
      this.myForm.controls.dinas.updateValueAndValidity();
      this.isreadonly = true;
      // this.myForm.controls.dinas.disable(); 
    }

    this.createPICASN(); 
    this.createDampakPendek(); 
    this.createDampakMenengah(); 
    this.createDampakPanjang(); 
    this.createIKU(); 
  }

  createPICASN(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, [Validators.required]), 
    });
  }
  createPICNONASN(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createTAJJ(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createFasilitator(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  }
  createMitra(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, []), 
    });
  } 
  createProblem(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, [Validators.required]), 
    });
  }
  createGoal(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}, [Validators.required]), 
    });
  }
  createDampakPendek(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createDampakMenengah(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createDampakPanjang(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }
  createIKU(): FormGroup {
    return new FormGroup({
      value: new FormControl({value: '', disabled: false}), 
    });
  }

  getDataDinas() {

    const json_dinas = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_dinas + '?is_deleted=false&_sort=kode_skpd:ASC', json_dinas).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataDinas = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataDinas = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataDinas = null;
        return null;
      }
    );
  }

  getDataIKU() {

    const json_iku = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_iku + '?is_deleted=false&_sort=nama:asc', json_iku).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIKU = result_msg;
          // console.log(this.dataDinas);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIKU = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIKU = null;
        return null;
      }
    );
  }

   

  onUpload(event) {
    let fileList: FileList = event.target.files; 
    if(fileList.length > 0) {
      let file: File = fileList[0];
      let formData:FormData = new FormData(); 
      formData.append('files', file, file.name); 
      this.loading = true;

      this.httpRequest.httpUpload(this.url_upload, formData).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            // console.log(result_msg);

            this.myForm.patchValue({ 
              logo: "/api" + result_msg[0]['url'], 
            });
            this.logoPath = "/api" + result_msg[0]['url'];
            this.loading = false; 

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );

    }
  }
 
  
  onSubmit() {
    this.submitted = true;
    let val_program = this.myForm.value;  

    let dataUser = JSON.parse(this.session.getData());
    // console.log(dataUser);
    if(dataUser['role']['type'] === 'dinas'){  
      val_program['dinas'] = dataUser['dinas']
    }
    val_program['pic_asn'] = {
      'arr' : val_program['pic_asn'] 
    };
    val_program['pic_non_asn'] = {
      'arr' : val_program['pic_non_asn'] 
    };
    val_program['tajj'] = {
      'arr' : val_program['tajj'] 
    };
    val_program['fasilitator'] = {
      'arr' : val_program['fasilitator'] 
    };
    val_program['mitra'] = {
      'arr' : val_program['mitra'] 
    };
    val_program['problem'] = {
      'arr' : val_program['problem'] 
    };
    val_program['goal'] = {
      'arr' : val_program['goal'] 
    }; 
    val_program['dampak_pendek'] = {
      'arr' : val_program['dampak_pendek'] 
    }; 
    val_program['dampak_menengah'] = {
      'arr' : val_program['dampak_menengah'] 
    }; 
    val_program['dampak_panjang'] = {
      'arr' : val_program['dampak_panjang'] 
    }; 
    val_program['iku'] = {
      'arr' : val_program['iku'] 
    }; 
    val_program['is_deleted'] = false; 
  
    console.log(val_program); 
     
    this.loading = true;
    let json_program = JSON.stringify(val_program); 
    this.httpRequest.httpPost(this.url_program, json_program).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 


  addFieldValuePICASN() {
    this.pic_asn = this.myForm.get('pic_asn') as FormArray;
    this.pic_asn.push(this.createPICASN());
    return false;
  }

  addFieldValuePICNONASN() {
    this.pic_non_asn = this.myForm.get('pic_non_asn') as FormArray;
    this.pic_non_asn.push(this.createPICNONASN());
    return false;
  }
  

  addFieldValueTAJJ() {
    this.tajj = this.myForm.get('tajj') as FormArray;
    this.tajj.push(this.createTAJJ());
    return false;
  }
  

  addFieldValueFasilitator() {
    this.fasilitator = this.myForm.get('fasilitator') as FormArray;
    this.fasilitator.push(this.createFasilitator());
    return false;
  }
  

  addFieldValueMitra() {
    this.mitra = this.myForm.get('mitra') as FormArray;
    this.mitra.push(this.createMitra());
    return false;
  }
     
  addFieldValueProblem() {
    this.problem = this.myForm.get('problem') as FormArray;
    this.problem.push(this.createProblem());
    return false;
  }

  addFieldValueGoal() {
    this.goal = this.myForm.get('goal') as FormArray;
    this.goal.push(this.createGoal());
    return false;
  }
  
  addFieldValueDampakPendek() {
    this.dampak_pendek = this.myForm.get('dampak_pendek') as FormArray;
    this.dampak_pendek.push(this.createDampakPendek());
    return false;
  }
  addFieldValueDampakMenengah() {
    this.dampak_menengah = this.myForm.get('dampak_menengah') as FormArray;
    this.dampak_menengah.push(this.createDampakMenengah());
    return false;
  }
  addFieldValueDampakPanjang() {
    this.dampak_panjang = this.myForm.get('dampak_panjang') as FormArray;
    this.dampak_panjang.push(this.createDampakPanjang());
    return false;
  }
  addFieldValueIKU() {
    this.iku = this.myForm.get('iku') as FormArray;
    this.iku.push(this.createIKU());
    return false;
  }
  
  removeFieldValuePICASN(i) {
    if(this.pic_asn){
      this.pic_asn.removeAt(i);
    }
    return false;
  }
  removeFieldValuePICNONASN(i) {
    if(this.pic_non_asn){
      this.pic_non_asn.removeAt(i);
    }
    return false;
  }
  removeFieldValueTAJJ(i) {
    if(this.tajj){
      this.tajj.removeAt(i);
    }
    return false;
  }
  removeFieldValueFasilitator(i) {
    if(this.fasilitator){
      this.fasilitator.removeAt(i);
    }
    return false;
  }
  removeFieldValueMitra(i) {
    if(this.mitra){
      this.mitra.removeAt(i);
    }
    return false;
  }
  removeFieldValueProblem(i) {
    if(this.problem){
      this.problem.removeAt(i);
    }
    return false;
  }
  removeFieldValueGoal(i) {
    if(this.goal){
      this.goal.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakPendek(i) {
    if(this.dampak_pendek){
      this.dampak_pendek.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakMenengah(i) {
    if(this.dampak_menengah){
      this.dampak_menengah.removeAt(i);
    }
    return false;
  }
  removeFieldValueDampakPanjang(i) {
    if(this.dampak_panjang){
      this.dampak_panjang.removeAt(i);
    }
    return false;
  }
  removeFieldValueIKU(i) {
    if(this.iku){
      this.iku.removeAt(i);
    }
    return false;
  }
  
  showBack(){
    this._location.back();
  }

  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Kegiatan '+ nama +' berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.router.navigate(['/program'])
    });
    
  }

}
