import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgramDetailComponent} from './program-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const ProgramDetailRoutes: Routes = [{
    path: '',
    component: ProgramDetailComponent,
    data: {
        breadcrumb: "Detail Kegiatan"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgramDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [ProgramDetailComponent]
})

export class ProgramDetailModule {}
