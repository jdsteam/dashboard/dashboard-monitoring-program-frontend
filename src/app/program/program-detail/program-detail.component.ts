import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { TouchSequence } from 'selenium-webdriver';


@Component({
  selector: 'app-program-detail',
  templateUrl: './program-detail.component.html',
  styleUrls: [ './program-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class ProgramDetailComponent implements OnInit {

  public loading = false; 
  public finish_loading = false; 
  private url_upload = '/api/upload';  
  private url_program = '/api/programs';
  private url_anggaran = '/api/periodeanggarans';
  private url_indikator = '/api/indikators'; 
  private url_lapprogress = '/api/lapprogresses'; 
  private url_lapanggaran = '/api/lapanggarans'; 

  private url_periodeanggaran = '/api/periodeanggarans'; 
  private url_kegiatan = '/api/kegiatans'; 

  private url_periodeprogress = '/api/periodeprogresses'; 
  private url_anggarans = '/api/anggarans'; 

  private url_iku = '/api/ikus';  

  disableTextbox:Boolean = true;  
  dataProgram = []; 
  dataAnggaran = []; 
  dataIndikator = [];  
  dataIKU = [];   
   
  is_detail_indikator = false;
  is_detail_periodeanggaran = false;
  is_add_indikator = false;
  is_add_periodeanggaran = false;
  is_edit_indikator = false;
  is_edit_periodeanggaran = false;
  is_delete_indikator = false;
  is_delete_periodeanggaran = false;

  myForm: FormGroup; 
  pic_asn: FormArray;
  pic_non_asn: FormArray;
  tajj: FormArray;
  fasilitator: FormArray;
  mitra: FormArray; 
  problem: FormArray;
  goal: FormArray;

  program_id : string = '0';
  indikator_id : string = '0';
  periode_id : string = '0';
  periodeprogress_id : string = '0';
  lapprogress_id : string = '0';

  submitted: boolean;
  logoPath = "";
  logoNull = "/api/uploads/default-logo.png";   


  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }

  ngOnInit() {
 
    if(!this.session.checkAccess('program','detail')){
      this.router.navigate(['/error/403']);
    }

    this.is_detail_indikator = this.session.checkAccess('indikator','detail');
    this.is_detail_periodeanggaran = this.session.checkAccess('periodeanggaran','detail');
    this.is_add_indikator = this.session.checkAccess('indikator','add');
    this.is_add_periodeanggaran = this.session.checkAccess('periodeanggaran','add');
    this.is_edit_indikator = this.session.checkAccess('indikator','edit');
    this.is_edit_periodeanggaran = this.session.checkAccess('periodeanggaran','edit');
    this.is_delete_indikator = this.session.checkAccess('indikator','delete');
    this.is_delete_periodeanggaran = this.session.checkAccess('periodeanggaran','delete');

    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = id; 

    this.getDataProgram(id);
    this.getDataAnggaran(id); 
    this.getDataIndikator(id); 
    this.getDataLapProgress(id); 

    // this.formPICASN = new FormGroup({
    //   value: new FormControl({value: '', disabled: false}, [])
    // })


    this.myForm = new FormGroup({  
    });

  }
 

  getDataProgram(id) {

    const json_program = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_program + '/'+ id, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.finish_loading = true;
          this.dataProgram = result_msg;
          
          this.getDataIKU();  
          // set data 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataProgram = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataProgram = null;
        return null;
      }
    );
  }

  getDataAnggaran(id) {

    const json_anggaran = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_anggaran + '?program='+ id + '&is_deleted=false&_sort=id:asc', json_anggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataAnggaran = result_msg;
          // console.log(this.dataProgram); 
          this.countPencapaianAnggaran(result_msg);
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataAnggaran = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataAnggaran = null;
        return null;
      }
    );
  }

  getDataIndikator(id) {

    const json_indikator = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_indikator + '?program='+ id  + '&is_deleted=false&_sort=id:asc', json_indikator).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIndikator = result_msg;
          this.countPencapaianIndikator(result_msg);
          // console.log(this.dataProgram); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIndikator = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIndikator = null;
        return null;
      }
    );
  }

  getDataIKU() {

    const json_iku = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_iku + '?is_deleted=false&_sort=nama:asc', json_iku).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.dataIKU = result_msg;
          // console.log(this.dataDinas);

          let i = 0;
            
            this.dataProgram['iku']['arr'].forEach(element2 => { 
              // console.log(element2)
              let result = this.dataIKU.filter(obj => {
                return String(obj.id) === element2['value']
              })
              try {
                this.dataProgram['iku']['arr'][i]['nama'] = result[0]['nama']; 
              } catch (error) {
                
              }
              i++;
            }); 

          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.dataIKU = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.dataIKU = null;
        return null;
      }
    );
  }

  getDataLapProgress(id) {

    const json_lapprogress = {};
    this.loading = true;
    this.httpRequest.httpGet(this.url_lapprogress + '?program='+ id  + '&is_deleted=false&_sort=id:asc', json_lapprogress).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.loading = false;
          this.getDataLapProgress = result_msg;
          // console.log(this.dataProgram); 
          return result_msg;

        } catch (error) {
          this.errorMessage.openErrorSwal('Something wrong.');
          console.log(error);
          this.loading = false;
          this.getDataLapProgress = null;
          return null;
        }
      },
      error => {
        console.log(error);
        this.loading = false;
        this.getDataLapProgress = null;
        return null;
      }
    );
  }
    
  countPencapaianIndikator(row){

    row.forEach(element => {
      
      let i = 0;
      const json_lapprogress = {};
      this.loading = true; 
      // console.log(query);

      this.httpRequest.httpGet(this.url_lapprogress+ '/?indikator=' + element.id + '&is_deleted=false' , json_lapprogress).subscribe(
        result => {
          try {
            // console.log(result);
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;
            temp.forEach(element => {
              // console.log(element); 
              kuantitas = kuantitas + element.kuantitas;
              target = element.indikator.target_kuantitatif;
            });
            pencapaian = (kuantitas / target) * 100;
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            // console.log('-----------');
            // console.log(i);
            // console.log(element.id);
            // console.log(kuantitas);
            // console.log(target);
            // console.log(pencapaian)
            // console.log('-----------'); 
            // console.log(row[i]);
            let index = this.dataIndikator.indexOf(element);
            this.dataIndikator[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataIndikator[index]['pencapaian_persen'] = pencapaian;

            this.loading = false;
            i = i + 1; 

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
      
    });
  }
 

  
  countPencapaianAnggaran(row){

    row.forEach(element => {
      
      let i = 0;
      const json_lapanggaran = {};
      this.loading = true; 
      // console.log(query);

      this.httpRequest.httpGet(this.url_lapanggaran+ '/?periodeanggaran=' + element.id + '&is_deleted=false' , json_lapanggaran).subscribe(
        result => {
          try {
            // console.log(result);
            const result_msg = JSON.parse(result._body);
            let temp = result_msg;   
            let kuantitas = 0;
            let target = 0;
            let pencapaian = 0;
            temp.forEach(element => {
              // console.log(element); 
              kuantitas = kuantitas + parseInt(element.serapan);
              target = parseInt(element.periodeanggaran.apbd) + parseInt(element.periodeanggaran.nonapbd);
            });
            pencapaian = (kuantitas / target) * 100;
            pencapaian = +pencapaian.toFixed(2);
            pencapaian = pencapaian || 0;
            // console.log('-----------');
            // console.log(i);
            // console.log(element.id);
            // console.log(kuantitas);
            // console.log(target);
            // console.log(pencapaian)
            // console.log('-----------'); 
            // console.log(row[i]);
            let index = this.dataAnggaran.indexOf(element);
            this.dataAnggaran[index]['total_anggaran'] = Number(this.dataAnggaran[index]['apbd']) + Number(this.dataAnggaran[index]['nonapbd']);
            this.dataAnggaran[index]['pencapaian_kuantitas'] = kuantitas;
            this.dataAnggaran[index]['pencapaian_persen'] = pencapaian;

            this.loading = false;
            i = i + 1; 

          } catch (error) {
            this.loading = false;
            this.errorMessage.openErrorSwal('Something wrong.');
          }
        },
        error => {
          console.log(error);
          this.loading = false;
        }
      );
      
    });
  }
 

  onDeleteIndikator(row) {
 
    // let json_indikator = row;
    let json_indikator = {};
    json_indikator['is_deleted'] = true;
    // console.log(json_indikator);

    this.loading = true;
    this.httpRequest.httpPut(this.url_indikator + '/' + row['id'], json_indikator).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          // get periode progress by indikator
          this.httpRequest.httpGet(this.url_periodeprogress+ '?indikator=' + row['id'], json_indikator).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body); 
                result_msg.forEach(element => {
          
                  // get lap progress by periode progress
                  this.httpRequest.httpGet(this.url_lapprogress + '?periodeprogress=' + element['id'], json_indikator).subscribe(
                    result => {
                      try {
                        const result_msg = JSON.parse(result._body);
                        result_msg.forEach(element => {

                          // delete lap progress
                          this.httpRequest.httpPut(this.url_lapprogress + '/' + element['id'], json_indikator).subscribe();
                          
                        });
                      } catch (error) {}
                    }
                  );

                   // delete periode progress
                  this.httpRequest.httpPut(this.url_periodeprogress + '/' + element['id'], json_indikator).subscribe();
                
                });

              }catch(error){}
            }
          );

          this.loading = false;
          this.showSuccessIndikator(row['nama'], result_msg['program']['id']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  
  onDeleteAnggaran(row) {
 
    // let json_anggaran = row;
    let json_anggaran = {};
    json_anggaran['is_deleted'] = true;
    // console.log(json_anggaran);

    this.loading = true;
    this.httpRequest.httpPut(this.url_anggaran + '/' + row['id'], json_anggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);


          // get kegiatan anggaran by periodeanggaran
          this.httpRequest.httpGet(this.url_kegiatan+ '?periodeanggaran=' + row['id'], json_anggaran).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body); 
                result_msg.forEach(element => {
          
                  // get lap anggaran by kegiatan
                  this.httpRequest.httpGet(this.url_lapanggaran + '?kegiatan=' + element['id'], json_anggaran).subscribe(
                    result => {
                      try {
                        const result_msg = JSON.parse(result._body);
                        result_msg.forEach(element => {

                          // delete lap progress
                          this.httpRequest.httpPut(this.url_lapanggaran + '/' + element['id'], json_anggaran).subscribe();
                          
                        });
                      } catch (error) {}
                    }
                  );

                   // delete kegiatan
                  this.httpRequest.httpPut(this.url_kegiatan + '/' + element['id'], json_anggaran).subscribe();
                
                });

              }catch(error){}
            }
          );

          this.loading = false;
          this.showSuccessAnggaran(row['nama'], result_msg['program']['id']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 
 
 
  showBack(){
    this._location.back();
  }
   
  
  showIndikatorAdd(row){  
    this.router.navigate(['/program/', this.program_id, 'indikator', 'add'])
  }
  showIndikatorDetail(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', row['id'], 'detail'])
  }
  showIndikatorEdit(row){ 
    this.router.navigate(['/program/', this.program_id, 'indikator', row['id'], 'edit'])
  }
  showIndikatorDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteIndikator(row);
      }
    });
  } 
  showSuccessIndikator(nama, program){ 
    swal({
      title: 'Success',
      text: 'Data Indikator berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataIndikator = [];
      this.getDataIndikator(program); 
      // this.router.navigate(['/program/'+ program +'/detail'])
    });
    
  }


  showAnggaranAdd(row){  
    this.router.navigate(['/program/', this.program_id, 'anggaran', 'add'])
  }
  showAnggaranDetail(row){ 
    this.router.navigate(['/program/', this.program_id, 'anggaran', row['id'], 'detail'])
  }
  showAnggaranEdit(row){ 
    this.router.navigate(['/program/', this.program_id, 'anggaran', row['id'], 'edit'])
  }
  showAnggaranDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      console.log(result);
      if (result) {
        this.onDeleteAnggaran(row);
      }
    });
  } 
  showSuccessAnggaran(nama, program){ 
    swal({
      title: 'Success',
      text: 'Data Periode Anggaran berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.dataAnggaran = [];
      this.getDataAnggaran(program); 
      // this.router.navigate(['/program/'+ program +'/detail'])
    });
    
  }


}
