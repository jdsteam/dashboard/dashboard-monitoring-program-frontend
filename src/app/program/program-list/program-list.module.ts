import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ProgramListComponent} from './program-list.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const ProgramListRoutes: Routes = [{
    path: '',
    component: ProgramListComponent,
    data: {
        breadcrumb: "List Kegiatan",
        status: true
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProgramListRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [ProgramListComponent]
})

export class ProgramListModule {}
