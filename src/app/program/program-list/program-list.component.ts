import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import swal from 'sweetalert2';


@Component({
  selector: 'app-program-list',
  templateUrl: './program-list.component.html',
  styleUrls: [ './program-list.component.css',],
  animations: [fadeInOutTranslate]
})
export class ProgramListComponent implements OnInit {
  @ViewChild(DatatableComponent) table: DatatableComponent;

  public loading = false;
  private url_program = '/api/programs';
  private url_indikator = '/api/indikators';
  private url_periodeprogress = '/api/periodeprogresses';
  private url_lapprogress = '/api/lapprogresses';
  private url_periodeanggaran = '/api/periodeanggarans';
  private url_kegiatan = '/api/kegiatans';
  private url_lapanggaran = '/api/lapanggarans';
  loadingIndicator: Boolean = true;
  reorderable: Boolean = true;
  showDialog: Boolean = false;

  is_list = false;
  is_detail = false;
  is_add = false;
  is_edit = false;
  is_delete = false;
  is_statistik = false;

  rowsFilter = [];
  tempFilter = [];
  limit = 10;
  messages = {
    emptyMessage: 'Tidak ada data.',
    totalMessage: 'total'
  }
  
  public currentPageLimit: number = 10;
  public pageLimitOptions = [
    {value: 5},
    {value: 10},
    {value: 25},
    {value: 50},
    {value: 100},
  ];
  
  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private _location: Location) {
    this.getData((data) => {
      this.tempFilter = [...data];
      this.rowsFilter = data;
    });
  }

  ngOnInit() { 
    if(!this.session.checkAccess('program','list')){
      this.router.navigate(['/error/403']);
    }
 
    this.is_list = this.session.checkAccess('program','list');
    this.is_detail = this.session.checkAccess('program','detail');
    this.is_add = this.session.checkAccess('program','add');
    this.is_edit = this.session.checkAccess('program','edit');
    this.is_delete = this.session.checkAccess('program','delete');
    this.is_statistik = this.session.checkAccess('program','statistik');
  }

  onLimitChange(limit: any): void {
    this.changePageLimit(limit);
    this.table.limit = this.currentPageLimit;
    this.table.recalculate();
    setTimeout(() => {
      if (this.table.bodyComponent.temp.length <= 0) {
        // TODO[Dmitry Teplov] test with server-side paging.
        this.table.offset = Math.floor((this.table.rowCount - 1) / this.table.limit);
      }
    });
  }

  changePageLimit(limit: any): void {
    this.currentPageLimit = parseInt(limit, 10);
  }

  getData(cb) {

    const json_program = {};
    this.loading = true;

    const sess = JSON.parse(this.session.getData());
    // console.log(sess['dinas']);
 

    let query = "";
    if((sess['role']['type'] == 'dinas') || (sess['role']['type'] == 'pelapor')){
      query = "?is_deleted=false&dinas="+sess['dinas']+"&_sort=id";
    }else{
      query = "?is_deleted=false&_sort=id";
    }
    // console.log(query);

    this.httpRequest.httpGet(this.url_program + query, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          cb(result_msg);
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    const temp = this.tempFilter.filter(function(d) {
      return d.nama.toLowerCase().indexOf(val) !== -1 || 
      d.dinas.nama.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.rowsFilter = temp;
    this.table.offset = 0;
  }

  onDelete(row) {
 
    // let json_program = row;
    let json_program = {};
    json_program['is_deleted'] = true;
    // console.log(json_program);

    this.loading = true;
    this.httpRequest.httpPut(this.url_program + '/' + row['id'], json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          console.log(result_msg);

          // get indikator by program
          this.httpRequest.httpGet(this.url_indikator+ '?program=' + row['id'], json_program).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body); 
                result_msg.forEach(element => {

                  // get periode progress by indikator
                  this.httpRequest.httpGet(this.url_periodeprogress+ '?indikator=' + element['id'], json_program).subscribe(
                    result => {
                      try {
                        const result_msg = JSON.parse(result._body); 
                        result_msg.forEach(element => {
                  
                          // get lap progress by periode progress
                          this.httpRequest.httpGet(this.url_lapprogress + '?periodeprogress=' + element['id'], json_program).subscribe(
                            result => {
                              try {
                                const result_msg = JSON.parse(result._body);
                                result_msg.forEach(element => {

                                  // delete lap progress
                                  this.httpRequest.httpPut(this.url_lapprogress + '/' + element['id'], json_program).subscribe();
                                  
                                });
                              } catch (error) {}
                            }
                          );

                          // delete periode progress
                          this.httpRequest.httpPut(this.url_periodeprogress + '/' + element['id'], json_program).subscribe();
                        
                        });

                      }catch(error){}
                    }
                  );
                   
                  // delete indikator
                  this.httpRequest.httpPut(this.url_indikator + '/' + element['id'], json_program).subscribe();
                         
                });
              } catch (error) {}
            }
          );


          // get periode anggaran by program
          this.httpRequest.httpGet(this.url_periodeanggaran+ '?program=' + row['id'], json_program).subscribe(
            result => {
              try {
                const result_msg = JSON.parse(result._body); 
                result_msg.forEach(element => {

                  // get kegiatan by periode anggaran
                  this.httpRequest.httpGet(this.url_kegiatan+ '?periodeanggaran=' + element['id'], json_program).subscribe(
                    result => {
                      try {
                        const result_msg = JSON.parse(result._body); 
                        result_msg.forEach(element => {
                  
                          // get lap anggaran by periode progress
                          this.httpRequest.httpGet(this.url_lapanggaran + '?kegiatan=' + element['id'], json_program).subscribe(
                            result => {
                              try {
                                const result_msg = JSON.parse(result._body);
                                result_msg.forEach(element => {

                                  // delete lap anggaran
                                  this.httpRequest.httpPut(this.url_lapanggaran + '/' + element['id'], json_program).subscribe();
                                  
                                });
                              } catch (error) {}
                            }
                          );

                          // delete kegiatan
                          this.httpRequest.httpPut(this.url_kegiatan + '/' + element['id'], json_program).subscribe();
                        
                        });

                      }catch(error){}
                    }
                  );
                   
                  // delete periode anggaran
                  this.httpRequest.httpPut(this.url_periodeanggaran + '/' + element['id'], json_program).subscribe();
                        
                });
              } catch (error) {}
            }
          );
 

          this.loading = false;
          this.showSuccess(row['nama']);

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }


  showList(){
    this.router.navigate(['/program/list'])
  }

  showAdd(){
    this.router.navigate(['/program/add'])
  }

  showDetail(row){
    // console.log(row);
    this.router.navigate(['/program/', row.id, 'detail'])
  }

  showEdit(row){
    // console.log(row);
    this.router.navigate(['/program/', row.id, 'edit'])
  }

  showStatistic(row){
    // console.log(row);
    this.router.navigate(['/program/', row.id, 'statistik'])
  }

  
  showBack(){
    this._location.back();
  }

  showDelete(event, row) {
    swal({
      title: 'Konfirmasi!',
      text: "Yakin akan menghapus data ini?",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#2ecc71',
      cancelButtonColor: '#dc3545',
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak',
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      allowOutsideClick: false
    }).then((result) => {
      if (result) {
        this.onDelete(row);
      }
    });
  }
 
  
  showSuccess(nama){
    swal({
      title: 'Informasi',
      html: 'Kegiatan ' + nama + ' berhasil dihapus.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this.rowsFilter = [];
      this.tempFilter = [];
      this.getData((data) => {
        this.tempFilter = [...data];
        this.rowsFilter = data;
      });
    });
    
  }

}
