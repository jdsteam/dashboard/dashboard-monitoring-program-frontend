import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProgramRoutes } from './program.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProgramRoutes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: []
})

export class ProgramModule {}
