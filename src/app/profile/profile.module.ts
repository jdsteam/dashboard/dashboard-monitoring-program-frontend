import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ProfileComponent } from './profile.component';
import { ProfileRoutes } from './profile.routing';
import {SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ProfileRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [ProfileComponent]
})

export class ProfileModule {}
