import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../shared/service/converter.service';
import { HttpRequestService } from '../shared/service/http-request.service';
import { SessionService } from '../shared/service/session.service';
import { ErrorMessageService } from '../shared/service/error-message.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import  "../../assets/jq-vmap/jquery.vmap.js";
import "../../assets/jq-vmap/maps/jquery.vmap.world.js";
declare var $:any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: [
    './profile.component.css',
    "../../assets/jq-vmap/jqvmap.css"
  ],
})

export class ProfileComponent implements OnInit {

  public loading = false;
  private url_user = '/api/users/me'; 
  private url_program = '/api/programs'; 
  dataUser = [];  
  dataProgram = [];   

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router) {
  } 

  ngOnInit() { 

    if(!this.session.checkAccess('profile','list')){
      this.router.navigate(['/error/403']);
    }
    this.dataUser = JSON.parse(this.session.getData());
    this.getDataUser();
    this.getDataProgram();
    console.log(this.dataUser);
  }
 
  getDataUser() {
    const json_program = {};
    this.loading = true;
  
    this.httpRequest.httpGet(this.url_user , json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataUser = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataProgram() {

    const json_program = {};
    this.loading = true;

    const sess = JSON.parse(this.session.getData());
    // console.log(sess['dinas']); 
    let query = "";
    if((sess['role']['type'] == 'dinas') || (sess['role']['type'] == 'pelapor')){
      query = "?is_deleted=false&dinas="+sess['dinas']+"&_sort=id";
    }else{
      query = "?is_deleted=false&_sort=id";
    }
    // console.log(query);

    this.httpRequest.httpGet(this.url_program + query, json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataProgram = result_msg;
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
}
