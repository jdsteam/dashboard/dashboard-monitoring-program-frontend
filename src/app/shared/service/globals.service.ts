import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';

@Injectable()
export class GlobalsService {

  private url_notification = '/api/notifications';

  message = [{
    jumlah: 0,
    data: []
  }];

  private messageSource = new BehaviorSubject(this.message);
  currentMessage = this.messageSource.asObservable();

  constructor(
    private httpRequest: HttpRequestService,
    private session: SessionService,
  ) { }

  changeMessage() {
    const sess = JSON.parse(this.session.getData());

    if (sess !== null) {

      const filter_j = '?type=broadcast&penerima=' + sess['id'] + '&is_read=false';
      const json_message_j = {};
      this.httpRequest.httpGet(this.url_notification + '/count' + filter_j, json_message_j).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.message[0].jumlah = result_msg;
          } catch (error) {
            console.log(error);
            this.message[0].jumlah = 0;
          }
        },
        error => {
          console.log(error);
          this.message[0].jumlah = 0;
        });

      const filter_d = '?type=broadcast&penerima=' + sess['id'] + '&_limit=5&_sort=created_at:DESC';
      const json_message_d = {};
      this.httpRequest.httpGet(this.url_notification + filter_d, json_message_d).subscribe(
        result => {
          try {
            const result_msg = JSON.parse(result._body);
            this.message[0].data = result_msg;
          } catch (error) {
            console.log(error);
            this.message[0].data = null;
          }
        },
        error => {
          console.log(error);
          this.message[0].data = null;
        });

      this.messageSource.next(this.message);
    }

  }
}
