import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { MenuItemsAdmin } from '../../shared/menu-items/menu-items-admin';
import { MenuItemsDinas } from '../../shared/menu-items/menu-items-dinas';
import { MenuItemsPelapor } from '../../shared/menu-items/menu-items-pelapor';
import { MenuItemsVerifikator } from '../../shared/menu-items/menu-items-verifikator';
import { MenuItemsPemantau } from '../../shared/menu-items/menu-items-pemantau';
import { AccessRole } from '../../shared/menu-items/access-role';


@Injectable()
export class SessionService {


  constructor(
    private accessRole: AccessRole,
    private menuItems: MenuItems,
    private menuItemsAdmin: MenuItemsAdmin,
    private menuItemsDinas: MenuItemsDinas,
    private menuItemsPelapor: MenuItemsPelapor,
    private menuItemsVerifikator: MenuItemsVerifikator,
    private menuItemsPemantau: MenuItemsPemantau,
  ) {
  }

  

  setToken(token: string) {
    localStorage.setItem('monitoring-proyek-token', token);
  }

  getToken() {
    return localStorage.getItem('monitoring-proyek-token');
  }

  setData(data: string) {
    localStorage.setItem('monitoring-proyek-data', data);
  }

  getData() {
    return localStorage.getItem('monitoring-proyek-data');
  }  
  
  setMenu(menu: string) {
    localStorage.setItem('monitoring-proyek-menu', menu);
  }

  getMenu() {
    return JSON.parse(localStorage.getItem('monitoring-proyek-menu'));
  }

  getRole() {
    const data = this.getData();
    const result_msg = JSON.parse(data);
    try {
      let role = result_msg['role']['type'];
      role = role.toString().toLowerCase();
      role = role.replace('_', '-');
      return role;
    } catch (error) {
      return null;
    }
  }

  getTokenExpired = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    try {
      const date = jwtHelper.getTokenExpirationDate(token);
      return date;
    } catch (error) {
      return null;
    }
  };

  isLoggedIn = function() {
    const token = this.getToken();
    const jwtHelper = new JwtHelperService();

    if (token) {
      const payload = jwtHelper.decodeToken(token);
      const date = jwtHelper.getTokenExpirationDate(token);
      const expired = jwtHelper.isTokenExpired(token);
      // console.log(payload);
      // console.log(date);
      // console.log(expired);

      if (expired) {
        return false;
      }else {
        return true;
      }

    }else {
      return false;
    }
  };

  checkMenu(route1, route2) {
    
    if(route1 === ''){
      route1 = 'dashboard';
    }if(route1 === 'profile'){
      route1 = 'dashboard';
    }

    let role = this.getRole();
    let role_access = [];
    let role_access_find = {};
    let role_access_can = false;

    if(role === 'admin'){
      role_access = this.menuItemsAdmin.getAll();
    }else if(role === 'dinas'){
      role_access = this.menuItemsDinas.getAll();
    }else if(role === 'pelapor'){
      role_access = this.menuItemsPelapor.getAll();
    }else if(role === 'verifikator'){
      role_access = this.menuItemsVerifikator.getAll();
    }else if(role === 'pemantau'){
      role_access = this.menuItemsPemantau.getAll();
    }

    try {
      // console.log(role_access);
      for (var i=0; i < role_access[0]['main'].length; i++) {
        // console.log(i);
        if (role_access[0]['main'][i].state === route1) {
          role_access_find = role_access[0]['main'][i];
          role_access_can = role_access_find['access'];
        }
      } 
      // console.log(role_access_find);
      // console.log(role_access_can);
      return role_access_can;

    } catch (error) {
      console.log("catch")
      return false;
    }

  }

  checkAccess(controller, func) {

      let accessRole = this.accessRole.getAll();
      let roles = this.getRole();
      if(roles === 'dinas'){
        roles = 'dinass';
      }
      let controllers = controller;
      let funcs = func;

      // console.log(accessRole);
      // console.log(roles);
      // console.log(controllers);
      // console.log(funcs); 
      
      let can_access = false;
      try { 
        can_access = accessRole[roles][controllers][funcs]; 
      } catch (error) {
        can_access = false; 
      }
      // console.log(can_access); 
      return can_access;
  }

  checkAccessSession(route1, route2) {

    let result_msg = JSON.parse(this.getData());
    console.log(result_msg)
    let route3 = '';

    if(route1 === ''){
      route1 = 'dashboard';
    }

    if(route2 === 'list') {
      route3 = 'find';
    }else if(route2 === 'detail'){
      route3 = 'findone';
    }else if(route2 === 'statistic'){
      route3 = 'findone';
    }else if(route2 === 'add'){
      route3 = 'create';
    }else if(route2 === 'edit'){
      route3 = 'update';
    }else if(route2 === 'delete'){
      route3 = 'delete';
    }else{
      route3 = 'find';
    }

    try {
      // console.log(result_msg);
      // console.log(route1);
      // console.log(route2); 
      // console.log(route3); 
      if(route1 === 'dashboard' || route1 ==='logout' || route1 === 'profile'){
        // console.log('masuk dashboard');
        return true;
      }
      else if(route1 === 'user'){
        // console.log('masuk user');
        return result_msg['role']['permissions']['users-permissions']['controllers']['user']['find']['enabled'];
      }
      else{
        // console.log('masuk else');
        return result_msg['role']['permissions']['application']['controllers'][route1][route3]['enabled'];
      }
      
    } catch (error) {
      console.log("catch")
      return false;
    }
  } 

  logIn = function(token, data, menu) {
    this.setData(data);
    this.setToken(token);
    this.setMenu(menu);
    return this.getToken();
  };

  logOut = function() {
    localStorage.removeItem('monitoring-proyek-token');
    localStorage.removeItem('monitoring-proyek-data');
    localStorage.removeItem('monitoring-proyek-menu');
  };
 
}
