import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { ErrorMessageService } from './error-message.service';
import { SessionService } from './session.service';
import { Session } from 'protractor';

@Injectable()

export class HttpRequestService {

    constructor(
        private http: Http, 
        private httpClient: HttpClient, 
        private router: Router, 
        private errorMessage: ErrorMessageService, 
        public session: SessionService
    ) { }

    public jwt = this.session.getToken();
    public header = { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + this.jwt };
    public header_upload = { 'Authorization': 'Bearer ' + this.jwt };
    public header_login = { 'Content-Type': 'application/json', 'Authorization': ''};

    // post data
    httpLogin(url: string, data: any): Observable<any> {
        return this.http.post( url, data, { headers : new Headers(this.header_login) })
        .map(result => {
            // console.log('httpPost result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPost error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal("Username atau Password salah");
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // post data
    httpRole(url: string, data: any): Observable<any> {
        return this.http.get( url, { headers : new Headers(this.header_login) })
        .map(result => {
            // console.log('httpPost result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPost error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal("Username atau Password salah");
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // api get
    httpGet(url: string, data: any): Observable<any> {
        return this.http.get( url, { headers : new Headers(this.header) })
        .map(result => {
            // console.log('httpGet result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpGet error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    // this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // post data
    httpPost(url: string, data: any): Observable<any> {
        return this.http.post( url, data, { headers : new Headers(this.header) })
        .map(result => {
            // console.log('httpPost result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPost error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // update data
    httpPut(url: string, data: any): Observable<any> {
        return this.http.put( url, data, { headers : new Headers(this.header) })
        .map(result => {
            // console.log('httpPut result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPut error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // delete data
    httpDelete(url: string, data: any): Observable<any> {
        return this.http.delete( url, { headers : new Headers(this.header) })
        .map(result => {
            // console.log('httpDelete result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpDelete error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // post data
    httpUpload(url: string, data: any): Observable<any> {
        return this.http.post( url, data, { headers : new Headers(this.header_upload) })
        .map(result => {
            // console.log('httpPost result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPost error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

    // post data
    httpUploadClient(url: string, data: any): Observable<any> {

        let headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.jwt);

        return this.httpClient.post( url, data, { 
            reportProgress: true,
            observe: 'events',
            headers : headers 
        })
        .map(result => {
            // console.log('httpPost result');
            // console.log(result);
            return result;
        })
        .catch(error => {
            // console.log('httpPost error');
            // console.log(error);
            if (String(error.status) === '504') {
                this.errorMessage.errorserver();
                return Observable.throw(error);
            }else {
                try {
                    const error_msg = JSON.parse(error._body);
                    this.errorMessage.openErrorSwal(error_msg['message']);
                    return Observable.throw(error);
                } catch (err) {
                    const error_msg = error.status + ' ' + error.statusText;
                    this.errorMessage.openErrorSwal(error_msg);
                    return Observable.throw(error);
                }
            }
        });
    }

}
