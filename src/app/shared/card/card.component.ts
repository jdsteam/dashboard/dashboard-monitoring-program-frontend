import {Component, OnInit, Input, ViewEncapsulation} from '@angular/core';
import {cardToggle, cardClose} from './card-animation'; 
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
  animations: [cardToggle, cardClose],
  encapsulation: ViewEncapsulation.None
})
export class CardComponent implements OnInit {
  @Input() nama: string;
  @Input() nama_lain: string;
  @Input() logo: string;
  @Input() dataPersen: any;
  @Input() dataPersenFinish: number = 0;
  @Input() dataPersenUnfinish: number = 0;
  @Input() blockClass: string;
  @Input() cardClass: string;
  @Input() classHeader: boolean = false;
  cardToggle: string = 'collapsed';
  cardClose: string = 'open';
  constructor(private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    console.log("************")
    console.log(this.dataPersenFinish)
    console.log(this.dataPersenUnfinish)
    console.log("************")
  }

  getBackground(image) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  toggleCard() {
    this.cardToggle = this.cardToggle === 'collapsed' ? 'expanded' : 'collapsed';
  }

  closeCard() {
    this.cardClose = this.cardClose === 'closed' ? 'open' : 'closed';
  }

  type = 'doughnut';
  data = {
	
    labels: [
      'Finish', 'Unfinish'
    ],
	
    datasets: [{
      data: [this.dataPersenFinish, this.dataPersenUnfinish],
      // data: [70, 30],
      backgroundColor: [
        '#2E4CCA',
        '#FFFFFF', 
      ],
      borderWidth: [
        '0px',
        '0px', 
      ],
      borderColor: [
        '#2E4CCA',
        '#FFFFFF', 

      ]
    }]
  }; 
  options1 = {
	legend: {
		display: false,
		labels: {
			fontColor: '#000000'
		},
		position: 'right'
	},
	tooltips: {
		enabled: false
	},
	plugins: {
      labels: {
		render: 'percentage',
		fontColor: ['white', 'white'],
		precision: 2
	  }
    }
  };

}