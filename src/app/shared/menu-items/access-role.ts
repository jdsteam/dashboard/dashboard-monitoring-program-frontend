import {Injectable} from '@angular/core';
import { SessionService } from '../service/session.service';
  

const access_role = 
  { 
    admin: {
      dashboard: {
        list: true
      },
      profile: {
        list: true
      },
      program: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        statistik: true
      },
      indikator: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      periodeprogress: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      lapprogress: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: true
      },
      periodeanggaran: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      kegiatan: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      lapanggaran: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: true
      },
      dinas: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      user: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      broadcast: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      notification: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      message: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
    },
    dinass: {
      dashboard: {
        list: true
      },
      profile: {
        list: true
      },
      program: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        statistik: true
      },
      indikator: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      periodeprogress: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      lapprogress: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: false
      },
      periodeanggaran: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      kegiatan: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true
      },
      lapanggaran: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: false
      },
      dinas: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      user: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      notification: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      message: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
    },
    pelapor: {
      dashboard: {
        list: true
      },
      profile: {
        list: true
      },
      program: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        statistik: true
      },
      indikator: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      periodeprogress: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapprogress: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: false
      },
      periodeanggaran: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      kegiatan: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapanggaran: {
        list: true,
        detail: true,
        add: true,
        edit: true,
        delete: true,
        verifikasi: false
      },
      dinas: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      user: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      notification: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      message: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
    },
    verifikator: {
      dashboard: {
        list: true
      },
      profile: {
        list: true
      },
      program: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        statistik: true
      },
      indikator: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      periodeprogress: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapprogress: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        verifikasi: true
      },
      periodeanggaran: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      kegiatan: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapanggaran: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        verifikasi: true
      },
      dinas: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      user: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      notification: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      message: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
    },
    pemantau: {
      dashboard: {
        list: true
      },
      profile: {
        list: true
      },
      program: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        statistik: true
      },
      indikator: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      periodeprogress: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapprogress: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        verifikasi: false
      },
      periodeanggaran: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      kegiatan: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      lapanggaran: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false,
        verifikasi: false
      },
      dinas: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      user: {
        list: false,
        detail: false,
        add: false,
        edit: false,
        delete: false
      },
      notification: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
      message: {
        list: true,
        detail: true,
        add: false,
        edit: false,
        delete: false
      },
    } 
  };

@Injectable()
export class AccessRole {
  getAll(){
    return access_role;
  }
  
}
