import {Injectable} from '@angular/core';
import { SessionService } from '../../shared/service/session.service';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
        {
            state: 'dashboard',
            name: 'Dashboard',
            type: 'link',
            icon: 'ti-home'
        },
        {
            state: 'program',
            name: 'Kegiatan',
            type: 'link',
            icon: 'ti-star',
        },
        {
            state: 'dinas',
            name: 'OPD',
            type: 'link',
            icon: 'ti-view-grid',
        },
        {
            state: 'user',
            name: 'User',
            type: 'link',
            icon: 'ti-user',
        },
        {
            state: 'logout',
            name: 'Logout',
            type: 'link',
            icon: 'ti-power-off',
        }
    ],
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  getAlls(): Menu[]{
    // let session: SessionService;
    // console.log(session.getMenu());
    console.log(JSON.parse(localStorage.getItem('monitoring-proyek-menu')));
    return JSON.parse(localStorage.getItem('monitoring-proyek-menu'))
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
