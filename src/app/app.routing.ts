import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    }, {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule'
    }, {
      path: 'program',
      loadChildren: './program/program.module#ProgramModule'
    }, {
      path: 'user',
      loadChildren: './user/user.module#UserModule'
    }, {
      path: 'dinas',
      loadChildren: './dinas/dinas.module#DinasModule'
    }, {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule'
    }, {
      path: 'broadcast',
      loadChildren: './broadcast/broadcast.module#BroadcastModule'
    }, {
      path: 'notification',
      loadChildren: './notification/notification.module#NotificationModule'
    }, {
      path: 'message',
      loadChildren: './message/message.module#MessageModule'
    }
  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [{
    path: 'auth',
    loadChildren: './authentication/authentication.module#AuthenticationModule'
  },{
    path: 'logout',
    redirectTo: 'auth/logout',
  },{
    path: 'error',
    loadChildren: './error/error.module#ErrorModule'
  },{
    path: 'maintenance/offline-ui',
    loadChildren: './maintenance/offline-ui/offline-ui.module#OfflineUiModule'
  }, ]
}, {
  path: '**',
  redirectTo: 'error/404'
}];
