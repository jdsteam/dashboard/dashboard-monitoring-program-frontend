import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapAnggaranAddComponent} from './lap-anggaran-add.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const LapAnggaranAddRoutes: Routes = [{
    path: '',
    component: LapAnggaranAddComponent,
    data: {
        breadcrumb: "Detail Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapAnggaranAddRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [LapAnggaranAddComponent]
})

export class LapAnggaranAddModule {}
