import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-lap-anggaran-add',
  templateUrl: './lap-anggaran-add.component.html',
  styleUrls: [ './lap-anggaran-add.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapAnggaranAddComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 
  
  private url_lapanggaran = '/api/lapanggarans';  
  private url_kegiatan = '/api/kegiatans';  
 
  dataLapAnggaran = [];  
  dataKegiatan = [];  

  myForm: FormGroup;   

  program_id : string = '0';
  periode_id : string = '0';
  periodeanggaran_id : string = '0';
  kegiatan_id : string = '0';
  lapanggaran_id : string = '0';
    
  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-logo.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapanggaran','add')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.periode_id = '0'; 
    this.periodeanggaran_id = routing[4];  
    this.kegiatan_id = routing[6]; 

    this.getDataKegiatan(this.kegiatan_id)
 

    this.myForm = new FormGroup({   
      bulan: new FormControl({value: '', disabled: false}, [Validators.required]),  
      serapan: new FormControl({value: '0', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),   
      persen: new FormControl({value: '0', disabled: false}, [Validators.required]),  
      keterangan: new FormControl({value: '', disabled: false}, []),    
      is_verified: new FormControl(), 
      is_deleted: new FormControl(), 
    });
 
  }

  
  getDataKegiatan(id) {

    const json_kegiatan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kegiatan + '/' + id , json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataKegiatan = result_msg; 
          this.periode_id = result_msg['periode']['id'];
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  

  checkPersen(value){ 
    // console.log(event);
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataKegiatan['anggaran']);
    let persen = (value / this.dataKegiatan['anggaran']) * 100;
    // console.log(persen)
    this.myForm.patchValue({
      persen: persen.toFixed(2)
    });
  }

  onSubmit() {
    this.submitted = true;
    let val_lapanggaran = this.myForm.value;  
 
    val_lapanggaran['program'] = this.program_id;
    val_lapanggaran['periode'] = this.periode_id;
    val_lapanggaran['periodeanggaran'] = this.periodeanggaran_id;
    val_lapanggaran['kegiatan'] = this.kegiatan_id;
    val_lapanggaran['is_deleted'] = false;
    val_lapanggaran['is_verified'] = false;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapanggaran = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPost(this.url_lapanggaran, json_lapanggaran).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']); 
 
        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showBack(){
    this._location.back();
  }

  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Laporan Anggaran berhasil ditambahkan.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => { 
      this.router.navigate(['/program/', this.program_id, 'anggaran', this.periodeanggaran_id, 'kegiatan', this.kegiatan_id])
    });
    
  }

}
