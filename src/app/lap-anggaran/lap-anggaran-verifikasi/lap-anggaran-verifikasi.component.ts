import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';


@Component({
  selector: 'app-lap-anggaran-verifikasi',
  templateUrl: './lap-anggaran-verifikasi.component.html',
  styleUrls: [ './lap-anggaran-verifikasi.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapAnggaranVerifikasiComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 

  private url_program = '/api/programs'; 
  private url_periode = '/api/periodes'; 
  private url_periodeanggaran = '/api/periodeanggarans'; 
  private url_kegiatan = '/api/kegiatans'; 
  private url_lapanggaran = '/api/lapanggarans';  

  dataProgram = [];
  dataPeriode = []; 
  dataPeriodeAnggaran = []; 
  dataKegiatan = [];
  dataLapAnggaran = [];  
 
  pencapaianPeriodeKuantitas:number = 0;
  pencapaianPeriodePersen:number = 0;
  pencapaianKegiatanKuantitas:number = 0;
  pencapaianKegiatanPersen:number = 0;

  myForm: FormGroup;   

  program_id: string = '0';
  periode_id: string = '0';
  periodeanggaran_id: string = '0';
  kegiatan_id: string = '0';
  lapanggaran_id: string = '0';
 
  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-gambar.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapanggaran','verifikasi')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.periode_id = '0'; 
    this.periodeanggaran_id = routing[4];
    this.kegiatan_id = routing[6];   
    this.lapanggaran_id = routing[8];  

    this.getDataLapAnggaran(this.lapanggaran_id);   
    this.countPencapaianPeriode(this.periodeanggaran_id); 
    this.countPencapaianKegiatan(this.kegiatan_id); 


    this.myForm = new FormGroup({ 
      id: new FormControl(),  
      serapan: new FormControl({value: '', disabled: false}, [Validators.required, Validators.min(0), Validators.pattern("^[0-9]*$")]),  
      persen: new FormControl({value: '', disabled: false}, [Validators.required]),  
      keterangan_verifikasi: new FormControl({value: '', disabled: false}, [Validators.required]),    
      is_verified: new FormControl(),  
    });
  }

   

  getDataLapAnggaran(id) {

    const json_lapanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/' + id , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataLapAnggaran = result_msg;   
          this.setForm(result_msg);
          
          this.getDataKegiatan(result_msg['kegiatan']['id']); 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKegiatan(id) {

    const json_kegiatan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kegiatan + '/' + id , json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataKegiatan = result_msg; 

          this.getDataPeriodeAnggaran(result_msg['periodeanggaran']['id']);   
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_periodeanggaran + '/' + id , json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataPeriodeAnggaran = result_msg; 
          
          this.getDataProgram(result_msg['program']['id']);  
          this.periode_id = result_msg['periode']['id']; 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_program+ '/' + id , json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataProgram = result_msg;   
          
          this.loading = false;
          this.finish_loading = true;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianPeriode(id){
    const json_lapanggaran= {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?periodeanggaran=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.periodeanggaran.apbd) + parseInt(element.periodeanggaran.nonapbd);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianKegiatan(id){
    const json_lapanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?kegiatan=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.kegiatan.anggaran);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianKegiatanKuantitas = kuantitas;
          this.pencapaianKegiatanPersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  onSubmit() {
    this.submitted = true;
    let val_lapanggaran = this.myForm.value;  
  
    val_lapanggaran['is_verified'] = true;
     
    console.log(val_lapanggaran);
    console.log(val_lapanggaran.value); 
     
    this.loading = true;
    let json_lapprogress = JSON.stringify(val_lapanggaran); 
    this.httpRequest.httpPut(this.url_lapanggaran + '/' + val_lapanggaran['id'], json_lapprogress).subscribe(
      result => {
        try {
          const result_msg = JSON.parse(result._body);  
          console.log(result_msg);
          this.loading = false;
          this.showSuccess(result_msg['nama']);  

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  
  setForm(data){ 
 
    console.log(data);   
    this.myForm.patchValue({
      id: data.id, 
      serapan: data.serapan, 
      persen: data.persen.toFixed(2),
      keterangan_verifikasi: data.keterangan_verifikasi, 
      is_verified: true,  
    });
  }

  
  checkPersen(value){ 
    // console.log(event);
    // let value = event.target.value;  
    // console.log(value);
    // console.log(this.dataKegiatan['anggaran']);
    let persen = (value / this.dataKegiatan['anggaran']) * 100;
    // console.log(persen)
    this.myForm.patchValue({
      persen: persen.toFixed(2)
    });
  }

  showBack(){
    this._location.back();
  }
 
  
  showSuccess(nama){
    swal({
      title: 'Success',
      text: 'Laporan Anggaran berhasil diverifikasi.',
      type: 'success',
      allowOutsideClick: false
    }).then(() => {
      this._location.back();
    });
    
  }

}
