import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapAnggaranVerifikasiComponent} from './lap-anggaran-verifikasi.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const LapAnggaranVerifikasiRoutes: Routes = [{
    path: '',
    component: LapAnggaranVerifikasiComponent,
    data: {
        breadcrumb: "Verifikasi Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapAnggaranVerifikasiRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [LapAnggaranVerifikasiComponent]
})

export class LapAnggaranVerifikasiModule {}
