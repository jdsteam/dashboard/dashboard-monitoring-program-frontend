import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapAnggaranDetailComponent} from './lap-anggaran-detail.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const LapAnggaranDetailRoutes: Routes = [{
    path: '',
    component: LapAnggaranDetailComponent,
    data: {
        breadcrumb: "Detail Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapAnggaranDetailRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [LapAnggaranDetailComponent]
})

export class LapAnggaranDetailModule {}
