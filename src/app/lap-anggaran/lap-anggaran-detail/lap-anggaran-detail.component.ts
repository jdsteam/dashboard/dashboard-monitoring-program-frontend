import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {fadeInOutTranslate} from '../../shared/elements/animation';
import {ToastyService, ToastOptions, ToastData} from 'ng2-toasty';
import { ConverterService } from '../../shared/service/converter.service';
import { HttpRequestService } from '../../shared/service/http-request.service';
import { SessionService } from '../../shared/service/session.service';
import { ErrorMessageService } from '../../shared/service/error-message.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators , FormArray} from '@angular/forms';
import swal from 'sweetalert2';
import {Location} from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { round } from 'd3';


@Component({
  selector: 'app-lap-anggaran-detail',
  templateUrl: './lap-anggaran-detail.component.html',
  styleUrls: [ './lap-anggaran-detail.component.css',],
  animations: [fadeInOutTranslate]
})
export class LapAnggaranDetailComponent implements OnInit {

  public loading = false;  
  public finish_loading = false;  
  disableTextbox:Boolean = true; 

  private url_program = '/api/programs'; 
  private url_periode = '/api/periodes'; 
  private url_periodeanggaran = '/api/periodeanggarans'; 
  private url_kegiatan = '/api/kegiatans'; 
  private url_lapanggaran = '/api/lapanggarans';  

  dataProgram = [];
  dataPeriode = []; 
  dataPeriodeAnggaran = []; 
  dataKegiatan = [];
  dataLapAnggaran = [];  

  pencapaianPeriodeKuantitas:number = 0;
  pencapaianPeriodePersen:number = 0;
  pencapaianKegiatanKuantitas:number = 0;
  pencapaianKegiatanPersen:number = 0;
  
  myForm: FormGroup;   

  program_id: string = '0';
  periode_id: string = '0';
  periodeanggaran_id: string = '0';
  kegiatan_id: string = '0';
  lapanggaran_id: string = '0';
 
  submitted: boolean; 
    
  logoPath = "";
  logoNull = "/api/uploads/default-gambar.png"; 

  constructor(
    private toastyService: ToastyService,
    private convert: ConverterService,
    private httpRequest: HttpRequestService,
    private session: SessionService,
    private errorMessage: ErrorMessageService,
    private router: Router,
    private route: ActivatedRoute,
    private _location: Location) {
 
  }
  
  ngOnInit() {
    if(!this.session.checkAccess('lapanggaran','detail')){
      this.router.navigate(['/error/403']);
    }
    const url = this.router.url;
    const routing = url.split('/');  
    let id = this.route.snapshot.paramMap.get('id'); 

    this.program_id = routing[2];
    this.periode_id = '0'; 
    this.periodeanggaran_id = routing[4];
    this.kegiatan_id = routing[6];   
    this.lapanggaran_id = routing[8];  

    this.getDataLapAnggaran(this.lapanggaran_id); 
    this.countPencapaianPeriode(this.periodeanggaran_id); 
    this.countPencapaianKegiatan(this.kegiatan_id); 

  }

   

  getDataLapAnggaran(id) {

    const json_lapanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/' + id , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataLapAnggaran = result_msg;   
          
          this.getDataKegiatan(result_msg['kegiatan']['id']); 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataKegiatan(id) {

    const json_kegiatan = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_kegiatan + '/' + id , json_kegiatan).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataKegiatan = result_msg; 

          this.getDataPeriodeAnggaran(result_msg['periodeanggaran']['id']);   
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
  
  getDataPeriodeAnggaran(id) {

    const json_periodeanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_periodeanggaran + '/' + id , json_periodeanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataPeriodeAnggaran = result_msg; 
          this.dataPeriodeAnggaran['total_anggaran'] = Number(result_msg['apbd']) + Number(result_msg['nonapbd']);
          
          this.getDataProgram(result_msg['program']['id']);  
          this.periode_id = result_msg['periode']['id']; 
          this.loading = false;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getDataProgram(id) {

    const json_program = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_program+ '/' + id , json_program).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          this.dataProgram = result_msg;   
          
          this.loading = false;
          this.finish_loading = true;

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }
 

  
  countPencapaianPeriode(id){
    const json_lapanggaran= {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?periodeanggaran=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element);
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.periodeanggaran.apbd) + parseInt(element.periodeanggaran.nonapbd);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianPeriodeKuantitas = kuantitas;
          this.pencapaianPeriodePersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  countPencapaianKegiatan(id){
    const json_lapanggaran = {};
    this.loading = true; 
    // console.log(query);

    this.httpRequest.httpGet(this.url_lapanggaran+ '/?kegiatan=' + id + '&is_deleted=false' , json_lapanggaran).subscribe(
      result => {
        try {
          // console.log(result);
          const result_msg = JSON.parse(result._body);
          let temp = result_msg;   
          let kuantitas = 0;
          let target = 0;
          let pencapaian = 0;
          temp.forEach(element => {
            // console.log(element); 
            kuantitas = kuantitas + parseInt(element.serapan);
            target = parseInt(element.kegiatan.anggaran);
          });
          pencapaian = (kuantitas / target) * 100;
          pencapaian = +pencapaian.toFixed(2);
          pencapaian = pencapaian || 0;
          this.pencapaianKegiatanKuantitas = kuantitas;
          this.pencapaianKegiatanPersen = pencapaian;
          console.log(kuantitas);
          console.log(target);
          console.log(pencapaian)

          this.loading = false; 

        } catch (error) {
          this.loading = false;
          this.errorMessage.openErrorSwal('Something wrong.');
        }
      },
      error => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  showBack(){
    this._location.back();
  }
 

}
