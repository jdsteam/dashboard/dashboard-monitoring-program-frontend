import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import {LapAnggaranEditComponent} from './lap-anggaran-edit.component';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

export const LapAnggaranEditRoutes: Routes = [{
    path: '',
    component: LapAnggaranEditComponent,
    data: {
        breadcrumb: "Detail Periode Anggaran"
    }
}];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LapAnggaranEditRoutes),
        SharedModule,
        NgxLoadingModule.forRoot({}),
    ],
    declarations: [LapAnggaranEditComponent]
})

export class LapAnggaranEditModule {}
